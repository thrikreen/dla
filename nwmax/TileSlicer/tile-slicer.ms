/*-----------------------------------------------------------------------------\

	Tile Slicer Plus v1.08.23
	
	NWN Tile Slicer
	by Gareth Hughes
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v0.10.5  --	Original version by roboius circa 2003
				 --	Fixed guess code as dimensions were swapped
				 --	Fixed location calc. rows/cols were swapped
				 --	Made nwmax the default tool used
		---------------------------------------------------------------
		v1.08.23 --	Original compilation version
					A trimmed down version, duplicated utilities are now
					gone.  Tile Light creation should work now as it conforms
					to the "Plus" version AuroraLight helper. "MDL Mirror"
					has been moved to "Plus".
		
\-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------\

	The Neverwinter Nights Tile Slicer is a utility for Discreet's 3DSMax and
	(eventually) gMax.  It is designed to assist in the process of creating
	multi-tile groups for NWN tilesets.  Specifically, it automates the process
	of converting a large model in 3DSMax into a set of NWN tile models, ready
	for use in the game.
	
	For more information about this utility, please see the README file or
	visit the following URL:
	
	http://rockbottom.vtex.net/slicer.html (can be found using the Wayback
	Machine)
	
\-----------------------------------------------------------------------------*/

--	Global Variable Definitions
--	Slicing global params

global tile_names = #()
global rows = 1
global cols = 1
global location = [0, 0, 0]

--	Options

global opt_increment = true
global opt_variation = true
global opt_pos_num_only = false
global opt_origin = true
global opt_create_lights
global opt_create_anim
global opt_snap_vertices
global opt_adjust_pivot
global opt_convert
global opt_test_shadows
global invalid_shadow_color = [255, 0, 0]
global disabled_shadow_color = [0, 0, 0]

--	Lighting parameters

global light_radius_1 = 1400.0
global light_radius_2 = 500.0
global light_color_1 = [0, 0, 0]
global light_color_2 = [0, 0, 0]

--	Export scripts

global nwmax_export_path = gnx_exportPath

--	Verbose debug output

global verbose = false

--	Shortcuts for tile name generation

global grid_letters = #("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "aa", "bb", "cc", "dd", "ee", "ff")
global max_letter = 26
global alpha_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
global numeric_chars = "0123456789"
global title_string = "Tile Slicer \"Plus\""

--	Globals to enable certain features

global version_5000 = false

--	End Globals
--	Functions

fn is_alpha char = (

	try (
	
		local ii = findstring alpha_chars char
		
		return (ii != undefined)
		
	) catch (
	
		return false
		
	)
	
)

fn is_numeric char = (

	try (
	
		local ii = findstring numeric_chars char
		
		return (ii != undefined)
		
	) catch (
	
		return false
		
	)
	
)

fn two_digit_string num = (

	local name = ""
	
	if (num < 10) then (
	
		name += "0"
		
	)
	
	name += num as string
	
	return name
	
)

fn uppercase instring = (

	local upper, lower, outstring, ii, jj
	
	upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lower = "abcdefghijklmnopqrstuvwxyz"
	outstring = copy instring
	
	for ii = 1 to outstring.count do (
	
		jj = findstring lower outstring[ii]
		
		if (jj != undefined) then (
		
			outstring[ii] = upper[jj]
			
		)
		
	)
	
	return outstring
	
)

fn lowercase instring = (

	local upper, lower, outstring, ii, jj
	
	upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lower = "abcdefghijklmnopqrstuvwxyz"
	outstring = copy instring
	
	for ii = 1 to outstring.count do (
	
		jj = findstring upper outstring[ii]
		
		if (jj != undefined) then (
		
			outstring[ii] = lower[jj]
			
		)
		
	)
	
	return outstring
	
)

--	Create the list of tile names.

fn create_names prefix pos var = (

	local num, tile = 1, ii, jj, errors = 0, msg = ""
	
	--	Error Checking
	--	Make sure the prefix and grid position are valid.
	
	if ((prefix.count == 0) or (prefix.count > 5)) then (
	
		msg += "Invalid tilename prefix.  Prefixes are usually in the\nform of three letters followed by a two-digit number (XXX##)./n"
		errors += 1
		
	)
	
	if (pos.count != 3) then (
	
		msg += "Invalid grid position.  Grid position should be in\nthe form of a letter followed by a two-digit number (X##).\n"
		errors += 1
		
	)
	
	--	Transform the input strings into corresponding integer values.
	
	num = substring pos 2 2 as integer
	pos = substring pos 1 1
	
	if (pos != undefined) then (
	
		pos = findstring alpha_chars pos
		
	)
	
	var = var as integer
	
	--	Check the input parameters for errors.
	
	if ((num == undefined) or (num == 0)) then (
	
		msg += "Invalid grid position number, should be between 01 and 99.  Grid\nposition should be in the form of a letter followed by a two-digit number (X##).\n"
		errors += 1
		
	)
	
	if ((num < 1) or (num > 99)) then (
	
		msg += "Grid position number must be between 01 and 99.\n"
		errors += 1
		
	)
	
	if ((pos == undefined) or (pos == 0)) then (
	
		msg += "Invalid grid position letter, should be between A and Z.  Grid position\nshould be in the form of a letter followed by a two-digit number (X##).\n"
		errors += 1
		
	)
	
	if ((var == undefined) or (var == 0)) then (
	
		msg += "Invalid variation number, should be between 01 and 99.\n"
		errors += 1
		
	)
	
	if ((var < 1) or (var > 99)) then (
	
		msg += "Variation number must be between 01 and 99.\n"
		errors += 1
		
	)
	
	--	End Error Checking
	
	if (errors > 0) then (
	
		messagebox msg title:title_string
		
		return 0
		
	) else (
	
		--	Okay, we have valid input if we get this far.  Let's do this thing.
		
		if (opt_variation) then (
		
			--	Update the variation number with each tile.
			
			pos = alpha_chars[pos] + two_digit_string num
			
			for ii in 1 to rows do (
			
				for jj in 1 to cols do (
				
					tile_names[tile] = prefix + "_" + pos + "_" + two_digit_string var
					
					--	Increment or decrement the variation number.
					
					if (opt_increment) then (
					
						var += 1
						
						if (var > 99) then (
						
							var = 1
							
						)
						
					) else (
					
						var -= 1
						
						if (var < 1) then (
						
							var = 99
							
						)
						
					)
					
					tile += 1;
					
				)
				
			)
			
		) else (
		
			--	Update the grid position with each tile.
			
			local initial = num
			
			var = two_digit_string var
			
			for ii in 1 to rows do (
			
				for jj in 1 to cols do (
				
					tile_names[tile] = prefix + "_" + grid_letters[pos] + two_digit_string num + "_" + var
					
					--	Increment or decrement the grid position number.
					
					if (opt_increment) then (
					
						num += 1
						
						if (num > 99) then (
						
							num = 1
							
						)
						
					) else (
					
						num -= 1
						
						if (num < 1) then (
						
							num = 99
							
						)
						
					)
					
					tile += 1;
					
				)
				
				if (not opt_pos_num_only) then (
				
					--	Increment or decrement the grid position letter.
					
					if (opt_increment) then (
					
						pos += 1
						
						if (pos > max_letter) then (
						
							pos = 1
							
						)
						
					) else (
					
						pos -= 1
						
						if (pos < 1) then (
						
							pos = max_letter
							
						)
						
					)
					
					--	Reset the grid position number for each row.
					
					num = initial
					
				)
				
			)
			
		)
		
		print tile_names
		
		return true
		
	)
	
)

--	Adjust the vertices of the given object, rounding them off to the nearest
--	integer value.

fn snap_vertices obj = (

	if (not iskindof obj Editable_Mesh) then (
	
		if (verbose) then (
		
			format "% is not an editable mesh\nSnap Vertices skipped.\n" obj.name
			
		)
		
		return 0
		
	) else (
	
		if (verbose) then (
		
			format "Snap Vertices on %\n" obj.name
			
		)
		
		for ii = 1 to obj.numverts do (
		
			local v, x, y, z
			
			v = getvert obj ii
			
			--	Correct IEEE float rounding
			
			if (v.x >= 0.0) then (
			
				x = floor(v.x + 0.5)
				
			) else (
			
				x = ceil(v.x - 0.5)
				
			)
			
			if (v.y >= 0.0) then (
			
				y = floor(v.y + 0.5)
				
			) else (
			
				y = ceil(v.y - 0.5)
				
			)
			
			if (v.z >= 0.0) then (
			
				z = floor(v.z + 0.5)
				
			) else (
			
				z = ceil(v.z - 0.5)
				
			)
			
			if (verbose) then (
			
				format "   % % % => % % %\n" v.x v.y v.z x y z
				
			)
			
			setvert obj ii x y z
			
		)
		
	)
	
)

--	Similar to the "Reset Transform" utility.

fn reset_transforms obj = (

	local ntm, ptm, rtm = matrix3 1, tm, piv
	
	if (not iskindof obj Editable_Mesh) then (
	
		if (verbose) then (
		
			format "% is not an editable mesh\nReset Transform skipped.\n" obj.name
			
		)
		
		return 0
		
	) else (
	
		--	Get the parent and node TMs.
		
		ntm = obj.transform
		
		if (obj.parent != undefined) then (
		
			ptm = obj.parent.transform
			
		) else (
		
			ptm = matrix3 1
			
		)
		
		--	Compute the relative TM.
		
		ntm = ntm * inverse ptm
		
		--	Need to flip the normals if the determinant is negative.
		
		if (ntm.determinantsign < 0) then (
		
			meshop.flipnormals obj obj.faces
			
		)
		
		--	The reset TM only inherits position.
		
		rtm.translation = ntm.translation
		
		--	Set the node TM to the reset TM.
		
		tm = rtm * ptm
		obj.transform = tm
		
		--	Compute the pivot TM.
		
		local piv = obj.objecttransform * inverse obj.transform
		
		--	Reset the offset to 0.
		
		obj.objectoffsetpos = [0, 0, 0]
		obj.objectoffsetrot = (quat 0 0 0 1)
		obj.objectoffsetscale = [1, 1, 1]
		
		--	Take the position out of the original node transform matrix since
		--	we don't reset position.
		
		ntm.translation = [0, 0, 0]
		
		--	Apply the offset to the TM.
		
		ntm = piv * ntm
		
		--	Apply an XForm modifier to the node.
		
		local xform_mod = xform()
		
		addmodifier obj xform_mod
		
		xform_mod.gizmo.transform = ntm
		
	)
	
)

--	Test for valid shadows.  If a ray from the pivot point hits the front of
--	any face in the object, the shadows will be off.

fn test_shadows obj = (

	local face, normal, vertex, dp, errors = 0
	
	if (not iskindof obj Editable_Mesh) then (
	
		if (verbose) then (
		
			format "% is not an editable mesh\nTest Shadows skipped.\n" obj.name
			
		)
		
		return 0
		
	) else (
	
		for ii = 1 to obj.numfaces do (
		
			face = getface obj ii
			normal = getfacenormal obj ii
			vertex = getvert obj face.x
			dp = dot normal vertex
			
			if (((dot normal obj.pivot) - dp) > 5e-3) then (
			
				errors += 1
				
			)
			
		)
		
		if (errors > 0) then (
		
			format "Warning: Object % may have an invalid shadow.\n" obj.name
			
			obj.wirecolor = invalid_shadow_color
			
			return 1
			
		) else (
		
			return 0
			
		)
		
	)
	
)

--	Copy the modifiers from source to destination.

fn copy_modifiers dst src = (

	local ii
	
	for ii in src.modifiers.count to 1 by -1 do (
	
		addmodifier dst (copy src.modifiers[ii])
		
	)
	
)

--	Slice up the selected geometry.

fn slice_tiles = (

	local mdl_bases = #()
	local xx, yy, ii, jj, errors = 0
	local tile_min = location
	local tile_max = [tile_min.x + (cols - 1) * 1000, tile_min.y + (rows - 1) * 1000, 0.0]
	
	--	Create the MDL helper nodes.
	
	ii = 1
	
	for yy = tile_min.y to tile_max.y by 1000 do (
	
		for xx = tile_min.x to tile_max.x by 1000 do (
		
			local mdl_base, mdl_light, mdl_anim
			
			--	Create the model base.
			
			try (
			
				mdl_base = aurorabase()
				mdl_base.classification_sel = 2
				mdl_base.delegate.boxSize = [100, 100, 5]
				
				local tri = ngon()
				
				tri.radius = 25
				tri.nsides = 3
				tri.rotation *= (quat 90 [0, 0, 1])
				tri.pos = mdl_base.pos + [0, 50, 0]	--[xx + 500, yy + 550, 0]
				tri.wireColor = (color 92 179 240)
				tri.name = "ignore_" + tri.name
				tri.parent = mdl_base
				
			) catch (
			
				msg = "Error: Unable to create AuroraBase helper object."
				
				messagebox msg title:title_string
				
				return 0
				
			)
			
			mdl_base.name = uppercase tile_names[ii]
			mdl_base.pos = [xx + 500, yy + 500, 0]
			
			--	Optionally create the tile lights.
			
			if (opt_create_lights) then (
			
				for jj = 1 to 2 do (
				
					try (
					
						mdl_light = auroralight()
						mdl_light.lightpriority = 5
						mdl_light.affectDynamic = 1
						mdl_light.nDynamicType = 0
						mdl_light.delegate.boxSize = [10, 10, 10]
						
						if (jj == 1) then (
						
							mdl_light.radius = light_radius_1
							mdl_light.rgb = light_color_1
							mdl_light.pos = [xx + 500, yy + 500, 500]
							mdl_light.name = mdl_base.name + "ml1"
							
						) else (
						
							mdl_light.radius = light_radius_2
							mdl_light.rgb = light_color_2
							mdl_light.pos = [xx + 300, yy + 300, 400]
							mdl_light.name = mdl_base.name + "ml2"
							
						)
						
					) catch (
					
						msg = "Error: Unable to create AuroraLight " + jj + " helper object."
						
						messagebox msg title:title_string
						
						return 0
				
					)
					
					mdl_light.parent = mdl_base
					
				)
				
			)
			
			--	Optionally create the animation dummy node.
			
			if (opt_create_anim) then (
			
				mdl_anim = dummy()
				mdl_anim.name = mdl_base.name + "a"
				mdl_anim.pos = mdl_base.pos
				mdl_anim.boxsize = [10, 10, 10]
				mdl_anim.parent = mdl_base
				
			)
			
			format "Created tile %\n" mdl_base.name
			
			mdl_bases[ii] = mdl_base
			ii += 1
			
		)
		
	)
	
	progressStart "Slicing tiles..."
	
	--	Make a slicing box big enough to cover the selected objects.
	
	l = 1000
	w = 1000
	h = selection.max.z - selection.min.z + 2000
	
	--	Need to make a copy of the selection array, as we'll be deleting
	--	objects from it.  Things go horribly wrong if you delete objects from
	--	the selection array directly.
	
	selected_objects = selection as array
	
	--	Slice the selected objects.
	
	for jj = 1 to selected_objects.count do (
	
		local obj = selected_objects[jj]
		local orig = copy obj
		
		--	Update the progress bar, check if we should quit early.
		
		if (progressUpdate (100.0 * jj / selected_objects.count) == false) then (
		
			progressEnd()
			
			return 0;
			
		)
		
		ii = 1
		
		for yy = tile_min.y to tile_max.y by 1000 do (
		
			for xx = tile_min.x to tile_max.x by 1000 do (
			
				local mdl_base = mdl_bases[ii]
				
				--	Create a slicing box.
				
				slicing_box = box length:l width:w height:h position:[xx + 500, yy + 500, obj.min.z - 1000]
				
				--	Do a cut/slice boolean operation.
				
				boolObj.createBooleanObject obj slicing_box 4 5
				boolObj.setBoolOp obj 5
				boolObj.setBoolCutType obj 2
				
				--	This collapses the modifier stack as well, which means all
				--	the standard 3DSMax modifiers get copied to any new objects
				--	we create.  We have to handle the MDL modifiers seperately.
				
				convertToMesh obj
				
				--	If the boolean operands overlapped, the cut faces will be
				--	selected.  We should detach them into their own object.
				
				if (obj.selectedFaces.count > 0) then (
				
					local new = copy obj
					
					--	Give the new object a sane name.
					
					new.name = obj.name + "_" + two_digit_string ii
					
					--	Detach the selected faces into the new object.
					
					new.mesh = meshOp.detachFaces obj obj.selectedFaces asMesh:true
					new.wirecolor = obj.wirecolor
					new.parent = mdl_base
					
					--	Optionally clean up the vertices.
					
					if (opt_snap_vertices) then (
					
						snap_vertices new
						
					)
					
					--	Optionally adjust the pivot of the new object.
					
					if (opt_adjust_pivot) then (
					
						new.pivot = new.center
						
					)
					
					--	Always Reset Transforms on the new object.
					
					reset_transforms new
					
					--	Optionally convert polygons to faces, or just collapse
					--	the stack.
					
					if (opt_convert) then (
					
						convertTo new Editable_Patch
						convertToMesh new
						
					) else (
					
						collapsestack new
						
					)
					
					--	Optionally test if the new object will cast valid
					--	shadows.
					
					if (opt_test_shadows) then (
					
						errors += test_shadows new
						
					)
					
					--	Copy the modifers to the new object.
					
					copy_modifiers new orig
					
				)
				
				ii += 1
				
			)
			
		)
		
		--	FIXME: Shouldn't do this all the time. (not sure what is wrong)
		
		delete orig
		delete obj
		
	)
	
	progressEnd()
	
	--	Report any errors.
	
	if (errors > 0) then (
	
		msg = "Warning: Detected potential shadow problems in " + errors as string + " objects.\nThese objects have had their wireframe color changed to the\ncolor you selected in the Tile Slicer rollout."
		
		messagebox msg title:title_string
		
	)
	
	--	Redraw all views.
	
	max views redraw
	
)

--	End Functions
--	Rollouts

--	Tile Slicer

rollout rloTileSlicer "NWN Tile Slicer" (

	group "Tile Names" (
	
		label lbl_name_1 "Prefix:" across:3 align:#left offset:[0, -4]
		label lbl_name_2 "Pos:" align:#center offset:[5, -4]
		label lbl_name_3 "Var:" align:#center offset:[7, -4]
		edittext txt_prefix "" across:3 fieldwidth:52 offset:[-5, -2] text:""
		edittext txt_position "_" fieldwidth:37 offset:[8, -2] text:"A01"
		edittext txt_variation "_" bold:true fieldwidth:26 offset:[12, -2] text:"01"
		dropdownlist ddl_inc_dec "" across:2 items:#("Increment", "Decrement") offset:[-1, 2] width:78
		dropdownlist ddl_pos_var "" items:#("Position", "Variation") offset:[8, 2] selection:2 width:66
		checkbox chk_number "Position Number Only" enabled:false
		
	)
	
	group "Group Dimensions" (
	
		spinner spn_rows "Rows:" across:2 align:#left fieldwidth:28 range:[1, 32, rows] scale:1 type:#integer width:65
		spinner spn_cols "Cols:" align:#right fieldwidth:28 range:[1, 32, cols] scale:1 type:#integer width:65
		button guess_button "Guess" tooltip:"Guess the dimensions of the group, using the size of the selected objects." width:145
		
	)
	
	group "Group Location" (
	
		checkbox chk_origin "Center is at [0, 0]" checked:opt_origin
		spinner spn_location_x "Bottom Left X:" align:#right enabled:false fieldwidth:62 range:[-100000, 100000, location.x] scale:1 type:#worldunits
		spinner spn_location_y "Bottom Left Y:" align:#right enabled:false fieldwidth:62 range:[-100000, 100000, location.y] scale:1 type:#worldunits
		
	)
	
	button slice_button "Slice" tooltip:"Slice the selected objects into tile-sized chunks." width:150
	
	--	Rollout Events
	--	Tile Names
	
	on ddl_inc_dec selected ii do (
	
		opt_increment = (ii == 1)
		
	)
	
	on ddl_pos_var selected ii do (
	
		if (ii == 1) then (
		
			opt_variation = false
			txt_position.bold = true
			txt_variation.bold = false
			chk_number.enabled = true
			
		) else (
		
			opt_variation = true
			txt_position.bold = false
			txt_variation.bold = true
			chk_number.enabled = false
			
		)
		
	)
	
	on chk_number changed value do (
	
		opt_pos_num_only = value
		
	)
	
	--	Group Dimensions
	
	on spn_rows changed value do (
	
		rows = value
		
	)
	
	on spn_cols changed value do (
	
		cols = value
		
	)
	
	on guess_button pressed do (
	
		rows = (((selection.max.y - selection.min.y) / 1000.0) + 0.5) as integer
		cols = (((selection.max.x - selection.min.x) / 1000.0) + 0.5) as integer
		spn_rows.value = rows
		spn_cols.value = cols
		
	)
	
	--	Group Location
	
	on chk_origin changed value do (
	
		opt_origin = value
		spn_location_x.enabled = not value
		spn_location_y.enabled = not value
		
	)
	
	--	Options
	
	on spn_location_x changed value do (
	
		location.x = value
		
	)
	
	on spn_location_y changed value do (
	
		location.y = value
		
	)
	
	--	Slice
	
	on slice_button pressed do undo on (
	
		local obj, ret, errors = 0, msg = ""
		
		--	Error Checking
		--	Make sure we have objects selected.
		
		if (selection.count == 0) then (
		
			msg += "Error: No objects selected!\n"
			errors += 1
			
		)
		
		--	Make sure there are no groups in the selected objects.
		
		for obj in selection do (
		
			if (isGroupHead obj or isGroupMember obj) then (
			
				msg += "Error: Cannot slice grouped objects!\n"
				errors += 1
				
			)
			
		)
		
		--	End Error Checking
		
		if (errors > 0) then (
		
			messagebox msg title:title_string
			
			return 0
			
		) else (
		
			--	Make sure the user wants to slice a 1x1 tile group.
			
			if ((rows == 1) and (cols == 1)) then (
			
				msg = "Warning: You are about to slice a 1x1 group.\nAre you sure this is what you want to do?"
				ret = querybox msg title:title_string
				
				if (ret == false) then (
				
					return 0
					
				)
				
			)
			
			--	Create the list of tile names.
			
			ret = create_names txt_prefix.text txt_position.text txt_variation.text
			
			if (ret == false) then (
			
				return 0
				
			)
			
			--	If the group is centered at the origin, calculate the position.
			
			if (opt_origin) then (
			
				location = [cols * -1000 / 2, rows * -1000 / 2, 0]
				
				format "Location = %\r\n" location
				
			)
			
			slice_tiles()
			
		)
		
	)
	
	on rloTileSlicer open do (
	
		--	Position floater
	
		tsPos_x = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "tileSlicer" "ts_pos_x"
		tsPos_y = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "tileSlicer" "ts_pos_y"
		
		if (tsPos_x == undefined) or (tsPos_x == "") then (
		
			tsFloater.pos = [20, 20]
			
		) else (
		
			tsFloater.pos.x = (tsPos_x as float) - 12
			tsFloater.pos.y = (tsPos_y as float) - 16
			
		)
		
		tsFloater.size = [200, 600]
		
		--	End Position
		
	)
	
	--	End Events
	
)

--	Preferences

rollout rlo_tsPrefs "Preferences" (

	group "Options" (
	
		checkbox chk_lights "Create Tile Lights" checked:opt_create_lights
		checkbox chk_anim "Create Animation Nodes" checked:opt_create_anim
		checkbox chk_snap "Snap Vertices" checked:opt_snap_vertices
		checkbox chk_pivot "Adjust Pivots" checked:opt_adjust_pivot
		checkbox chk_convert "Make TriMesh" checked:opt_convert
		checkbox chk_shadow "Test Shadow Validity" checked:opt_test_shadows
		colorpicker col_shadow "Color Invalid Objects:" align:#right color:disabled_shadow_color enabled:opt_test_shadows fieldwidth:20
		button btn_saveOptions "Save Options" width:145
		
	)
	
	button btn_setPos "Set Position" width:150
	
	on chk_lights changed value do (
	
		opt_create_lights = value
		
	)
	
	on chk_anim changed value do (
	
		opt_create_anim = value
		
	)
	
	on chk_snap changed value do (
	
		opt_snap_vertices = value
		
	)
	
	on chk_pivot changed value do (
	
		opt_adjust_pivot = value
		
	)
	
	on chk_convert changed value do (
	
		opt_convert = value
		
	)
	
	on chk_shadow changed value do (
	
		opt_test_shadows = value
		
		if (value) then (
		
			col_shadow.enabled = value
			col_shadow.color = invalid_shadow_color
			
		) else (
		
			col_shadow.enabled = false
			col_shadow.color = disabled_shadow_color
			
		)
		
	)
	
	on col_shadow changed value do (
	
		invalid_shadow_color = value
		
	)
	
	on rlo_tsPrefs open do (
	
		create_lights = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "tileSlicer" "createLights"
		create_anim_nodes = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "tileSlicer" "createAnimNodes"
		snap_verts = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "tileSlicer" "snapVerts"
		adjustPivots = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "tileSlicer" "adjustPivots"
		makeTrimesh = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "tileSlicer" "makeTrimesh"
		testShadows = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "tileSlicer" "testShadows"
		
		if (create_lights != undefined) and (create_lights != "") then (
		
			opt_create_lights = nx_itob (create_lights as integer)
			
		) else (
		
			opt_create_lights = true
			
		)
		
		if (create_anim_nodes != undefined) and (create_anim_nodes != "") then (
		
			opt_create_anim = nx_itob (create_anim_nodes as integer)
			
		) else (
		
			opt_create_anim = false
			
		)
		
		if (snapVerts != undefined) and (snapVerts != "") then (
		
			opt_snap_vertices = nx_itob (snapVerts as integer)
			
		) else (
		
			opt_snap_vertices = true
			
		)
		
		if (adjustPivots != undefined) and (adjustPivots != "") then (
		
			opt_adjust_pivot = nx_itob (adjustPivots as integer)
			
		) else (
		
			opt_adjust_pivot = true
			
		)
		
		if (makeTrimesh != undefined) and (makeTrimesh != "") then (
		
			opt_convert = nx_itob (makeTrimesh as integer)
			
		) else (
		
			opt_convert = true
			
		)
		
		if (testShadows != undefined) and (testShadows != "") then (
		
			opt_test_shadows = nx_itob (testShadows as integer)
			
		) else (
		
			opt_test_shadows = false
			
		)
		
		chk_lights.checked = opt_create_lights
		chk_anim.checked = opt_create_anim
		chk_snap.checked = opt_snap_vertices
		chk_pivot.checked = opt_adjust_pivot
		chk_shadow.checked = opt_test_shadows
		
	)
	
	on btn_saveOptions pressed do (
	
		local datalist = #()
		
		append datalist #("tileSlicer", "createLights", ((nx_btoi opt_create_lights) as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
		append datalist #("tileSlicer", "createAnimNodes", ((nx_btoi opt_create_anim) as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
		append datalist #("tileSlicer", "snapVerts", ((nx_btoi opt_snap_vertices) as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
		append datalist #("tileSlicer", "adjustPivots", ((nx_btoi opt_adjust_pivot) as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
		append datalist #("tileSlicer", "makeTrimesh", ((nx_btoi opt_convert) as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
		append datalist #("tileSlicer", "testShadows", ((nx_btoi opt_test_shadows) as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
		nx_setinivalue datalist
		
	)
	
	on btn_setPos pressed do (
	
		local datalist = #()
		
		append datalist #("tileSlicer", "ts_pos_x", (tsFloater.pos.x as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
		append datalist #("tileSlicer", "ts_pos_y", (tsFloater.pos.y as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
		nx_setinivalue datalist
		
	)
	
)

--	End Rollouts

--	Get the version information

version = maxVersion()
version_5000 = (version[1] >= 5000)

--	Grab the background color for when the shadow test is disabled

col = QuadMenuSettings.GetItemBackgroundColor 1 as string
col = filterstring col "( )"
disabled_shadow_color = [col[2] as float, col[3] as float, col[4] as float]

--	Close the old floater

if (tsFloater != undefined) do (
	
	closeRolloutFloater tsFloater
		
)
	
--	Create a new floater

tsFloater = newRolloutFloater title_string 200 600 0 100

addRollout rloTileSlicer tsFloater
addRollout rlo_tsPrefs tsFloater
