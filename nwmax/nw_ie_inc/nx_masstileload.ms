/*-----------------------------------------------------------------------------\

	Mass Tile Loader
	
	Loads tiles into the scene and positions them into a grid

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
     
\-----------------------------------------------------------------------------*/

global flagMassTileLoader

rollout nx_masstileload "Mass Tile Loader" width:162 (

	local groups
	
	group "File Details" (
	
		label lblSpacer_1 "Source Directory:" align:#left
		edittext edt_sourcedir width:140 align:#center
		button btn_browse "Browse" width:145 align:#center
		label lblSpacer_2 "Pattern:" align:#left
		edittext edt_pattern text:"*.mdl" width:140 align:#center
		label lbl_count "Model Count: " align:#center
		
	)
	
	group "Tile Details" (
	
		spinner spn_cols "Num Columns: " fieldwidth:40 align:#right type:#integer range:[1, 500, 10]
		spinner spn_space "Tile Spacing: " fieldwidth:40 align:#right type:#integer range:[0, 1000, 0] scale:10
		
	)
	
	checkbox chk_clear "Clear Scene" checked:true align:#left
	button btn_load "Load Tile Models" width:150 align:#center
	
	on nx_masstileload open do (
	
		flagMassTileLoader = true
		
	)
	
	on nx_masstileload close do (
	
		flagMassTileLoader = false
		
	)
	
	on btn_browse pressed do (
	
		edt_sourcedir.text = getSavePath()
		
		local model_path = edt_sourcedir.text
		
		if ((model_path == undefined) or (not nx_existDir(model_path))) then (
		
			messagebox "No Valid Path Provided!"
			
			return false
			
		)
		
		if ((model_path[model_path.count] != "\\") or (model_path[model_path.count] != "/")) then (
		
			model_path += "\\"
			
		)
		
		local models = getFiles (model_path + edt_pattern.text)
		
		lbl_count.caption = ("Model Count: " + models.count as string)
		
	)
	
	on btn_load pressed do (
	
		local columns
		local rowpl, columnpl, modelcount
		local pwk_file
		local dwk_file
		local model_path = edt_sourcedir.text
		
		--	Sanity checks
		
		if ((model_path == undefined) or (not nx_existDir(model_path))) then (
		
			messagebox "No Valid Path Provided!"
			
			return false
			
		)
		
		if ((model_path[model_path.count] != "\\") or (model_path[model_path.count] != "/")) then (
		
			model_path += "\\"
			
		)
		
		local models = getFiles (model_path + edt_pattern.text)
		
		columns = spn_cols.value
		
		if chk_clear.checked then (
		
			resetMaxFile #noPrompt
			
		)
		
		clearListener()
		
		--	Got the model names.
		
		rowpl = 0
		columnpl = 0
		modelcount = 0
		disableSceneRedraw()
		progressStart "Loading models"
		
		for model in models do (
		
			modelcount += 1.00
			progressUpdate ((modelcount * 100) / models.count)
			ImportNWNmdl model true false location:[(columnpl * (1000 + spn_space.value)), (rowpl * (1000 + spn_space.value)), 0]
			
			--	Import pwk file if available
			
			pwk_file = (getFilenamePath model) + (getFilenameFile model) + ".pwk"
			
			if (nx_existFile pwk_file) then (
			
				ImportNWNwk pwk_file "pwk"
				
			)
			
			--	Import dwk file if available
			
			dwk_file = (getFilenamePath model) + (getFilenameFile  model) + ".dwk"
			
			if (nx_existFile dwk_file) then (
			
				ImportNWNwk dwk_file "dwk"
				
			)
			
			if (columnpl >= (columns - 1)) then (
			
				columnpl = 0
				rowpl += 1
				
			) else (
			
				columnpl += 1
				
			)
			
		)
		
		progressEnd()
		enableSceneRedraw()
		
		--	Redraw gmax viewports
		
		clearSelection()
		
		max tool zoomextents all
		max views redraw
		
		gc()
		
	)
	
)

if (flagMassTileLoader != true) then (

	createDialog nx_masstileload
	
)
