/*-----------------------------------------------------------------------------\

	TVert Displacer.  For Lisa

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
     
\-----------------------------------------------------------------------------*/

global flagTVertDisplacer

rollout nx_tvert_displacer "TVert Displacer" width:162 (

	group "TVert Scale Data" (
	
		spinner spn_scalevalx "X-Axis: " fieldwidth:45 align:#right type:#float range:[0.0, 2.0, 1.0] scale:0.001
		spinner spn_scalevaly "Y-Axis: " fieldwidth:45 align:#right type:#float range:[0.0, 2.0, 1.0] scale:0.001
		
	)
	
	--	"Position for original texture"
	
	group "TVert Origin Adjustment" (
	
		checkbox chk_change "Change Position" align:#left checked:false
		spinner spn_otherx "Custom X Origin: " fieldwidth:45 align:#right type:#float range:[-1.0, 1.0, 0.0] scale:0.001 enabled:false
		spinner spn_othery "Custom Y Origin: " fieldwidth:45 align:#right type:#float range:[-1.0, 1.0, 0.0] scale:0.001 enabled:false
		
	)
	
	button btn_scalemove "Process TVerts" width:150 align:#center
	
	on nx_tvert_displacer open do (
	
		flagTVertDisplacer = true
		
	)
	
	on nx_tvert_displacer close do (
	
		flagTVertDisplacer = false
		
	)
	
	on chk_change changed val do (
	
		spn_otherx.enabled = val
		spn_othery.enabled = val
		
	)
	
	on btn_scalemove pressed do (
	
		local sel = selection as array
		local tvc		--	The tvert count
		local tvp		--	The tvert pos data
		local xscale
		local yscale
		local orig		--	Original object
		local isSkin = false
		local swl
		local weightBuffer
		local strStream
		
		--	Sanity checks
		
		if (sel.count < 1) then (
		
			messagebox "No Selection Found!"
			
			return false
			
		)
		
		--	Sort out and store the scale values
		
		xscale = spn_scalevalx.value
		yscale = spn_scalevaly.value
		
		if (xscale == 0) then (
		
			xscale = 1.0
			
		)
		
		if (yscale == 0) then (
		
			yscale = 1.0
			
		)
		
		disableSceneRedraw()
		
		for s in sel do (
		
			isSkin = false
			
			if (iskindof s Editable_mesh) then (
			
				if (s.modifiers["Skin"] != undefined) then (
				
					isSkin = true
					weightBuffer = #()
					
					--	Code from Bioware.  To export bone weights, modified
					--	for this particular purpose.
					
					max modify mode
					select s
					
					local i, j, zeroWeights
					local m = s.modifiers["Skin"]
					local n = skinops.getnumbervertices m
					
					for i = 1 to n do (
					
						local w_num = skinops.getvertexweightcount m i
						local counter = 0
						
						strStream = stringStream ""
						
						for j = 1 to w_num do (
						
							local bone_id   = (skinops.getvertexweightboneid m i j)
							local bone_name = "root"
							local weight_i_j = 1
							
							if(bone_id > 0) do (
							
								bone_name = (skinops.getbonename m bone_id 0)		--	Could be 1 or could be 0 UNSURE!
								
							)
							
							weight_i_j = (skinops.getvertexweight m i j)
							
							if (weight_i_j != 0.0) then (
							
								format "  % % "  bone_name weight_i_j to:strStream
								
								counter += 1
								
							)
							
						)
						
						if ((w_num == 0) or (counter == 0)) do (
						
							format "  root 1.00 " to:strStream
							
						)
						
						append weightBuffer (strStream as string)
						
					)
					
				)		--	End if : skin processing
				
				--	Sort out the stack as can't do anything if has modifiers
				
				orig = copy s
				
				try (		--	Collapse Stack
				
					collapsestack s
					
				) catch (
				
					--	Do Nothing Here
					
				)
				
				tvc = getNumTVerts s
				
				--	Start processing this in earnest
				
				for i = 1 to tvc do (
				
					tvp = getTVert s i
					
					--	Now scale the tvp data
					
					tvp.x = tvp.x * xscale
					tvp.y = tvp.y * yscale
					
					if chk_change.checked then (
					
						--	Adjust the origin
						
						tvp.x += spn_otherx.value
						tvp.y += spn_othery.value
						
					)		--	End if
					
					--	Set the tvert data back
					
					setTVert s i tvp
					
				)
				
				--	Restore original modifiers
				
				nx_copy_modifiers s orig ignore:#("Unwrap UVW", "Skin")
				delete orig
				
				--	If this is a skin the do skin post-processing
				
				if isSkin then (
				
					addModifier s (Skin())
					
					--	Apply weight info to the skin modifer
					
					addMDLSkin s weightBuffer
					
				)
				
			)
			
		)
		
		enableSceneRedraw()
		
	)
	
)

if (flagTVertDisplacer != true) then (

	createdialog nx_tvert_displacer
	
)
