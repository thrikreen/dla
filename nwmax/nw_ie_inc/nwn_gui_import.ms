/*-----------------------------------------------------------------------------\

	MDL Loading
	
	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
		---------------------------------------------------------------
		v5.05.27 --	Added reload last imported model functionality
		v5.05.29 --	Added functionality to "Reload"
		---------------------------------------------------------------
		v6.11.28 --	Fixed checkHide bug.
		
\-----------------------------------------------------------------------------*/

global dirName, fileName

--	ImportFile routines

filein (g_startpath + "nw_ie_inc/aurora_fn_import.ms")
filein (g_startpath + "nw_ie_inc/nwmax_plus.ms")

--	Import function for rollout and supporting utils

fn nx_importer = (

	local bDoAnims = true
	local bDoGeom = true
	local targetAnimBase = undefined
	local mdlFileName = fileName
	local animname = undefined		--	For loading single anims
	local insertpoint = 10
	
	case ImportRollout.rdo_options.state of (
	
		1: (
		
			bDoGeom = true
			bDoAnims = true
			
		)
		
		2: (
		
			bDoGeom = true
			bDoAnims = false
			
		)
		
		3: (
		
			bDoGeom = false
			bDoAnims = true
			
			if (ImportRollout.edt_animname.text.count > 0) then (
			
				animname = #()
				append animname (ImportRollout.edt_animname.text)
				
			)
			
			if (animname == undefined) then (
			
				insertpoint = 10
				
			) else (
			
				insertpoint = ImportRollout.spn_startframe.value
				
			)
			
			--	Check to make sure we have only a model base selected
			
			if ((nx_strclassof selection[1]) != "aurorabase") then (
			
				messageBox "No target model base selected for Animation Import" title:"No Selelction Found"
				
				return 0
				
			) else (
			
				targetAnimBase = selection[1]
				
			)
			
		)
		
	)
	
	if not (nx_existFile mdlFileName) then (
	
		messageBox "Model not imported.  " + mdlFileName + " does not exist." title:"File does not exist"
		
	) else (
	
		--	The call structure for the import function
		--		fn ImportNWNmdl pFile importAnims showWarnings loadGeom:true animLoadBase:undefined loadsingleanim:undefined insertpoint:0 mappingtbl:undefined
		
		clearListener()
		disableSceneRedraw()
		ImportNWNmdl mdlFileName bDoAnims ImportRollout.warn_checkbox.checked loadGeom:bDoGeom animLoadBase:targetAnimBase loadanimlist:animname insertpoint:insertpoint showprogress:true location:[ImportRollout.spn_loc_x.value, ImportRollout.spn_loc_y.value, 0]
		
		--	Import pwk file if available
		
		local pwk_file = (getFilenamePath mdlFileName) + (getFilenameFile  mdlFileName) + ".pwk"
		
		if (nx_existFile pwk_file) then (
		
			ImportNWNwk pwk_file "pwk"
			
		)
		
		--	Import dwk file if available
		
		local dwk_file = (getFilenamePath mdlFileName) + (getFilenameFile  mdlFileName) + ".dwk"
		
		if (nx_existFile dwk_file) then (
		
			ImportNWNwk dwk_file "dwk"
			
		)
		
		enableSceneRedraw()
		
		if ImportRollout.chk_resetctrl.checked then (
		
			local objSum = $objects.count * 2.0
			local cnt = 0.0
			
			progressStart "Reset Controllers"
			
			format "Setting to tcb rotation\r\n"
			
			for o in $objects do (
			
				o.rotation.controller = tcb_rotation()
				o.pos.controller = tcb_position()
				o.scale.controller = tcb_scale()
				cnt += 1
				
				if ((progressUpdate (((cnt * 100) / objSum) as integer)) == false) then (
				
					return 0
					
				)
				
			)
			
			format "Setting back to linear rotation\r\n"
			
			for o in $objects do (
			
				o.rotation.controller = linear_rotation()
				o.pos.controller = linear_position()
				o.scale.controller = linear_scale()
				cnt += 1
				
				if ((progressUpdate (((cnt * 100) / objSum) as integer)) == false) then (
				
					return 0
					
				)
				
			)
			
			progressEnd()
			
		)
		
		--	Redraw gMax viewports
		
		clearSelection()
		
		max tool zoomextents all
		max views redraw
		
		ImportRollout.progress.value = 0
		percent = 0
		gc()
		
	)
	
)

rollout ImportRollout "MDL Loading" (

	group "Filename" (
	
		edittext import_filename_edit "" width:140 align:#center readOnly:true
		button import_browse_button "Browse" width:140 align:#center
		
	)
	
	group "Options" (
	
		radioButtons rdo_options "" align:#left labels:#("Import Geom + Anims", "Import Geom Only", "Import Anims Only") default:1
		label lbl_animname "Anim Name to Load:" align:#center enabled:false
		edittext edt_animname width:140 align:#center enabled:false
		spinner spn_startframe "Insert Frame: " fieldwidth:65 align:#right type:#integer range:[0, 100000, 0] scale:1 enabled:false
		checkbox chk_resetctrl "Reset Controllers" align:#left checked:true
		checkbox warn_checkbox "Show warnings" align:#left
		spinner spn_loc_x "Import Loc X: " fieldwidth:65 align:#right type:#float range:[-1000000.0, 1000000.0, 0.0] scale:0.1
		spinner spn_loc_y "Import Loc Y: " fieldwidth:65 align:#right type:#float range:[-1000000.0, 1000000.0, 0.0] scale:0.1
		
	)
	
	button import_button "Import" width:150 align:#center enabled:false
	button btnReload "Reload" align:#center enabled:false width:150
	
	group "Specialist Tools" (
	
		button btn_removepointer "Delete Base Pointer" width:140 align:#center
		button btn_loadgrp "Load Tile Group" width:140 align:#center
		button btn_masstileload "Mass Tile Load" width:140 align:#center
		
	)
	
	progressBar progress "" width:146 height:14  align:#center
	
	on import_filename_edit changed newText do (
	
		fileName = newText
	
		import_button.enabled = true
		btnReload.enabled = false
	
	)
	
	on btn_loadgrp pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus/nw_ie_inc/nx_loadgrp.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_masstileload pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus/nw_ie_inc/nx_masstileload.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on rdo_options changed val do (
	
		if (val == 3) then (
		
			edt_animname.enabled = true
			lbl_animname.enabled = true
			spn_startframe.enabled = true
			
		) else (
		
			edt_animname.enabled = false
			lbl_animname.enabled = false
			spn_startframe.enabled = false
			
		)
		
	)
	
	on btn_removepointer pressed do (
	
		for obj in $objects do (
		
			if (matchPattern obj.name pattern:"ignore_NGon*") then (
			
				select obj
				
				max delete
				
			)
			
		)
		
	)
	
	on import_browse_button pressed do (
	
		if ((loadFilename = getOpenFileName caption:"Import MDL" types:"NWN Model (*.mdl)|*.mdl|NWN ASCII Model (*.ascii)|*.ascii|All Files (*.*)|*.*|") != undefined) then (
		
			dirName = getFilenamePath loadFilename
			fileName = filenameFromPath loadFilename
		
			import_filename_edit.text = fileName
			import_button.enabled = true
			
		) else (
		
			import_filename_edit.text = ""
			import_button.enabled = false
			
		)
		
	)
	
	on import_button pressed do (
	
		fileName = dirName + fileName
		
		nwmaxPlus.checkHide()
		nx_importer()
		
		import_button.enabled = false
		btnReload.enabled = true
		
	)
	
	on btnReload pressed do (
	
		max select all
		
		if (selection.count > 0) then (
		
			max select none
			
			if ((queryBox "Are you sure you want to Reload?" beep:true title:"Reload?") == true) then (
			
				nwmaxplus.checkHide()
				nx_importer()
			
			)
		
		) else (
		
			max select none
		
			nwmaxplus.checkHide()
			nx_importer()
			
		)
		
	)
	
)
