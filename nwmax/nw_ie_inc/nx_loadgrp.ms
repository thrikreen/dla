/*-----------------------------------------------------------------------------\

	Tile Group Loader
	
	Load groups of tiles and position them.
	Needs to read data from .set files.
	
	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
     
\-----------------------------------------------------------------------------*/

global flagLoadGroup

rollout nx_loadgrp "Tile Group Loader" width:162 (

	local groups
	
	group "SET File" (
	
		edittext edt_setfile "File: " width:145 align:#center
		button btn_browse "Browse" width:145 align:#center
		
	)
	
	listbox lst_grps "Groups:" width:150 height:10 align:#center
	button btn_load "Load Group" width:150 align:#center
	
	on nx_loadgrp open do (
	
		flagLoadGroup = true
		
	)
	
	on nx_loadgrp close do (
	
		flagLoadGroup = false
		
	)
	
	on btn_browse pressed do (
	
		local filename = getOpenFileName caption:"SET File" types:"NWN SET Files (*.set)|*.set|All Files (*.*)|*.*|"
		
		if (filename == undefined) then (
		
			filename = ""
			
		)
		
		edt_setfile.text = filename
		groups = #()
		
		--	Parse the groups from the set file
		
		local grp_count = (getINISetting filename "GROUPS" "Count") as integer
		local grp_name
		
		for i = 0 to (grp_count - 1) do (
		
			grp_name = getINISetting filename ("GROUP" + (i as string)) "Name"
			grp_name =  "[" + (i as string) + "]  " + grp_name
			append groups grp_name
			
		)
		
		lst_grps.items = groups
		lst_grps.selection = 1
		
	)
	
	on btn_load pressed do (
	
		local rows, columns, grp_number, filename, tile_name, tile_number
		local rowpl, columnpl, model_base
		local model_path
		local models = #()
		local sel_text = lst_grps.selected
		local data = filterString sel_text " []"
		
		filename = edt_setfile.text
		grp_number = data[1]
		rows = (getINISetting filename ("GROUP" + grp_number) "Rows") as integer
		columns = (getINISetting filename ("GROUP" + grp_number) "Columns") as integer
		
		for i = 0 to ((rows * columns) - 1) do (
		
			tile_number = getINISetting filename ("GROUP" + grp_number) ("Tile" + (i as string))
			
			if (tile_number != "-1") then (
			
				tile_name = getINISetting filename ("TILE" + tile_number) "Model"
				
			) else (
			
				tile_name = "RANDOM"
				
			)
			
			append models tile_name
			
		)
		
		clearListener()
		
		format "Models: %\r\n" models
		
		--	Got the model names.  Assume in same path as set file.
		
		model_path = getFilenamePath filename
		rowpl = 0
		columnpl = 0
		
		for model in models do (
		
			if (model != "RANDOM") then (
			
				ImportNWNmdl (model_path + model + ".mdl") true false
				
			)
			
			model_base = getNodeByName model
			
			if (model_base == undefined) then (
			
				format "ERROR: Can't Find Modelbase!\r\n"
				
			) else (
			
				model_base.pos = [(columnpl * 1000), (rowpl * 1000), 0]
				
			)
			
			if (columnpl >= (columns - 1)) then (
			
				columnpl = 0
				rowpl += 1
				
			) else (
			
				columnpl += 1
				
			)
			
		)
		
		--	Redraw gmax viewports
		
		clearSelection()
		
		max tool zoomextents all
		max views redraw
		
		gc()
		
	)
	
)

if (flagLoadGroup != true) then (

	createDialog nx_loadgrp
	
)
