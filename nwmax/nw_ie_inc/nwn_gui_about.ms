/*-----------------------------------------------------------------------------\

	NWmax About
	
	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
		---------------------------------------------------------------
		v6.11.13 --	Links to vote for NWMax and the CCG Wiki have been removed
					as they are no longer valid.
					Added link for TBotR's Facebook page.
     
\-----------------------------------------------------------------------------*/

rollout CreditDialog "Credits" width:326 height:280 (

	label lbl5 \
"This is based on, adapted from and an extension of code originally developed by:

BioWare
Joco
Wayland

Special Thanks and Acknowledgements to

  * DLA (ideas, mentoring and lots of bug reports)
  * roboius (assistance, code and \"The TileSlicer\")
  * Velmar (animation lists, code and \"Velmar's Tools\")
  * Torlack (\"nwnmdlcomp\" mdl compiler/decompiler)
  * simple tim (fixes posted in the Vault for the orginal
    Wayland script)
  * scooterpb (MDL Suite UI)
  * Zaddix (original WOK file importer)
  * Revinor (decoding the MDL format)" \
	pos:[21, 10] width:284 height:235
	button btn_close "Close" width:150 align:#center
	
	on btn_close pressed do (
	
		destroyDialog CreditDialog
		
	)
	
)

rollout AboutRollout "NWgMax Plus" (

	local bmap = g_startpath + "nw_ie_inc/nwgmax_plus_v1.bmp"
	local maskmap = g_startpath + "nw_ie_inc/nwmax_plus_v1_mask.bmp"
	
	button btn_nwmax "" width:160 height:70 align:#center images:#(bmap, maskmap, 1, 1, 1, 1, 1) toolTip:"Click for About/Credits."
	label lblNwgmaxPlus "" align:#center
	HyperLink hypNwmaxPlus "Visit TBotR" address:"http://www.tbotr.net/" align:#center toolTip:"Find out more information about NWgmax Plus"
	HyperLink hypFacebook "Visit us on Facebook" address:"https://www.facebook.com/The-Brotherhood-of-the-Realm-304591026230774/" align:#center toolTip:"Visit and Like us on Facebook"
	HyperLink hlink_docs "NWgmax+ Help" address:("file:///" + scriptsPath + "nwmax_plus\\docs\\index.html") align:#center toolTip:"Local NWgmax+ Documentation"
	
	on btn_nwmax pressed do (
	
		createDialog CreditDialog
		
	)
	
	on AboutRollout open do (
	
		lblNwgmaxPlus.text = gblVersion
		
	)
	
)
