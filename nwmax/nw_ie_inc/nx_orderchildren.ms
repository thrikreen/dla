/*-----------------------------------------------------------------------------\

	Order Child Nodes

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
     
\-----------------------------------------------------------------------------*/

--	UI

rollout nx_OrderChildren "Order Selected Node's Children" width:225 (

	listbox clist "Child Objects:" width:150 height:20 pos:[10, 5]
	button btn_up "Up" width:50 pos:[170, 121]
	button btn_down "Down" width:50 pos:[170, 171]
	button btn_ok "Done" width:150 pos:[38, 293]
	
	on nx_OrderChildren open do (
	
		local strList
		
		--	Initalise the dialog
		
		local sel = selection[1]
		
		if (sel == undefined) then (
		
			return 0
			
		)
		
		--	Build array of text strings for list
		
		strList = #()
		
		for c in sel.children do (
		
			append strList c.name
			
		)
		
		clist.items = strList
		
		if (strList.count > 0) then (
		
			clist.selection = 1
			
		)
		
	)
	
	on btn_up pressed do (
	
		format "btn_up::start\n"
		
		if (clist.selection == 1) then (
		
			return 0
			
		)
		
		local sel = selection[1]
		local list = clist.items
		
		--	Swap the text items over
		
		local index = clist.selection
		local strTemp = list[index]
		
		list[index] = list[index - 1]
		list[index - 1] = strTemp
		clist.items = list
		clist.selection = index - 1
		
		--	Swap the underlying child items over by using their parents
		
		format "btn_up:: swap objects\n"
		
		local childrenTemp = #()
		
		for c in sel.children do (
		
			append childrenTemp c
			
		)
		
		local objTmp = childrenTemp[index]
		
		childrenTemp[index] = childrenTemp[index - 1]
		childrenTemp[index - 1] = objTmp
		
		--	Now reset the parents of all the child nodes
		
		format "btn_up:: reset\n"
		
		for c in childrenTemp do (
		
			deleteItem sel.children c
			
		)
		
		--	Reapply the children in new order
		
		format "btn_up:: reassign\n"
		
		for c in childrenTemp do (
		
			append sel.children c
			
		)
		
		--	Release memory
		
		childrenTemp = #()
		
	)
	
	on btn_down pressed do (
	
		format "btn_down::start\n"
		
		if (clist.selection == clist.items.count) then (
		
			return 0
			
		)
		
		local sel = selection[1]
		local list = clist.items
		
		--	Swap the text items over
		
		format "btn_down:: swap text in list\n"
		
		local index = clist.selection
		local strTemp = list[index]
		
		list[index] = list[index + 1]
		list[index + 1] = strTemp
		clist.items = list
		clist.selection = index + 1
		
		--	Swap the underlying child items over by using their parents
		
		format "btn_down:: swap objects\n"
		
		local childrenTemp = #()
		
		for c in sel.children do (
		
			append childrenTemp c
			
		)
		
		local objTmp = childrenTemp[index]
		
		childrenTemp[index] = childrenTemp[index + 1]
		childrenTemp[index + 1] = objTmp
		
		--	Now reset the parents of all the child nodes
		
		format "btn_down:: reset\n"
		
		for c in childrenTemp do (
		
			deleteItem sel.children c
			
		)
		
		--	Reapply the children in new order
		
		format "btn_down:: reassign\n"
		
		for c in childrenTemp do (
		
			append sel.children c
			
		)
		
		--	Release memory
		
		childrenTemp = #()
		
	)
	
	on btn_ok pressed do (
	
		--	Finish
		
		DestroyDialog nx_OrderChildren
		
	)
	
)

createDialog nx_OrderChildren modal:true
