/*-----------------------------------------------------------------------------\

	Mass Action Utilities

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
     
\-----------------------------------------------------------------------------*/

global flagMassOperations

--	Mass Operations UI

rollout nx_massutils "Mass Operations" width:162 (

	group "Mass Sanity Check" (
	
		edittext edt_sanitypattern "File Pattern: " width:144 align:#center text:"*.mdl"
		button btn_massmdlsanity "Mass Sanity Check" width:144 align:#center
		
	)
	
	group "Mass Load and Export" (
	
		edittext edt_source "Source: " width:144 align:#center
		button btnBrowseSource "Browse" width:144 align:#center
		edittext edt_target "Target: " width:144 align:#center
		button btnBrowseTarget "Browse" width:144 align:#center
		checkbox chk_anims "Include Animations" align:#left
		button btn_massload "Load" width:144 align:#center
		button btn_massexport "Export" width:144 align:#center
		
	)
	
	on nx_massutils open do (
	
		flagMassOperations = true
		
		--	Load up any saved path values
		
		local sanity = getINISetting (scriptsPath + "nxlocalprefs.ini") "massutils" "mass_sanity_pattern"
		local src = getINISetting (scriptsPath + "nxlocalprefs.ini") "massutils" "mass_source_pattern"
		local trgt = getINISetting (scriptsPath + "nxlocalprefs.ini") "massutils" "mass_target_path"
		
		if (sanity != undefined) then (
		
			edt_sanitypattern.text = sanity
			
		)
		
		if (src != undefined) then (
		
			edt_source.text = src
			
		)
		
		if (trgt != undefined) then (
		
			edt_target.text = trgt
			
		)
		
	)
	
	on nx_massutils close do (
	
		local datalist = #()
		
		-- Each sub array contains the section, atrrib, value, file
		
		append datalist #("massutils", "mass_sanity_pattern", "\"" + edt_sanitypattern.text + "\"", (scriptsPath + "nxlocalprefs.ini"))
		append datalist #("massutils", "mass_source_pattern", "\"" + edt_source.text + "\"", (scriptsPath + "nxlocalprefs.ini"))
		append datalist #("massutils", "mass_target_path", "\"" + edt_target.text + "\"", (scriptsPath + "nxlocalprefs.ini"))
		
		--	Save field values for later reuse
		
		nx_setinivalue datalist
		flagMassOperations = false
		
	)
	
	on btn_massmdlsanity pressed do (
	
		massmdlsanity pattern:(edt_sanitypattern.text)
		
	)
	
	on btn_massload pressed do (
	
		--	massload pattern:<filepath\pattern> target:<target dir> {<anims:true|false>}
		
		massload pattern:(edt_source.text) target:(edt_target.text) anims:(chk_anims.checked)
		
	)
	
	on btn_massexport pressed do (
	
		--	massexport pattern:<filepath\pattern> target:<target dir> {<anims:true|false>}
		
		massexport pattern:(edt_source.text) target:(edt_target.text) anims:(chk_anims.checked)
		
	)
	
)

if (flagMassOperations != true) then (

	createDialog nx_massutils
	
)
