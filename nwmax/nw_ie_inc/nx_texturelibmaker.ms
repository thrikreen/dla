/*-----------------------------------------------------------------------------\

	Texture Library Maker

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
     
\-----------------------------------------------------------------------------*/

global flagTexLibMaker

rollout nx_texmanmaker "TexLib Maker" width:162 (

	local tga_list_name, tga_list
	
	--	UI
	
	listBox lst_textures "TGA Textures:" width:150 height:15 align:#center
	edittext edt_loaddir "Dir: " width:150 align:#center
	button btn_browse "Browse" width:150 align:#center
	label lblTGACount "TGA Count: " align:#center
	button btn_add "Add TGAs" width:150 align:#center
	
	group "Library Details" (
	
		edittext edt_libname "Name: " width:144 align:#center
		button btn_save "Save" width:144 align:#center
		
	)
	
	--	Actions
	
	on nx_texmanmaker open do (
	
		tga_list = #()
		tga_list_name = #()
		flagTexLibMaker = true
		
	)
	
	on nx_texmanmaker close do (
	
		flagTexLibMaker = false
		
	)
	
	on btn_browse pressed do (
	
		local dir = getSavePath caption:"TGA Files Location"
		local tgaCount
		
		if (dir != undefined) then (
		
			edt_loaddir.text = dir
			
			if ((dir[dir.count] != "/") and (dir[dir.count] != "\\")) then (
			
				dir += "\\"
				
			)
			
			tgaCount = getFiles(dir + "*.tga")
			lblTGACount.caption = "TGA Count: " + (tgaCount.count as string)
			
		)
		
	)
	
	on btn_add pressed do (
	
		--	Make sure exists
		
		if not nx_existDir(edt_loaddir.text) then (
		
			return false
			
		)
		
		local dir = edt_loaddir.text
		local files
		
		if ((dir[dir.count] != "/") and (dir[dir.count] != "\\")) then (
		
			dir += "\\"
			
		)
		
		files = getFiles(dir + "*.tga")
		
		for f in files do (
		
			append tga_list f
			
			append tga_list_name (getFilenameFile f)
			
		)
		
		--	Set the display list
		
		lst_textures.items = tga_list_name
		
	)
	
	on btn_save pressed do (
	
		--	Make sure we have an acceptable file name
		
		if ((edt_libname.text == undefined) or (edt_libname.text.count == 0)) then (
		
			messagebox "Invalid Library Name!"
			
			return false
			
		)
		
		try (
		
			local fs = createFile (scriptsPath + "nwmax_plus/tlibs/" + edt_libname.text + ".tlib")
			
		) catch (
		
			messageBox "Unable to Create Texture Library File!"
			
			return false
			
		)
		
		if (fs == undefined) then (
		
			--	Some file error
			
			messagebox "Unable to Create Texture Library File!"
			
			return false
			
		)
		
		for tga in tga_list do (
		
			format "%\n" (tga as string) to:fs
			
		)
		
		flush fs
		close fs
		
	)
	
)

if (flagTexLibMaker != true ) then (

	createDialog nx_texmanmaker
	
)
