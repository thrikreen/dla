/*-----------------------------------------------------------------------------\

	Texture Library Manager

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
     
\-----------------------------------------------------------------------------*/

global flagTexLibMan

rollout nx_texman "TexLib Loader" width:750 height:683 (

	local tlibFile
	local tlibTGAs
	
	--	UI
	
	listbox lst_libs "Texture Libraries:" width:200 height:10 pos:[10, 5]
	multiListBox lst_textures "TGA Textures:" width:200 height:20 pos:[10, 165]
	multiListBox lst_meditmats "Material Editor Slots:" width:200 height:15 pos:[10,455]
	label lblPreview "Preview:" pos:[221, 5]
	bitmap bmp_backgrnd pos:[221, 21] filename:(scriptsPath + "nwmax_plus/nw_ie_inc/520sq.jpg")
	ImgTag imgt_preview "Preview" width:512 height:512 pos:[225, 28] style:#bmp_center
	label lbl_info "" width:550 height:100 pos:[221, 547]
	button btn_addsel "Add Selected to Scene" width:124 pos:[221, 651]
	button btn_refreshmats "Refresh Material Slots" width:124 pos:[355, 651]
	button btn_rebuild "Rebuild Open Library" width:124 pos:[489, 651]
	
	--	Actions
	
	on nx_texman open do (
	
		--	Get the available texture libs
		
		tlibTGAs = #()
		
		local tlibs = #()
		local files = getFiles (scriptsPath + "nwmax_plus/tlibs/*.tlib")
		
		for f in files do (
		
			append tlibs (getFilenameFile f)
			
		)
		
		lst_libs.items = tlibs
		lst_libs.selection = 0
		
		--	Populate the materials editors list
		
		local mats = #()
		
		for i = 1 to 24 do (
		
			append mats (meditMaterials[i].name)
			
		)
		
		lst_meditmats.items = mats
		flagTexLibMan = true
		
	)
	
	on nx_texman close do (
	
		flagTexLibMan = false
		
	)
	
	on lst_libs selected val do (
	
		--	Load into the tga list all the tga names from the selected texture
		--	library
		
		local instr
		local fname = scriptsPath + "nwmax_plus/tlibs/" + lst_libs.items[val] + ".tlib"
		local fs = openFile fname mode:"r"
		local templist = #()
		
		if (fs == undefined) then (
		
			--	Can't open file
			
			messagebox "Unable to Open Texture Library File!"
			
			return false
			
		)
		
		--	Store the currently selected tlib file
		
		tlibFile = fname
		tlibTGAs = #()
		
		while not eof(fs) do (
		
			instr = readLine fs
			append tlibTGAs instr
			append templist (getFilenameFile instr)
			
		)
		
		lst_textures.items = templist
		
	)
	
	on lst_textures selected index do (
	
		if lst_textures.selection[index] then (
		
			local info = stringStream ""
			local bmp = openBitMap tlibTGAs[index]
			
			if (bmp != undefined) then (
			
				format "Location: %\nDepth: %\nFilesize: %\nKBSize: %x%" (tlibTGAs[index]) (nx_read_tga_bit tlibTGAs[index]) ((getFileSize tlibTGAs[index]) / 1024) (bmp.width) (bmp.height) to:info
				
				imgt_preview.bitmap = bmp
				lbl_info.text = info as string
				
			)
			
			freeSceneBitmaps()
			gc()
			
		)
		
	)
	
	on btn_addsel pressed do (
	
		--	Work through the selected textures and add them to the material
		--	editor
		
		local mat
		local matselindex = 0
		
		for s in lst_textures.selection do (
		
			clearListener()
			
			format "texture num:%  name:%\r\n" s lst_textures.items[s]
			
			matselindex += 1
			
			while (not (lst_meditmats.selection[matselindex])) and (matselindex < 24) do (
			
				matselindex += 1
				
			)
			
			if (matselindex < 25) then (
			
				nx_mat_from_tga tlibTGAs[s] lst_textures.items[s] index:matselindex
				
			)
			
		)
		
		--	Populate the materials editors list
		
		local mats = #()
		
		for i = 1 to 24 do (
		
			append mats (meditMaterials[i].name)
			
		)
		
		lst_meditmats.items = mats
		
	)
	
	on btn_refreshmats pressed do (
	
		--	Populate the materials editors list
		
		local mats = #()
		
		for i = 1 to 24 do (
		
			append mats (meditMaterials[i].name)
			
		)
		
		lst_meditmats.items = mats
		
	)
	
	on btn_rebuild pressed do (
	
		--	Cycle through all the unique dirs found in the texture list and use
		--	to rebuild the library from
		
		local dir_list = #()
		local this_dir
		local tga_list = #()
		local fs
		local files
		local templist = #()
		
		for f in tlibTGAs do (
		
			this_dir = getFilenamePath f
			
			if ((findItem dir_list this_dir) == 0) then (
			
				append dir_list this_dir
				
			)
			
		)
		
		--	Build up tga list
		
		for dir in dir_list do (
		
			files = getFiles(dir + "*.tga")
			
			for f in files do (
			
				append tga_list f
				
			)
			
		)
		
		try (
		
			fs = createFile tlibFile
			
		) catch (
		
			messageBox "No Texture Library Selected!"
			
			return false
			
		)
		
		if (fs == undefined) then (
		
			--	Some file error
			
			messagebox "Unable to Create Texture Library File"
			
			return false
			
		)
		
		for tga in tga_list do (
		
			format "%\n" (tga as string) to:fs
			
		)
		
		flush fs
		close fs
		
		--	Refresh the texture list
		
		tlibTGAs = #()
		
		for instr in tga_list do (
		
			append tlibTGAs instr
			append templist (getFilenameFile instr)
			
		)
		
		lst_textures.items = templist
		
	)
	
)

if (flagTexLibMan != true) then (

	createDialog nx_texman
	
)
