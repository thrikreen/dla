/*-----------------------------------------------------------------------------\

	Walkmesh Checker Plus
	
	NWMax Plus for gMax
	
	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.19 --	Original compilation version
		---------------------------------------------------------------
		v2.08.23 --	Changed the way the tool opens.
     
\-----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------\

	This tool executes the WOK sanity check function on a selected walkmesh.
	It then returns a list of suspect edges in a list box.  You can then select
	the listbox entries and have the edge or verts selected in the mesh.  You
	can then perform whatever corrective action you feel is needed.  Rinse and
	repeat until all WOK errors are either removed or deemed as immaterial by
	yourself.
	
	Plus version combines VelTools WOK-Checker.
	
\-----------------------------------------------------------------------------*/

rollout nx_woktool "Walkmesh Checker" (

	local woknode
	local subobj = 2
	
	fn wok_filter obj = (
	
		obj.modifiers["AuroraWalkmesh"] != undefined
		
	)
	
	--	UI
	
	label lbl_wok "NULL"
	pickbutton pck_wok "Select Walkmesh" width:150 align:#center filter:wok_filter
	button btn_checkwok "Check Walkmesh" width:150 align:#center enabled:false
	label lblStatus "Status: " align:#left
	listbox lstProblems width:150 height:10 align:#center
	
	--	Rollout Events
	
	on pck_wok picked obj do (
	
		woknode = obj
		lbl_wok.text = obj.name
		btn_checkwok.enabled = true
		
	)
	
	on btn_checkwok pressed do (
	
		--	Velmar's Walkmesh Checker
		
		local vertErrors = #()
		local edgeErrors = #()
		local cornerErrors = #()
		local pivotErrors = #()
		local faceErrors = #()
		local vertpos_array = #()
		local cornerTR = 0
		local cornerTL = 0
		local cornerBR = 0
		local cornerBL = 0
		local px = 0
		local py = 0

		lstProblems.items = #()

		lblStatus.caption = "Status: Checking " + woknode.name

		if ((woknode.parent != undefined) or (iskindof woknode.parent aurorabase)) then (

			myParent = woknode.parent

			--	Vert Check

			lblStatus.caption = "Status: Check Vertices"

			for v in woknode.verts do (

				px = abs((abs(myParent.pos.x) - abs(v.pos.x)))
				py = abs((abs(myParent.pos.y) - abs(v.pos.y)))

				if ((px > 500) or (py > 500)) then (

					append vertErrors ("#" + (v.index as string) + " - " + (v.pos.x as string) + ", " + (v.pos.y as string) + ", " + (v.pos.z as string))

				)

			)

			if (vertErrors.count > 0) then (

				lstProblems.items = append lstProblems.items ("-- " + (vertErrors.count as string) + " Vert Errors --")

				for e in vertErrors do (

					lstProblems.items = append lstProblems.items e

				)

				lstProblems.items = append lstProblems.items ("-- End Vert Errors --")

			)

			--	Open Edge Check

			lblStatus.caption = "Status: Check Open Edges"

			openEdges = meshop.getOpenEdges woknode
			woknode.selectedEdges = openEdges
			openEdgeVerts = meshop.getVertsUsingEdge woknode openEdges

			for v in openEdgeVerts do (

				px = abs((abs(myParent.pos.x) - abs(woknode.verts[v].pos.x)))
				py = abs((abs(myParent.pos.y) - abs(woknode.verts[v].pos.y)))
				append vertpos_array [-((myParent.pos.x) - (woknode.verts[v].pos.x)), -((myParent.pos.y) - (woknode.verts[v].pos.y))]

				if ((px != 500) and (py != 500)) then (

					append edgeErrors ((woknode.verts[v].pos.x as string) + ", " + (woknode.verts[v].pos.y as string) + ", " + (woknode.verts[v].pos.z as string))

				)

			)

			if (edgeErrors.count > 0) then (

				lstProblems.items = append lstProblems.items ("-- " + (edgeErrors.count as string) + " Edge Errors --")

				for e in edgeErrors do (

					lstProblems.items = append lstProblems.items e

				)

				lstProblems.items = append lstProblems.items ("-- End Edge Errors --")

			)

			--	Corner Check

			lblStatus.caption = "Status: Check Corners"

			for p in vertpos_array do (

				if (p == [500, 500]) then (

					cornerTL += 1

				) else if (p == [-500, 500]) then (

					cornerTR += 1

				) else if (p == [-500, -500]) then (

					cornerBR += 1

				) else if (p == [500, -500]) then (

					cornerBL += 1

				)

			)

			if (cornerTL != 1) then (

				if (cornerTL > 1) then (

					append cornerErrors ((cornerTL as string) + " Verts at position (500, 500)")

				) else (

					append cornerErrors ("No vert at position (500, 500)")

				)

			)

			if (cornerTR != 1) then (

				if (cornerTR > 1) then (

					append cornerErrors ((cornerTR as string) + " Verts at position (-500, 500)")

				) else (

					append cornerErrors ("No vert at position (-500, 500)")

				)

			)

			if (cornerBR != 1) then (

				if (cornerBR > 1) then (

					append cornerErrors ((cornerBR as string) + " Verts at position (-500, -500)")

				) else (

					append cornerErrors ("No vert at position (-500, -500)")

				)

			)

			if (cornerBL != 1) then (

				if (cornerBL > 1) then (

					append cornerErrors ((cornerBL as string) + " Verts at position (500, -500)")

				) else (

					append cornerErrors ("No vert at position (500, -500)")

				)

			)

			if (cornerErrors.count > 0) then (

				lstProblems.items = append lstProblems.items ("-- " + (cornerErrors.count as string) + " Corner Errors --")

				for e in cornerErrors do (

					lstProblems.items = append lstProblems.items e

				)

				lstProblems.items = append lstProblems.items ("-- End Corner Errors --")

			)

			--	Position Check

			lblStatus.caption = "Status: Check Pivot"

			if ((woknode.pos.x != myParent.pos.x) or (woknode.pos.y != myParent.pos.y)) then (

				append pivotErrors ("Pivot Not Set to Center")

			) else if ((mod woknode.pos.x 1 != 0.0) or (mod woknode.pos.y 1 != 0.0) or (mod woknode.pos.z 1 != 0.0)) then (

				append pivotErrors ("Pivot Position Not an Integer Value")

			)

			if (pivotErrors.count > 0) then (

				lstProblems.items = append lstProblems.items ("-- " + (pivotErrors.count as string) + " Pivot Errors --")

				for e in pivotErrors do (

					lstProblems.items = append lstProblems.items e

				)

				lstProblems.items = append lstProblems.items ("-- End Pivot Errors --")

			)

			--	Face Check

			lblStatus.caption = "Status: Check Faces"

			for face in woknode.faces do (

				for f in woknode.faces do (

					if (((getFace woknode face.index) == (getFace woknode f.index)) and (f.index != face.index)) then (

						append faceErrors ("#" + (f.index as string) + " - Double Face")

					)

				)

			)

			if (faceErrors.count > 0) then (

				lstProblems.items = append lstProblems.items ("-- " + (faceErrors.count as string) + " Face Errors --")

				for e in faceErrors do (

					lstProblems.items = append lstProblems.items e

				)

				lstProblems.items = append lstProblems.items ("-- End Face Errors --")

			)

		) else (

			lstProblems.items = #("Valid Parent Not Found")

		)

		if (lstProblems.items.count > 0) then (

			lblStatus.caption = "Status: Errors!!!"

		) else (

			lblStatus.caption = "Status: Walkmesh Passed!!!"

		)

	)

)

--	Close the old floater

if (wcFloater != undefined) do (
	
	closeRolloutFloater wcFloater
		
)
	
--	Create a new floater

wcFloater = newRolloutFloater "Walkmesh Checker" 192 320 0 100
addRollout nx_woktool wcFloater
addRollout rlo_wcPrefs wcFloater rolledUp:true
	
--	Position from Ini file
	
wcFloater.pos = gnx_pos_wokChecker
