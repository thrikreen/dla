/*-----------------------------------------------------------------------------\

    Aurora Trimesh Modifer
    
    This modifier stores the information needed by the Aurora engine for
    Trimeshes.
    
    BioWare noted in their original script that it would be better to create a
    NULL modifier in the SDK and them extend that.  Under GMAX we don't have
    that option unless you own the game developers SDK.  And I DON'T!   :-)
    
    NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.26 --	Original compilation version
     
\-----------------------------------------------------------------------------*/

format "Load: aurora_mod_trimesh.ms\r\n"

nx_progressPlus()

plugin SimpleMod auroratrimesh name:"AuroraTrimesh" classID:#(0x453cd5bb, 0x8ced9abf) version:1 (

	parameters paramblock rollout:auroraTrimeshParams (
	
		--	All the attributes that we want to save AND export for a trimesh
		--	need to be stored in this plug in.
		--	These attributes are:
		--		trimesh, scale, color inheritence, rotation, render options,
		--		tile fade, alpha channel options
		
		tilefadeprop			type:#integer		ui:radio_cap				default:1										--	Defaults to "Not a cap"
		scale					type:#float			ui:spn_scale				default:1.0
		render					type:#integer		ui:box_render				default:1										--	Defaults to render
		shadow					type:#integer		ui:box_shadow				default:0										--	Defaults to cast shadow
		beaming					type:#integer		ui:box_beaming				default:0										--	Defaults to off
		inheritcolor			type:#integer		ui:box_inheritcolor			default:0										--	Defaults to off
		rotatetexture			type:#integer		ui:box_rotatetexture		default:0					animatable:false	--	Defaults to off
		alpha					type:#float			ui:spn_alpha				default:1.0					animatable:true		--	Defaults to no opacity
		transparencyhint		type:#integer		ui:spn_transparencyhint		default:0					animatable:true
		
		--	Special data for materials
		
		selfillumcolor			type:#color			ui:colorpicker_selfillum	default:[0, 0, 0]			animatable:true		--	Defaults to Black
		extra_mat_data			type:#integer		ui:chk_xtra_mat_data		default:0
		ambient					type:#color			ui:colorpicker_ambient		default:[255, 255, 255]
		diffuse					type:#color			ui:colorpicker_diffuse		default:[255, 255, 255]
		specular				type:#color			ui:colorpicker_specular		default:[0, 0, 0]
		shininess				type:#integer		ui:spn_shininess			default:1
		
	)
	
	rollout auroraTrimeshParams "MDL Trimesh Parameters" (
	
		button btn_tex "" width:140 align:#center tooltip:"Applied Texture, press to select all objects that use this texture."
		button btn_parent "" width:140 align:#center tooltip:"Parent. Press to select."
		
		group "Tile Properties" (
		
			checkbox box_render "Render" align:#left
			checkbox box_shadow "Shadow" align:#left
			checkbox box_rotatetexture "Rotate Texture" align:#left
			checkbox box_beaming "Beaming" align:#left
			checkbox box_inheritcolor "Inherit Color" align:#left
			
		)
		
		group "Fading Tile Properties" (
		
			radiobuttons radio_cap "" align:#left labels:#("Not a cap", "Fadable", "Neighbour", "Base") columns:1
			
		)
		
		spinner spn_scale "Scale Factor: " fieldwidth:50 align:#right type:#float range:[0.0, 1000.0, 1.0] scale:0.1
		spinner spn_transparencyhint "Transparency Hint: " fieldwidth:30 align:#right type:#integer range:[0, 9, 0] scale:1
		spinner spn_alpha "Alpha: " fieldwidth:30 align:#right type:#float range:[0.0, 1.0, 1.0] scale:0.1

		group "Self Illumination Color" (
		
			colorPicker colorpicker_selfillum "" width:44 height:55 align:#left default:[0, 0, 0] across:2
			spinner spnSIRed "Red: " width:85 fieldwidth:40 align:#right type:#integer range:[0, 255, 0]
			spinner spnSIGreen "Green: " width:85 fieldwidth:40 align:#right offset:[0, -39] type:#integer range:[0, 255, 0]
			spinner spnSIBlue "Blue: " width:85 fieldwidth:40 align:#right type:#integer range:[0, 255, 0]
			button btn_selfillum_fromwire "Get SelfIllum From Wire Color" width:145 align:#center
		
		)
		
		--	Extra optional data
		
		group "Material Data" (
		
			checkbox chk_xtra_mat_data "Over-ride Mat Values" default:false
			
			groupBox gbxAmbient "Ambient Color" width:145 height:85 align:#center
			
			colorPicker colorpicker_ambient "" width:44 height:55 align:#left color:[255, 255, 255] enabled:false offset:[0, -70] across:2
			spinner spnAmbRed "Red: " width:85 fieldwidth:40 align:#right type:#integer range:[0, 255, 255] enabled:false offset:[0, -70]
			spinner spnAmbGreen "Green: " width:85 fieldwidth:40 align:#right offset:[0, -39] enabled:false type:#integer range:[0, 255, 255]
			spinner spnAmbBlue "Blue: " width:85 fieldwidth:40 align:#right type:#integer range:[0, 255, 255] enabled:false
			
			label lblSpacer_3 "" height:2
		
			groupBox gbxDiffuse "Diffuse Color" width:145 height:85 align:#center
			
			colorPicker colorpicker_diffuse "" width:44 height:55 align:#left color:[255, 255, 255] enabled:false offset:[0, -70] across:2
			spinner spnDifRed "Red: " width:85 fieldwidth:40 align:#right type:#integer range:[0, 255, 255] enabled:false offset:[0, -70]
			spinner spnDifGreen "Green: " width:85 fieldwidth:40 align:#right offset:[0, -39] enabled:false type:#integer range:[0, 255, 255]
			spinner spnDifBlue "Blue: " width:85 fieldwidth:40 align:#right type:#integer range:[0, 255, 255] enabled:false
			
			label lblSpacer_4 "" height:2
			
			groupBox gbxSpecular "Specular Color" width:145 height:85 align:#center
			
			colorPicker colorpicker_specular "" width:44 height:55 align:#left color:[0, 0, 0] enabled:false offset:[0, -70] across:2
			spinner spnSpecRed "Red: " width:85 fieldwidth:40 align:#right type:#integer range:[0, 255, 0] enabled:false offset:[0, -70]
			spinner spnSpecGreen "Green: " width:85 fieldwidth:40 align:#right offset:[0, -39] enabled:false type:#integer range:[0, 255, 0]
			spinner spnSpecBlue "Blue: " width:85 fieldwidth:40 align:#right type:#integer range:[0, 255, 0] enabled:false
			
			label lblSpacer_5 "" height:3
			
			spinner spn_shininess "Shininess: " fieldwidth:30 align:#right type:#integer enabled:false
		
		)
		
		--	Update Self Illumination Color
		
		on colorpicker_selfillum changed col do (
		
			spnSIRed.value = col.r
			spnSIGreen.value = col.g
			spnSIBlue.value = col.b
			
		)
		
		on spnSIRed changed val do (
		
			colorpicker_selfillum.color.r = val
			
		)
		
		on spnSIGreen changed val do (
		
			colorpicker_selfillum.color.g = val
			
		)
		
		on spnSIBlue changed val do (
		
			colorpicker_selfillum.color.b = val
			
		)
		
		--	End Self Illumination Update
		
		--	Update Ambient Color
		
		on colorpicker_ambient changed col do (
		
			spnAmbRed.value = col.r
			spnAmbGreen.value = col.g
			spnAmbBlue.value = col.b
			
		)
		
		on spnAmbRed changed val do (
		
			colorpicker_ambient.color.r = val
			
		)
		
		on spnAmbGreen changed val do (
		
			colorpicker_ambient.color.g = val
			
		)
		
		on spnAmbBlue changed val do (
		
			colorpicker_ambient.color.b = val
			
		)
		
		--	End Ambient Update
		
		--	Update Diffuse Color
		
		on colorpicker_diffuse changed col do (
		
			spnDifRed.value = col.r
			spnDifGreen.value = col.g
			spnDifBlue.value = col.b
			
		)
		
		on spnDifRed changed val do (
		
			colorpicker_diffuse.color.r = val
			
		)
		
		on spnDifGreen changed val do (
		
			colorpicker_diffuse.color.g = val
			
		)
		
		on spnDifBlue changed val do (
		
			colorpicker_diffuse.color.b = val
			
		)
		
		--	End Diffuse Update
		
		--	Update Specular Color
		
		on colorpicker_specular changed col do (
		
			spnSpecRed.value = col.r
			spnSpecGreen.value = col.g
			spnSpecBlue.value = col.b
			
		)
		
		on spnSpecRed changed val do (
		
			colorpicker_specular.color.r = val
			
		)
		
		on spnSpecGreen changed val do (
		
			colorpicker_specular.color.g = val
			
		)
		
		on spnSpecBlue changed val do (
		
			colorpicker_specular.color.b = val
			
		)
		
		--	End Specular Update
		
		fn get_material_name o = (
		
			local tempstring = ""
			local m
			
			if ((superclassof o) == GeometryClass) then (
			
				if (o.material != undefined) then (
				
					m = o.material
					
					--	Multi-Material support
					
					if (classof(m) == Multimaterial) then (
					
						try (
						
							local m_id = getFaceMatID o 1
							
							m = m[m_id]
							
						) catch (
						
							--	Do Nothing Here
							
						)
						
					)
					
					try (
					
						tempstring = m.name
						
						if (m.diffuseMap != undefined) then (
						
							tempstring = (getfilenamefile m.diffuseMap.filename)
							
						)
						
					) catch (
					
						tempstring = ""
						
					)
					
				) else (
				
					tempstring = ""		--	"No material set"
					
				)
				
			)
			
			tempstring = nx_lowercase (tempstring as string)
			
			return tempstring
			
		)
		
		on auroraTrimeshParams open do (
		
			if chk_xtra_mat_data.checked then (
			
				colorpicker_ambient.enabled = true
				spnAmbRed.enabled = true
				spnAmbGreen.enabled = true
				spnAmbBlue.enabled = true
				colorpicker_diffuse.enabled = true
				spnDifRed.enabled = true
				spnDifGreen.enabled = true
				spnDifBlue.enabled = true
				colorpicker_specular.enabled = true
				spnSpecRed.enabled = true
				spnSpecGreen.enabled = true
				spnSpecBlue.enabled = true
				spn_shininess.enabled = true
				
			)
			
			local parentname = ""
			
			if (selection[1].parent != undefined) then (
			
				parentname = selection[1].parent.name
				
			) else (
			
				parentname = "NONE"
				
			)
			
			btn_parent.caption = ("Parent: " + parentname)
			
			if (selection[1].material != undefined) then (
			
				try (
				
					btn_tex.caption = (get_material_name selection[1])
					
				) catch (
				
					--	Do Nothing Here
					
				)
				
			) else (
			
				btn_tex.caption = ""
				
			)
			
		)
		
		on btn_parent pressed do (
		
			if (selection[1].parent != undefined) then (
			
				undo on (
				
					select selection[1].parent
					
				)
				
			)
			
		)
		
		on btn_tex pressed do (
		
			if (btn_tex.caption == "") then (
			
				return 0
				
			) else (
			
				local selectme = #()
				local texname = nx_lowercase (btn_tex.caption)
				
				for obj in $geometry do (
				
					tempstring = get_material_name obj
					
					if (tempstring == texname) then (
					
						append selectme obj
						
					)
					
				)
				
				select selectme
				
			)
			
		)
		
		on auroraTrimeshParams close do (
		
			--	Do Nothing Here
			
		)
		
		on spn_alpha changed val do (
		
			local node
			local mat
			
			node = selection[1]
			
			if (node != undefined) then (
			
				mat = node.material
				
				if (mat != undefined) then (
				
					try (
					
						mat.opacity = val * 100
						
					) catch (
					
						format "spn_alpha: Opacity Set on Material Failed!\r\n"
						
					)
					
				)
				
			)
			
		)
		
		on chk_xtra_mat_data changed val do (
		
			colorpicker_ambient.enabled = val
			spnAmbRed.enabled = val
			spnAmbGreen.enabled = val
			spnAmbBlue.enabled = val
			colorpicker_diffuse.enabled = val
			spnDifRed.enabled = val
			spnDifGreen.enabled = val
			spnDifBlue.enabled = val
			colorpicker_specular.enabled = val
			spnSpecRed.enabled = val
			spnSpecGreen.enabled = val
			spnSpecBlue.enabled = val
			spn_shininess.enabled = val
			
		)
		
		on btn_selfillum_fromwire pressed do (
		
			local col = selection[1].wirecolor
			
			colorpicker_selfillum.color = col
			spnSIRed.value = col.r
			spnSIGreen.value = col.g
			spnSIBlue.value = col.b
			selfillumcolor = col
			
		)
		
	)
	
	on map i p do (
	
		--	We do nothing here
		--	Per BioWare:
		--	This is a simplemod so it expects us to be adjusting the point
		--	positions, but we dont want to do that.
		
	)
	
)
