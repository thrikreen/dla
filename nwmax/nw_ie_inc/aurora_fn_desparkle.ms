/*-----------------------------------------------------------------------------\

	Aurora Desparkle

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
		---------------------------------------------------------------
		v3.09.22 --	Fixed a bug with the Clean Model Routine
		v3.10.15 --	Fixed an issue that was causing the script to not run
					under 3DSMax5
     
\-----------------------------------------------------------------------------*/

fn nx_mesh_desparkle node = (

	if DEBUG then (
	
		format "#nx_mesh_desparkle\r\n"
		
	)
	
	--	Only fix IF is a trimesh.
	
	if ((canConvertTo node Editable_mesh) and ((classof node) == Editable_mesh)) then (
	
		--	Code from Bioware's scripts.  Takes care of sparkly-bit removal for
		--	tiles.  It then rounds off the vertices to the nearest centimeter.
		--	Work through the mesh and apply the vertice roundings.  To do this
		--	we need to collapse the mesh.  We keep a copy of the mesh hold all
		--	the modifiers and then reapply them later.
		
		local i
		local copy_of_meshnode = copy node
		local node_mods_count = (node.modifiers).count
		local badPivot = 0
		local checkPivot = 0
		
		if (node_mods_count > 0) then (
		
			if (node.modifiers[1].name == "AuroraTrimesh") then (
			
				if (node.modifiers[1].shadow == 1) then (
				
					checkPivot = 1
					
				)
				
			)
			
		)
		
		convertToMesh node
		
		--	Addition of zero orientation to "Desparkle" routine.
		
		if (nwmaxPlus.chk_mdaRotCheck.checked == true) then (
		
			if (node.rotation != (quat 0 0 0 1)) then (
			
				WorldAlignPivot node
				
				format "Orientation of % set to zero\r\n" node.name
				
			)
			
		)
		
		--	End zero orientation
		
		--	Original "Desparkle" routine
		
		if (nwmaxPlus.chk_mdaDesparkle.checked == true) then (
		
			for i = 1 to node.numverts do (
			
				local v, p3
				
				v = getvert node i
				
				--	We round to the nearest centimeter.  MaxScript doesn't have
				--	a round function, so we make our own.
				
				p3 = nx_roundp3 v
				setVert node i p3
				
			)
			
			update node
			
			format "Desparkle applied to %\r\n" node.name
			
		)
		
		--	End of "Desparkle"
		
		--	Addition of weld and autosmooth to "Desparkle" routine
		
		if (nwmaxPlus.chk_mdaWeldVerts.checked == true) then (
		
			if (meshop.weldVertsByThreshold node node.verts 0.1) then (
			
				format "Verts in % welded\r\n" node.name
				
				if (nwmaxPlus.chk_mdaAutoSmooth.checked == true) then (
				
					meshop.autoSmooth node node.faces 45.0
					
					format "AutoSmooth performed on %\r\n" node.name
					
				)
				
			)
			
		)
		
		--	End of weld and autosmooth
		
		--	Addition of delete unused tverts to "Desparkle" routine
		
		if (nwmaxPlus.chk_mdaWeldTVerts.checked == true) then (
		
			if ((meshop.deleteIsoMapVertsAll node) == "OK") then (
			
				format "Unused tverts in % deleted\r\n" node.name
				
			)
			
			matName = node.material as string
			
			if ((node.material == undefined) or ((substring matName 1 6) == "#Multi")) then (
			
				mapNum = meshop.getNumMaps node
				
				for x = 0 to mapNum by 1 do (
				
					if (meshop.getMapSupport node x) then (
					
						meshop.freeMapVerts node x
						
					)
					
				)
				
				format "Unused tverts in % deleted\r\n" node.name
				
				if (nwmaxPlus.chk_mdaNormalTVerts.checked == false) then (
				
					remainsFalse = true
					
				) else (
				
					nwmaxPlus.chk_mdaNormalTVerts.checked = false
					remainsFalse = false
					
				)
				
			) else (
			
				--	End of delete unused tverts
				
				--	Addition of weld and normalize tverts to "Desparkle" routine
				--	Help from Snoelk of the CGSociety forums)
				
				max modify mode
				
				tVerts = node.numtverts
				doWeld = 0
				
				for doOnce = 1 to (tVerts - 1) do (
				
					for doAgain = (doOnce + 1) to tVerts do (
					
						if ((getTVert node doOnce) == (getTVert node doAgain)) then (
						
							doWeld = 1
							
						)
						
						if (doWeld == 1) then (
						
							exit
							
						)
						
					)
					
					if (doWeld == 1) then (
					
						exit
						
					)
					
				)
				
				if (doWeld == 1) then (
				
					select node
					uvwUnwrap = Unwrap_UVW()
					addmodifier node uvwUnwrap
					nodeUnwrap = uvwUnwrap.unwrap
					nodeUnwrap.edit()
					subObjectLevel = 1
					
					max select all
					
					nodeUnwrap.setWeldThreshold 0.01
					nodeUnwrap.weldSelected()
					
					format "Tverts Welded in %\r\n" node.name
					
					collapseStack node
					clearSelection()
					
				)
				
			)
			
			--	End of weld tverts
			
			--	Addition of normalize tverts to "Desparkle" routine
			
			if (nwmaxPlus.chk_mdaNormalTVerts.checked == true) then (
			
				local uvwU, uvwV
				
				uvwNormal = false
				tVerts = node.numtverts
				uvw = getTVert node 1
				uvwU = uvw.x
				uvwV = uvw.y
				
				/*	Debug code.  Uncomment to use
				
				format "Pass 1 : uvwU : %, uvwV : %\r\n" uvwU uvwV
				
				*/
				
				for i = 2 to tVerts do (
				
					uvwTVert = getTVert node i
					
					if (uvwTVert.x < uvwU) then (
					
						uvwU = uvwTVert.x
						
					)
					
					if (uvwTvert.y < uvwV) then (
					
						uvwV = uvwTVert.y
						
					)
					
					/*	Debug code.  Uncomment to use
					
					format "Pass % : uvwU : %, uvwV : %\r\n" i uvwU uvwV
					
					*/
					
				)
				
				/*	Debug code.  Uncomment to use
				
				format "Final : uvwU : %, uvwV : %\r\n" uvwU uvwV
				
				*/
				
				if (((uvwU < 0) or (uvwU > 1)) or ((uvwV < 0) or (uvwV > 1))) then (
				
					for i = 1 to tVerts do (
					
						uvwTVert = getTVert node i
						
						if ((uvwU < 0) or (uvwU > 1)) then (
						
							uvwTVert.x -= uvwU
							
						)
						
						if ((uvwV < 0) or (uvwV > 1)) then (
						
							uvwTVert.y -= uvwV
							
						)
						
						setTVert node i uvwTVert
						
					)
					
					format "UVW values Normalized in %\r\n" node.name
					
				)
				
			)
			
			if (remainsFalse == false) then (
			
				nwmaxPlus.chk_mdaNormalTVerts.checked = true
				
			)
			
		)
		
		--	Addition of re-pivot to "Desparkle" routine
		
		if ((nwmaxPlus.chk_mdaRepivot.checked == true) and (checkPivot == 1)) then (
		
			matName = node.material as string
			
			if (((substring matName 1 6) == "#Multi")) then (
			
				if (node.pivot != [0, 0, 0]) then (
				
					node.pivot = [0, 0, 0]
					wasRePivot = 1
					
				)
				
			) else (

				i = 0			
				testCase = 0
				testNode = node
				testPivot = testNode.pivot
				numFaces = getNumFaces testNode
				
				--	Testing initial Pivots
				
				illegalPivot = true
				
				/*	Debug code.  Uncomment to use
				
				format "Mesh : %\r\n" testNode.name
				
				*/
				
				for i = 1 to numFaces do (
				
					while illegalPivot do (
					
						negTest = ((meshop.getFaceCenter testNode i) - testPivot) * (getFaceNormal testNode i)
						
						/*	Debug code.  Uncomment to use
						
						format "Face #% -- %\r\n" i negTest
						
						*/
						
						if ((negTest.x < 0) or (negTest.y < 0) or (negTest.z < 0)) then (
						
							illegalPivot = false
							
						)
						
					)
					
				)
				
				if (illegalPivot == false) then (
				
					numFaces = getNumFaces testNode
					faceCentroid = [0, 0, 0]
					
					for i = 1 to numFaces do (
					
						faceCentroid += (meshop.getFaceCenter testNode i)
						
					)
					
					testPivot = faceCentroid / numFaces
					
					--	Testing new Pivots
					
					illegalPivot = true
					pivotPassed = true
					
					while pivotPassed do (
					
						i = 1
						
						while illegalPivot and (i <= numFaces) do (
						
							negTest = ((meshop.getFaceCenter testNode i) - testPivot) * (getFaceNormal testNode i)
							
							if (testCase != priorTestCase) then (
							
								badPivot = 0
								
							)
							
							if (testCase > 0) then (
							
								case of (
								
									(testCase == 1): (
									
										if (negTest.x < priorNegTest.x) then (
										
											badPivot += 1
											
											if (badPivot == 1) then (
											
												negTest.x *= -1
												
											)
											
										)
										
									)
									
									(testCase == 2): (
									
										if (negTest.y < priorNegTest.y) then (
										
											badPivot += 1
											
											if (badPivot == 1) then (
											
												negTest.y *= -1
												
											)
											
										)
										
									)
									
									(testCase == 3): (
									
										if (negTest.z < priorNegTest.z) then (
										
											badPivot += 1
											
											if (badPivot == 1) then (
											
												negTest.z *= -1
												
											)
											
										)
										
									)
									
								)
								
								testCase = 0
								
								if (badPivot == 3) then (
								
									format "Bad Pivot on %\r\n" testNode.name
									
									nwmaxPlus.lst_mdaBadPivots.items = append mdaBadPivots testNode.name
									pivotPassed = true
									badPivot = 0
									
									exit
									
								)
								
							)
							
							/*	Debug code.  Uncomment to use
							
							format "Face #% -- %\r\n" i negTest
							
							*/
							
							if ((negTest.x < 0) or (negTest.y < 0) or (negTest.z < 0)) then (
							
								if (negTest.z < 0) then (
								
									if (negTest.z > -1) then (
									
										negTest.z = -1
										
									)
									
									if (testPivot.z > 0) then (
									
										testPivot.z += negTest.z
										
									) else (
									
										testPivot.z -= negTest.z
										
									)
									
									testCase = 3
									
								)
								
								if (negTest.y < 0) then (
								
									if (negTest.y > -1) then (
									
										negTest.y = -1
										
									)
									
									if (testPivot.y > 0) then (
									
										testPivot.y += negTest.y
										
									) else (
									
										testPivot.y -= negTest.y
										
									)
									
									testCase = 2
									
								)
								
								if (negTest.x < 0) then (
								
									if (negTest.x > -1) then (
									
										negTest.x = -1
										
									)
									
									if (testPivot.x > 0) then (
									
										testPivot.x += negTest.x
										
									) else (
									
										testPivot.x -= negTest.x
										
									)
									
									testCase = 1
									
								)
								
								i -= 1
								priorNegTest = negTest
								priorTestCase = testCase
								
								/*	Debug code.  Uncomment to use
								
								format "testCase : % -- priorNegTest : %\r\n" testCase priorNegTest
								format "Alternative Pivot : %\r\n" testPivot
								
								*/
								
							)
							
							if illegalPivot then (
							
								pivotPassed = false
								
							)
							
							i += 1
							
						)
						
						if pivotPassed then (
						
							testCase = 0
							badPivot = 0
							
							exit
							
						) else (
						
							format "Re-Pivoting % to %\r\n" testNode.name testPivot
							
							testNode.pivot = testPivot
							
						)
						
					)
					
				) else (
				
					format "% Pivot Passed\r\n" testNode.name
					
				)
				
			)
			
		)
		
		--	End of "Clean" Model additions
		
		update node
		
		--	Copy modifiers from node copy to original collapsed node.
		
		if (node_mods_count != 0) then (
		
			for i = node_mods_count to 1 by -1 do (
			
				addmodifier node copy_of_meshnode.modifiers[i]
				
			)
			
		)
		
		delete copy_of_meshnode
		gc light:true delayed:true
		
		--	End sparkly bit removal
		
	) else (
	
		format "Node [%] not Editable_Mesh, skipped.\r\n" node.name
		
	)
	
	for child in node.children do (
	
		nx_mesh_desparkle child
		
	)
	
)
