/*-----------------------------------------------------------------------------\

    Aurora Reference Helper Object

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.26 --	Original compilation version
		---------------------------------------------------------------
		v6.11.12 --	Helpers are now being categorized under "Neverwinter
					Nights"
     
\-----------------------------------------------------------------------------*/

format "Load: aurora_helper_reference.ms\r\n"

nx_progressPlus()

plugin helper aurorareference name:"AuroraRef" classID:#(0x994096b0, 0x2b62706c) version:1 extends:Dummy category:"Neverwinter Nights" replaceUI:true invisible:true (

	parameters main rollout:params (
	
		refModel			type:#string		default:"fx_ref"		ui:edit_refModel
		reattachable		type:#integer		default:1				ui:box_reattachable
		
	)
	
	rollout params "MDL Reference Parameters" (
	
		edittext edit_refModel "Ref Model: " width:140 align:#center
		checkbox box_reattachable "Reattachable" align:#left
		
	)
	
)

plugin helper aurorarefbox name:"AuroraRef" category:"Neverwinter Nights" (

	local mdaReference
	
	tool create (
	
		on mousePoint click do (
		
			if (click == 1) then (
			
				in coordsys grid (
				
					--	We only ever do this once.
					
					mdaReference = aurorareference pos:[0, 0, 0]
					mdaReference.pos = gridPoint
				
				)
				
				mdaReference.delegate.boxSize = [10, 10, 10]
				
				local objCone = cone height:-25 pos:gridPoint heightsegs:1 sides:4 smooth:false
				
				objCone.wireColor = (color 92 179 240)
				objCone.parent = mdaReference
				objCone.name = "ignore_" + objCone.name
				select mdaReference
				
				--	Enter modify mode to exit mouse tool and get ready for name
				--	updates
				
				max modify mode
			
			)
			
			return #stop
		
		)
	
	)
	
)
