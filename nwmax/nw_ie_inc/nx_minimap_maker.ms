/*-----------------------------------------------------------------------------\

	Tile MiniMap Maker (3DSMax Only for now)

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
		---------------------------------------------------------------
		v6.11.13 --	Fix to the Minimap Maker.  Now looks for the proper
					lowercase function.
     
\-----------------------------------------------------------------------------*/

global flagMiniMapMaker

--	MiniMap Maker UI

rollout nx_tile_minimap "Tile MiniMap Maker" width:162 (

	local silentmode = true
	
	--	BioWare's generic black texture
	
	local bmp_black = standardmaterial diffuse:(color 0 0 0) ambient:(color 0 0 0)
	
	--	Define UI
	
	group "Source Data" (
	
		edittext edt_source_path "Dir: " width:144 align:#center
		button btn_src "Browse" width:144 align:#center tooltip:"Browse to the directory with the models to process."
		edittext edt_pattern "Filter: " width:144 align:#center text:"*.mdl"
		label lblModelCount "Model Count: " align:#center
		label lblGroupCount "Group Count: " align:#center
		
	)
	
	group "Destination Data" (
	
		edittext edt_target_path "Dir: " width:144 align:#center
		button btn_trg "Browse" width:144 align:#center tooltip:"Browse to the target directory for the created Mapimages."
		radiobuttons rdo_size "MiniMap Size:" align:#left labels:#("16x16", "32x32", "64x64", "128x128") default:1
		
	)
	
	checkbox chk_silentmode "Only print errors" align:#left checked:silentmode
	button btn_makemaps "MiniMap All Models" width:150 align:#center tooltip:"Create MiniMaps for all models in the directory."
	button btn_dogroups "MiniMap Groups Only" width:150 align:#center tooltip:"Search for a SET file and create MiniMaps for all groups in the SET file."
	
	on nx_tile_minimap open do (

		flagMiniMapMaker = true

	)

	on nx_tile_minimap close do (

		flagMiniMapMaker = false

	)

	fn nx_load_group sel_text filename = (

		local rows, columns, grp_number, filename, tile_name, tile_number
		local rowpl, columnpl, model_base
		local model_path
		local models = #()
		local retarray = #()
		local data = filterString sel_text " []"

		grp_number = data[1]
		rows = (getINISetting filename ("GROUP" + grp_number) "Rows") as integer
		columns = (getINISetting filename ("GROUP" + grp_number) "Columns") as integer
		append retarray rows
		append retarray columns

		for i = 0 to ((rows * columns) - 1) do (

			tile_number = getINISetting filename ("GROUP" + grp_number) ("Tile" + (i as string))

			if (tile_number != "-1") then (

				tile_name = getINISetting filename ("TILE" + tile_number) "Model"

			) else (

				tile_name = "RANDOM"

			)

			append models tile_name
			append retarray tile_name

		)

		if not silentmode then (

			format "Models: %\r\n" models

		)

		--	Got the model names.  Assume in same path as set file.

		model_path = getFilenamePath filename
		rowpl = 0
		columnpl = 0

		for model in models do (

			if (model != "RANDOM") then (

				ImportNWNmdl (model_path + model + ".mdl") false false loadGeom:true loadtiff:false

			)

			model_base = getNodeByName model

			if (model_base == undefined) then (

				format "ERROR: Can't Find Modelbase!\r\n"

			) else (

				model_base.pos = [(columnpl * 1000), (rowpl * 1000), 0]

			)

			if (columnpl >= (columns - 1)) then (

				columnpl = 0
				rowpl += 1

			) else (

				columnpl += 1

			)

		)

		gc()
		retarray

	)

	fn nx_render_tile imagename scaleit:true = (

		local bmp, scaled_bmp
		local logvar
		local UVlog

		backgroundColor = color 0 255 0 0
		bgc = color 0 255 0 0
		bmp = render frame:0 outputsize:[256, 256] renderer:#production renderType:#normal antiAliasing:false

		local h = bmp.height
		local w = bmp.width
		local r		--	Row counter
		local c		--	Column counter
		local rowdata
		local image = #()
		local l_edge, r_edge, t_edge, b_edge
		local sc_w, sc_h

		--	Build up image data

		for r = 0 to (h - 1) do (

			rowdata = getPixels bmp [0, r] w
			append image rowdata

		)

		--	Clean up # 1

		close bmp
		rowdata = undefined
		gc()

		if not silentmode then (

			format "bmp width: %  height: %\r\n" w h

		)

		--	Do collison testing for left edge

		l_edge = w - 1

		for r = 0 to (h - 1) do (

			for c = 0 to (w - 1) do (

				if ((image[r + 1][c + 1] != bgc) and (c < l_edge)) then (

					l_edge = c + 1

				)

			)

		)

		--	Do collison testing for right edge

		r_edge = 0

		for r = 0 to (h - 1) do (

			for c = (w - 1) to 0 by -1 do (

				if ((image[r + 1][c + 1] != bgc) and (c > r_edge)) then (

					r_edge = c

				)

			)

		)

		--	Do collison testing for top edge

		t_edge = h - 1

		for c = 0 to (w - 1) do (

			for r = 0 to (h - 1) do (

				if ((image[r + 1][c + 1] != bgc) and (r < t_edge)) then (

					t_edge = r + 1

				)

			)

		)

		--	Do collison testing for bottom edge

		b_edge = 0

		for c = 0 to (w - 1) do (

			for r = (h - 1) to 0 by -1 do (

				if ((image[r + 1][c + 1] != bgc) and (r > b_edge)) then (

					b_edge = r

				)

			)

		)

		if not silentmode then  (

			format "name: %\r\n" imagename
			format "left edge: %\r\n" l_edge
			format "right edge: %\r\n" r_edge
			format "top edge: %\r\n" t_edge
			format "bottom edge: %\r\n" b_edge

		)

		--	Sanity checks

		if ((l_edge > r_edge) or (t_edge > b_edge)) then (

			format "Error - Nothing to Render!\r\n"

			return false

		)

		--	Build new bitmap

		local new_w, new_h		--	New calculated width and height
		local new_rowdata = #()

		new_w = (r_edge - l_edge) + 1
		new_h = (b_edge - t_edge) + 1
		bmp = bitmap new_w new_h color:black

		for r = t_edge to b_edge do (

			for c = l_edge to r_edge do (

				append new_rowdata image[r + 1][c + 1]

			)

			--	Ensure any bgc set pixels are now black (0 0 0 0)

			for rd = 1 to new_rowdata.count do (

				if (new_rowdata[rd] == bgc) then (

					new_rowdata[rd] = color 0 0 0 0

				)

			)

			--	Set pixel

			setPixels bmp [0, (r - t_edge)] new_rowdata
			new_rowdata = #()

		)

		--	Determine scale target

		if (scaleit == true) then (

			case rdo_size.state of (

				1: (

					sc_w = 16
					sc_h = 16

				)

				2: (

					sc_w = 32
					sc_h = 32

				)

				3: (

					sc_w = 64
					sc_h = 64

				)

				4: (

					sc_w = 128
					sc_h = 128

				)

			)

		) else (

			sc_w = new_w
			sc_h = new_h

		)

		if not silentmode then (

			format "Save as: %\r\n" imagename
			format "Scaled image size: %, %\r\n" sc_w sc_h

		)

		scaled_bmp = bitmap sc_w sc_h color:black filename:imagename
		copy bmp scaled_bmp
		save scaled_bmp

		--	Clean up # 2

		close bmp
		image = undefined
		new_rowdata = undefined
		scaled_bmp

	)

	fn nx_make_tile_minimap = (

		local chk_map

		--	Make sure the stack and heap is really large!!

		if (heapSize < 7500000) then (

			heapSize = 7500000

		)

		if (stackLimit < 4000000) then (

			stackLimit = 4000000

		)

		local map_tga

		--	Process all the files in a specified directory

		local dir_path = edt_source_path.text
		local targetdir = edt_target_path.text

		--	Basic sanity check

		if ((dir_path == "") or (targetdir == "")) then (

			messageBox "Source or Target directory not Selected!"

			return false

		)

		--	Correct lack of trailing /

		if ((dir_path[dir_path.count] != "/") and (dir_path[dir_path.count] != "\\")) then (

			dir_path += "\\"

		)

		if ((targetdir[targetdir.count] != "/") and (targetdir[targetdir.count] != "\\")) then (

			targetdir += "\\"

		)

		local filenum = 0

		local modelname

		progressStart "Making Mini Maps"

		local files = getFiles (dir_path + (edt_pattern.text))

		--	Work through the files.  Load and render, repeat until done

		local l, w, h, selected_objects, obj, slicing_box

		for f in files do (

			filenum += 1

			if ((progressUpdate ((filenum * 100) / files.count)) == false) then (

				--	Stop the loop

				exit

			)

			--	Setup

			gc()
			disableSceneRedraw()
			resetMaxFile #noPrompt

			if doesFileExist f then (

				ImportNWNmdl f false false loadGeom:true loadtiff:false

			)

			enableSceneRedraw()

			--	Clip off any mesh that is outside the x, y plane of -500, +500
			--	around 0, 0, 0.  Do this using a boolean operation with a
			--	bounding dummy object.  Also make the dummy object VERY high.
			--	Make a slicing box big enough to cover the selected objects.

			max select all

			l = 1000
			w = 1000
			h = selection.max.z - selection.min.z + 2000

			bottom = selection.min.z - 10
			objlist = $objects as array

			for obj in objlist do (

				if (iskindof obj Editable_mesh) then (

					--	Copy of original so we can get the modifiers back

					orig = copy obj

					--	Create a slicing box.

					slicing_box = box length:l width:w height:h position:[0, 0, bottom]

					--	Do a cut/slice and remove boolean operation.

					boolObj.createBooleanObject obj slicing_box 4 5
					boolObj.setBoolOp obj 5
					boolObj.setBoolCutType obj 4

					--	Convert to mesh and restore nwmax modifiers

					convertToMesh obj

					--	Restore modifiers

					nx_copy_modifiers obj orig ignore:#("Skin")
					delete orig

				)

			)

			--	Work out which mesh objects should not be rendered
			--	1.	Walkmeshes
			--	2.	Any mesh with a tga that has an alpha channel
			--	3.	Any mesh with a trimesh modifier with "No Render" set

			for o in $objects do (

				if (iskindof o Editable_mesh) then (

					--	Is it a walkmesh

					if (o.modifiers["AuroraWalkmesh"] != undefined) then (

						setRenderable o false

					) else if ((o.mat != undefined) and (o.mat.diffusemap != undefined)) then (

						if (nx_read_tga_bit(o.mat.diffusemap.filename) == 32) then (

							--	Depth of 32 means an alpha channel

							setRenderable o false

						)

					)

					--	Is there a trimesh modifier

					if (o.modifiers["AuroraTrimesh"] != undefined) then (

						if ((o.modifiers["AuroraTrimesh"].render) == 0) then (

							setRenderable o false

						)

					)

					--	Edit by Velmar
					--	Changed lowercase function to nx_lowercase MDA

					if (o.material != undefined) then (

						chk_mat = o.material.name
						chk_mat = nx_lowercase chk_mat

						if (chk_mat == "black") then (

							o.material = bmp_black

							format "Applied generic texture 'Black' \r\n"

						)

					)

					if (getNumTverts o < 1) then (

						addModifier o (UVWmap())

					)

				)

				--	Get modelbase name

				if (iskindof o aurorabase) then (

					modelname = o.name

				)

			)

			disableSceneRedraw()
			viewport.setLayout #layout_1
			viewport.setType #view_top

			max tool zoomextents

			enableSceneRedraw()
			map_tga = nx_render_tile (targetdir + "mi_" + modelname + ".tga")

			if (map_tga != false) then (

				close map_tga

			)

			gc()

		)

		progressEnd()

	)

	fn nx_process_groups = (

		--	Make sure the stack and heap is really large!!

		if (heapSize < 7500000) then (

			heapSize = 7500000

		)

		if (stackLimit < 4000000) then (

			stackLimit = 4000000

		)

		local map_tga
		local retvalarray

		--	Process all the files in a specified directory

		local dir_path = edt_source_path.text
		local targetdir = edt_target_path.text

		--	Basic sanity check

		if ((dir_path == "") or (targetdir == "")) then (

			messageBox "Source or Target directory not Selected!"

			return false

		)

		--	Correct lack of trailing /

		if ((dir_path[dir_path.count] != "/") and (dir_path[dir_path.count] != "\\")) then (

			dir_path += "\\"

		)

		if ((targetdir[targetdir.count] != "/") and (targetdir[targetdir.count] != "\\")) then (

			targetdir += "\\"

		)

		local filenum = 0
		local modelname

		--	Process the groups using the .set file definition
		--	1.	If a SET is found, continue

		filenameSET = getFiles (dir_path + "*.set")

		format "Start processing groups from SET file: %\r\n" filenameSET[1]

		if (filenameSET.count < 1) then (

			format "ERROR: Could Not Find SET File!\r\n"

			return false

		)

		filenameSET = filenameSET[1]

		--	2.	Get the group list from the SET file

		local groups = #()
		local grp_count = (getINISetting filenameSET "GROUPS" "Count") as integer
		local grp_name

		for i = 0 to (grp_count - 1) do (

			grp_name = getINISetting filenameSET ("GROUP" + (i as string)) "Name"
			grp_name =  "[" + (i as string) + "]  " + grp_name
			append groups grp_name

		)

		if not silentmode then (

			format "Group count = %, names: %\r\n" grp_count groups

		)

		--	3.	Work through the groups

		for g in groups do (

			if not silentmode then (

				format "Get group: %\r\n" g

			)

			disableSceneRedraw()
			resetMaxFile #noPrompt
			retvalarray = nx_load_group g filenameSET

			--	EDIT BY Velmar - workaround for script stop at
			--	'nothing to render'.  Creates a red plane instead

			if ($geometry.count < 1) then (

				format "ERROR: File % Does Not Exist!\r\n" retvalarray

				p = plane length:1000 width:1000
				p.wirecolor = red
				convertToMesh p

			) else if not silentmode then (

				format "Group made of: %\r\n" retvalarray

			)

			--	Set up the view port correctly for rendering

			viewport.setLayout #layout_1
			viewport.setType #view_top

			max tool zoomextents

			enableSceneRedraw()

			--	Work out which mesh objects should not be rendered
			--	1.	Walkmeshes
			--	2.	Any mesh with a tga that has an alpha channel

			for o in $objects do (

				if (iskindof o Editable_mesh) then (

					--	Is it a walkmesh

					if (o.modifiers["AuroraWalkmesh"] != undefined) then (

						setRenderable o false

					) else if ((o.mat != undefined) and (o.mat.diffusemap != undefined)) then (

						if (nx_read_tga_bit(o.mat.diffusemap.filename) == 32) then (

							--	Depth of 32 means an alpha channel

							setRenderable o false

						)

					)

					--	Is there a trimesh modifier

					if (o.modifiers["AuroraTrimesh"] != undefined) then (

						if ((o.modifiers["AuroraTrimesh"].render) == 0) then (

							setRenderable o false

						)

					)

					--	Edit by Velmar
					--	Changed lowercase function to nx_lowercase MDA

					if (o.material != undefined) then (

						chk_mat = o.material.name
						chk_mat = nx_lowercase chk_mat

						if (chk_mat == "black") then (

							o.material = bmp_black

							format "Applied generic texture 'Black' \r\n"

						)

					)

					if (getNumTverts o < 1) then (

						addModifier o (UVWmap())

					)

				)

				--	Get modelbase name

				if (iskindof o aurorabase) then (

					modelname = o.name

				)

			)

			--	For groups we don't want things scaled in the render routine.
			--	We scale after chopping

			map_tga = nx_render_tile (targetdir + "group_" + (g as string) + ".tga") scaleit:false

			if (map_tga == false) then (

				gc()

				format "map_tga == FALSE\r\n"

				return false

			) else (

				if not silentmode then (

					format "map_tga == a bitmap\r\n"

				)

			)

			--	Chop the returned tga into the correct parts

			local rows = retvalarray[1]
			local columns = retvalarray[2]

			if not silentmode then (

				format "grp rows=%\r\n" rows
				format "grp columns=%\r\n" columns

			)

			deleteItem retvalarray 2
			deleteItem retvalarray 1

			if not silentmode then (

				format "grp model list: %\r\n" retvalarray

			)

			local cut_width = (map_tga.width) / columns
			local cut_height = (map_tga.height) / rows

			if not silentmode then (

				format "cut_width = %\r\n" cut_width
				format "cut_height = %\r\n" cut_height
				format "map_tga.width = %\r\n" (map_tga.width)
				format "map_tga.height = %\r\n" (map_tga.height)

			)

			local cut_pos_x, cut_pos_y, sc_w, sc_h
			local imagename
			local rowpl = 0
			local columnpl = 0
			local pixdata = #()

			for model in retvalarray do (

				--	Model to cut, scale and save

				if (model != "RANDOM") then (

					--	Calculate upper left position of subimage

					cut_pos_x = (cut_width * columnpl)
					cut_pos_y = (cut_height * (rows - rowpl - 1))		--	Velmar edits

					--	Build up the bmp piece

					cutbmp = bitmap cut_width cut_height color:black

					for r = cut_pos_y to (cut_pos_y + cut_height) do (

						pixdata = getPixels map_tga [cut_pos_x, r] cut_width
						setPixels cutbmp [0, r - cut_pos_y] pixdata

					)

					--	Scale the cutbmp

					case rdo_size.state of (

						1: (

							sc_w = 16
							sc_h = 16

						)

						2: (

							sc_w = 32
							sc_h = 32

						)

						3: (

							sc_w = 64
							sc_h = 64

						)

						4: (

							sc_w = 128
							sc_h = 128

						)

					)

					--	Set the image save name

					imagename = targetdir + "mi_" + model + ".tga"

					format "Save as: %\r\n" imagename

					scaled_bmp = bitmap sc_w sc_h color:black filename:imagename
					copy cutbmp scaled_bmp
					save scaled_bmp

					--	Clean up

					close cutbmp
					close scaled_bmp

				)

				if (columnpl >= (columns - 1)) then (

					columnpl = 0
					rowpl += 1

				) else (

					columnpl += 1

				)

			)

			if (map_tga != false) then (

				--	Close the bmp

				close map_tga

			)

			gc()

		)

	)

	on chk_silentmode changed val do (

		silentmode = val

	)

	on btn_src pressed do (

		local p = getsavepath()

		if (p != undefined) then (

			edt_source_path.text = p
			edt_target_path.text = p
			
			local models = getFiles (edt_source_path.text + "\\" + edt_pattern.text)
			
			lblModelCount.caption = ("Model Count: " + models.count as string)
		
		)

	)

	on btn_trg pressed do (

		local p = getsavepath()

		if (p != undefined) then (

			edt_target_path.text = p

		)

	)

	on btn_makemaps pressed do (

		nx_make_tile_minimap()

	)

	on btn_dogroups pressed do (

		clearListener()
		nx_process_groups()

	)
	
)

if (flagMiniMapMaker != true) then (

	createDialog nx_tile_minimap
	
)
