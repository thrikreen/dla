/*-----------------------------------------------------------------------------\

    Aurora Animation Mesh Modifer
    
    This modifier stores the information needed by the Aurora engine for
    animated meshes.
    
    BioWare noted in their original script that it would be better to create a
    NULL modifier in the SDK and then extend that.  Under GMAX we don't have
    that option unless you own the game developers SDK.  And I DON'T!   :-)
    
    NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.26 --	Original compilation version
		---------------------------------------------------------------
		v3.09.22 --	Fixed a "Division by Zero" bug with the # of Samples
					spinner.
     
\-----------------------------------------------------------------------------*/

format "Load: aurora_mod_animesh.ms\r\n"

nx_progressPlus()

--	Applying this modifier to an animated mesh will allow the mesh to be
--	exported with animation into a .mdl file.  The user will specify how many
--	animation subsamples they wish to be taken.

plugin Simplemod auroraanimesh name:"AuroraAniMesh" classID:#(0xa0deb1a3, 0xa32823d0) (

	parameters pblock rollout:params (
	
		--	Make sure all exported attributes are listed here:
		
		animMeshSamples		type:#stringtab		tabsizevariable:true
		
	)
	
	--	Currently selected animation
	
	local current
	
	--	Aurabase object that is parent to this object
	
	local aura = undefined
	
	--	Locate the aurabase node for this object
	
	fn getAuroraBase a = (
	
		aura = a.parent
		
		if (nx_strclassof(aura) != "aurorabase") then (
		
			if (classof(aura) != UndefinedClass) then (
			
				getAuroraBase aura
				
			)
			
		)
		
	)
	
	rollout params "AuroraAniMesh" (
	
		local animStr
		
		label modelname "Model: " align:#center
		label animname "Anim Name: " align:#left
		label animstart "Anim Start: " align:#left
		label animend "Anim End: " align:#left
		listbox animlist "Animations:" width:145 height:5 align:#center
		label gap ""
		spinner spn_sample "# of Samples: " fieldwidth:50 align:#right type:#integer range:[1, 65335, 1] scale:1
		
		--	Joco: Add to show interval details
		
		label calcdinterval "Sampling Interval: 0" align:#left
		button btn_frames "Print Sample Frames" width:140 align:#center toolTip:"Print list of sample frames to Listener"
		
		--	Update the labels with the values of the currently selected
		--	animation
		
		fn updateLabels = (
		
			if (current != 0) then (
			
				animStr = filterstring aura.animations[current] " "
				animname.text = "Anim Name: " + animStr[1]
				animstart.text = "Anim Start: " + (start = animStr[2])
				animend.text = "Anim End: " + (start = animStr[3])
				
				--	Update Sampling Interval
				
				local divBy = 1
				
				if (spn_sample.value != 1) then (
				
					divBy = spn_sample.value - 1
					
				)
				
				local i = ((animStr[3] as integer) - (animStr[2] as integer)) / divBy
				
				calcdinterval.text = "Sampling Interval: " + (i as string)
				
			)
			
		)
		
		--	Upon selecting a new animation, update labels and load the sample
		--	value
		
		fn selectanim a = (
		
			if (a == 0) then (
			
				start = end = current = 0
				animStr = ""
				animname.text = "Anim Name: "
				animstart.text = "Anim Start: "
				animend.text = "Anim End: "
				calcdinterval.text = "Sampling Interval: "		--	Joco
				spn_sample.value = 1
				
			) else (
			
				current = a
				
				if (animMeshSamples[current] == undefined) then (
				
					animMeshSamples[current] = "0"
					spn_sample.value = 1
					
				) else (
				
					spn_sample.value = animMeshSamples[current] as integer
					
				)
				
				updateLabels()		--	Joco: Moved to ensure spn_sample is done first
				
			)
			
		)
		
		--	Retrieve list of animations this aurabase node has
		
		fn getAnimations = (
		
			local temp = #()
			
			for a in aura.animations do (
			
				join temp #(a)
				
			)
			
			this.params.animlist.items = temp
			
		)
		
		on animlist selected a do (
		
			selectanim a
			
		)
		
		on animlist doubleclicked a do (
		
			--	Use these variables for easier understanding
			
			start = (animStr[2] as integer)
			end = (animStr[3] as integer)
			
			if (start < end) then (
			
				animationrange = (interval (start) (end))
				
			)
			
		)
		
		on spn_sample changed num do (
		
			if (current != 0) then (
			
				end = (animStr[3] as integer)
				start = (animStr[2] as integer)
				
				if ((end - start) < spn_sample.value) then (
				
					str = "The number of samples must be less \nor equal to the animation length."
					messageBox str title:"AnimMesh Error: Sample Value" beep:true
					spn_sample.value = animMeshSamples[current] as integer
					
				) else (
				
					animMeshSamples[current] = spn_sample.value as string
					
				)
				
				--	Update Sampling Interval
				
				local divBy = 1
				
				if (spn_sample.value != 1) then (
				
					divBy = spn_sample.value - 1
					
				)
				
				local i = (end - start) / divBy
				
				calcdinterval.text = "Sampling Interval: " + (i as string)
				
			)
			
		)
		
		--	Joco
		
		on btn_frames pressed do (
		
			if (current == 0) then (
			
				--	Nothing selected so do nothing
			
				return 0
				
			)
			
			local animStr = filterstring aura.animations[current] " "
			local firstframe = (animStr[2] as integer)
			local lastframe = (animStr[3] as integer)
			local sample = spn_sample.value
			local interval = (lastframe - firstframe) / (sample - 1)
			
			clearListener()
			
			format "Sample frames from % to % with interval %:\r\n" firstframe lastframe interval
			
			for i = firstframe to lastframe by interval do (
			
				if (i > lastframe) then (
				
					i == lastframe
					
				)
				
				format "%\r\n" i
				
			)
			
		)
		
		on params open do (
		
			name = ""
			current =0
			getAuroraBase selection[1]		--	Note: not $, cause $ can be multiple things
			
			if (nx_strclassof(aura) == "aurorabase") then (
			
				name = aura.name
				getAnimations()
				
			)
			
			for i = 1 to animlist.items.count do (
			
				selectanim i
				
			)
			
			animlist.selection = 0
			modelname.text = "Model: " + name
			spn_sample.value = 1
			
		)
		
	)
	
)
