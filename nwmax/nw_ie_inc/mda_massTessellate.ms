/*----------------------------------------------------------------------------\

	Mass Tessellate
	
	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)

	Plus
	by Michael DarkAngel (www.tbotr.net)

	Credits:

		Wayland Reid:

			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script

		Zaddix:

			Based on code from Zaddix's original WOK file importer

		BioWare:

			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.

	Legal Stuff:

		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.

		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.

		3.	Credit has been given where code or methods have been integrated
			from other sources.

		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.

		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.

	Version Info:

		v1.09.26 --	Original compilation version

\-----------------------------------------------------------------------------*/

--	Import Routine

fn mdaImport mdaModel = (

	--	Variable Declaration
	
	local mdaPwkFile, mdaDwkFile
	
	--	End Variables
	
	--	Sanity Check (valid model and path)
	
	if ((mdaModel == undefined) or ((getfiles mdaModel).count == 0)) then (
	
		messageBox "Cannot find file!" title:"File Missing!"
		
		return false
		
	)
	
	disableSceneRedraw()
	progressStart "Loading Model"
	ImportNWNmdl mdaModel true false location:[0, 0, 0]
	progressEnd()
	enableSceneRedraw()
	clearSelection()
	gc()
	
)

--	Mass Tessellate Wizard

rollout mdaMassTessellate "Mass Tessellate Wizard" width:300 height:280 (

	Group "Directories" (
	
		label lblSrc "Source: " align:#left across:3
		edittext txtSourceDir align:#center width:150
		button btnSrcBrowse "Browse" align:#right
		edittext txtNumModels "Number of Models: " align:#center width:125
		label lblDest "Destination: " align:#left across:3
		edittext txtDestinationDir align:#center width:150
		button btnDestBrowse "Browse" align:#right
	
	)

	Group "Parameters" (
	
		radiobuttons rdoFacePoly labels:#("Faces", "Polygons")
		radiobuttons rdoEdgeFace labels:#("Edge", "Face-Center")
		spinner spnTension "Tension: " range:[-100.0, 100.0, 25.0] type:#float align:#center width:75
		label lblStatus "" align:#center
	
	)
	
	Group "Iterations" (
	
		radiobuttons rdoIterations labels:#("1", "2", "3", "4")
	
	)
	
	button btnTessellate "Tessellate" width:60 enabled:false across:2
	button btnClose "Close" width:60
	
	on btnClose pressed do (
	
		destroyDialog mdaMassTessellate
	
	)
	
	on rdoEdgeFace changed state do (
	
		case state of (
	
			1: (

				spnTension.enabled = true

			)

			2: (

				spnTension.enabled = false

			)
		
		)
	
	)
	
	on btnSrcBrowse pressed do (
	
		--	Variable Declaration
		
		local mdaNumMdls
		local mdaDirPath
		
		--	End Variables
		
		--	Get Directory Path and Display it
		
		mdaDirPath = getSavePath()
		
		if (mdaDirPath != undeifined) then (
		
			txtSourceDir.text = mdaDirPath
			
			--	Get Number of Models in Directory
			
			mdaNumMdls = getFiles(mdaDirPath + "\\*.mdl")
			txtNumModels.text = mdaNumMdls.count as string
			
			--	End Number of Models
			
			--	Enable Mass Tessellate
			
			if ((txtSourceDir.text != "") and (txtDestinationDir.text != "") and (mdaNumMdls.count != 0)) then (
			
				btnTessellate.enabled = true
			
			)
			
			--	End Enable Mass Tessellate
			
			--	Display Error if No Models in Directory
			
			if (mdaNumMdls.count == 0) then (
			
				MessageBox "No Model Files in the Selected Directory.\nPlease Select Another Directory." title:"MDL Not Found!"
				btnTessellate.enabled = false
			
			)
			
			--	End Display Error
		
		)
		
		--	End Directory Path
	
	)
	
	on btnDestBrowse pressed do (
	
		--	Variable Declaration
		
		local mdaDirPath
		
		--	End Variables
		
		--	Get Directory Path and Display it
		
		mdaDirPath = getSavePath()
		
		if (mdaDirPath != undefined) then (
		
			txtDestinationDir.text = mdaDirPath
			nx_util1.edt_resetdir.text = mdaDirPath
			
			--	Enable Mass Tessellate
			
			if ((txtSourceDir.text != "") and (txtDestinationDir.text != "") and (txtNumModels.text != "")) then (
			
				btnTessellate.enabled = true
			
			)
			
			--	End Enable Mass Tessellate
		
		)
		
		--	End Directory Path
	
	)
	
	on btnTessellate pressed do (
	
		--	Variable Declaration
		
		local doTessellate = true
		local mdaMdlNames = #()
		local mdaDirPath = txtSourceDir.text
		local mdaMdlCount = 1
		
		if ((mdaDirPath != "*.mdl") and (mdaDirPath != "")) then (
		
			mdaMdlNames = getFiles(mdaDirPath + "\\*.mdl")
			mdaMdlCount = mdaMdlNames.count
		
		) else (
		
			MessageBox "Please Select a Source Directory Before Continuing." title:"No Source Directory Selected!"
			doTessellate = false
		
		)
		
		--	End Variables
		
		--	Start Tessellating
		
		if (doTessellate == true) then (
		
			for i = 1 to mdaMdlCount do (
			
				--	Variable Declaration
				
				local skinslist = #()
				local selectedSkins = #()
				local lParents = #()
				local objSelect = #()
				local n
				local s
				local obj
				
				--	End Variables
				
				resetMaxFile #noPrompt
				clearListener()
				
				--	Import Model
				
				mdaImport mdaMdlNames[i]
				gc()
				
				--	Select Everything
				
				max select all
				lblStatus.text = "Storing Selection..."
				
				--	Build Up Parent List and a List of the Selection
				
				for s in selection do (
				
					append lParents s
					append lParents s.parent
					append objSelect s
				
				)
				
				--	We Have to Delink all Skins at this Stage
				--	Also Record all the Skins for Future Processing
				
				lblStatus.text = "Unlinking Skinmeshes..."
				
				for s in objSelect do (
				
					if (s.modifiers["Skin"] != undefined) then (
					
						s.parent = undefined
						append selectedSkins s
					
					)
				
				)
				
				--	Now Store the Bone/Weight Information for each Skin
				--	We Will "re-import" this Later when we Recreate the Skin
				
				lblStatus.text = "Storing Bone Weights..."
				
				--	Variable Declaration
				
				local swl = #()
				local weightBuffer
				local strStream
				
				--	End Variables
				
				for s in selectedSkins do (
				
					weightBuffer = #()
					
					--	Code from Bioware.  To Export Bone Weights,
					--	Modified for thsi Particular Purpose
					
					local saveSelection = selection
					
					max modify mode
					select s
					
					local i, j, zeroWeights
					local m = s.modifiers["Skin"]
					local n = skinops.getnumbervertices m
					
					--	Debug Code, Uncomment "format" Lines to use
					
					--	format "  weight %%" n g_delim to:g_strBuffer
					
					--	End Debug
					
					for i = 1 to n do (
					
						local w_num = skinops.getvertexweightcount m i
						local counter = 0
						
						strStream = stringStream ""
						
						--	Debug Code, Uncomment "format" Lines to use
						
						--	format "    " to:g_strBuffer
						
						--	End Debug
						
						for j = 1 to w_num do (
						
							local bone_id = (skinops.getvertexweightboneid m i j)
							local bone_name = "root"
							local weight_i_j = 1
							
							if (bone_id > 0) do (
							
								bone_name = (skinops.getbonename m bone_id 0)	--	Could be 1 or Could be 0 UNSURE!!!
							
							)
							
							weight_i_j = (skinops.getvertexweight m i j)
							
							if (weight_i_j != 0.0) then (
							
								format "  % % " bone_name weight_i_j to:strStream
								counter += 1
							
							)
						
						)
						
						if ((w_num == 0) or (counter == 0)) do (
						
							format "  root 1.00" to:strStream
						
						)
						
						append weightBuffer (strStream as string)
					
					)
					
					if (saveSelection != undefined) do (
					
						select saveSelection
					
					)
					
					append swl (nx_SkinWeights s weightBuffer)
				
				)
				
				--	Apply Tessellation
				
				lblStatus.text = "Tessellating Geometry..."
				
				for obj in objSelect do (
				
					if (obj.modifiers["Skin"] == undefined) then (
					
						if ((validModifier obj Tessellate) and (iskindof obj Editable_Mesh)) then (
						
							addmodifier obj (tessellate())
							obj.tessellate.iterations = (rdoIterations.state - 1)
							obj.tessellate.tension = (spnTension.value * 100)
							obj.tessellate.faceType = (rdoFacePoly.state - 1)
							obj.tessellate.type = (rdoEdgeFace.state - 1)
							
						)
						
					)
				
				)
				
				--	Unlink the Entire Selection
				
				lblStatus.text = "Unlinking Geometry..."
				
				for s in objSelect do (
				
					--	Debug Code
					
					--	queryBox "Unlinking Geometry"
					
					--	End Debug
				
					s.parent = undefined
					
				)
				
				--	Collapse all Skin Modifiers
				
				lblStatus.text = "Collapsing Skinmeshes..."
				
				for s in selectedSkins do (
				
					try (
					
						--	Debug Code

						--	queryBox "Collapsing Skinmeshes"

						--	End Debug
				
						collapseStack s
					
					) catch (
					
						--	Do Nothing Here
					
					)
				
				)
				
				--	Reset XForms
				
				local rotvalue
				local piv
				local orig
				
				for s in objSelect do (
				
					if (iskindof s Editable_Mesh) then (
					
						rotvalue = s.rotation
						s.rotation = (quat 0 0 0 1)
						orig = copy s
						nx_reset_transforms s
						s.rotation = rotvalue
						
						format "XForm added: %\r\n" s.name
						
						--	Collapse the Stack
						
						try (
						
							--	Debug Code

							--	queryBox "Collapsing the Stack"

							--	End Debug
				
							collapsestack s
						
						) catch (
						
							--	Do Nothing Here
						
						)
						
						format "XForm Collapsed\r\n"
						
						--	Restore Original Modifiers Except the XForm
						
						--	Debug Code
						
						--	queryBox "Restoring Modifiers"
						
						--	End Debug
						
						nx_copy_modifiers s orig ignore:#("XForm", "Tessellate")
						delete orig
					
					) else (
					
						--	Not Editable Mesh Therefore is a Dummy
						
						format "Not Editable Mesh %\r\n" s.name
					
					)
				
				)
				
				--	Restore Skin Modifiers
				
				lblStatus.text = "Restoring Skinmeshes..."
				
				for s in selectedSkins do (
				
					--	Make a New Skin Modifier and Apply it
					
					--	Debug Code
					
					--	queryBox "Restoring Skinmeshes"
					
					--	End Debug
					
					addmodifier s (skin())
				
				)
				
				for i = 1 to swl.count do (
				
					--	Apply Weight Info to the Skin Modifier
					
					--	Debug Code
					
					--	queryBox "Applying Weight Data"
					
					--	End Debug
					
					addMDLSkin (swl[i]).Object (swl[i]).WeightsData
				
				)
				
				--	Relink Selection
				
				lblStatus.text = "Linking Geometry..."
				
				for n = 1 to lParents.count by 2 do (
				
					--	Debug Code
					
					--	queryBox "Linking Geometry"
					
					--	End Debug
				
					lParents[n].parent = lParents[n + 1]
				
				)
				
				--	Prep for Export
				
				--	Debug Code
				
				--	queryBox "Prep for Export"
				
				--	End Debug
				
				nx_util1.box_WeldCheck.checked = false
				nx_util1.box_WOKchecker.checked = false
				
				local bIsSelection = false
				
				if not(nx_existDir nx_util1.edt_resetdir.text) then (
				
					MessageBox "Directory Does Not Exist!" title:"Invalid Directory!"
					return 0
				
				)
				
				for b in $helpers do (
				
					if (b.isSelected and (iskindof b aurorabase)) then (
					
						bIsSelection = true
						
						--	Node is Selected so Reset Base
						
						b.export_path = nx_util1.edt_resetdir.text
					
					)
				
				)
				
				if not(bIsSelection) then (
				
					MessageBox "No Aurora Bases Selected!"
					return 0
				
				)
				
				--	Export All
				
				--	Debug Code
				
				--	queryBox "Exporting"
				
				--	End Debug
				
				ExportAll()
				
				--	End Export
				
				gc()
				
			)
		
		)
	
	)

)

createDialog mdaMassTessellate modal:true