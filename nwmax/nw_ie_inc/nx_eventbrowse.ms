/*-----------------------------------------------------------------------------\

	Event Browser & Editor

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
     
\-----------------------------------------------------------------------------*/

global nx_eventbrowse_flag

rollout nx_eventbrowse "Event Browser" width:180 (

	fn is_modelbase obj = (

		(iskindof obj aurorabase)

	)

	--	UI

	label lblModel "Modelbase:"
	label lbl_model "undefined"
	pickbutton btn_setbase "Pick Model Base" width:150 align:#center filter:is_modelbase
	listbox lst_events "Events" width:170 height:20 align:#center
	button btn_setevent "Set Event Frame" width:150 align:#center toolTip:"Sets current event to current frame"
	button btn_delete "Delete Event" width:150 align:#center

	group "Quick Events" (

		button btn_qe_blurstart "Blur Start" width:75 align:#left across:2
		button btn_qe_blurend "Blur End" width:75 align:#right
		button btn_qe_drawarrow "Draw Arrow" width:75 align:#left across:2
		button btn_qe_drawweapon "Draw Weapon" width:75 align:#right
		button btn_qe_hit "Hit" width:50 align:#left across:3
		button btn_qe_parry "Parry" width:50 align:#center
		button btn_qe_cast "Cast" width:50 align:#right
		button btn_qe_sndfootstep "Footstep" width:75 align:#left across:2
		button btn_qe_sndhitground "Hitground" width:75 align:#right
		button btn_qe_detonate "Detonate" width:150 align:#center
		
	)
	
	button btn_refresh "Refresh" width:150 align:#center
	
	fn refresh_list = 	(
	
		local temp = #()
		
		if (btn_setbase.object != undefined) then (
		
			local mbase = btn_setbase.object
			
			join temp mbase.events
			lst_events.items = temp
			
		)
		
	)
	
	--	Actions
	
	on nx_eventbrowse open do (
	
		nx_eventbrowse_flag = true
		
	)
	
	on nx_eventbrowse close do (
	
		nx_eventbrowse_flag = false
		
	)
	
	on btn_setbase picked obj do (
	
		lbl_model.text = obj.name
		refresh_list()
		
	)
	
	on lst_events doubleClicked val do (
	
		--	Parse out the selected animation
		
		local str_event = lst_events.selected
		
		if (str_event != undefined) then (
		
			--	Parse the event string
			
			tokens = filterString str_event " "
			
			local first_frame = (tokens[2] as integer) - 15
			local last_frame = (tokens[2] as integer) + 15
			
			if (first_frame < 0) then (
			
				first_frame = 0
				
			)
			
			--	Zoom to the double clicked animation
			
			if (first_frame < last_frame) then (
			
				animationRange = (interval first_frame last_frame)
				sliderTime = (tokens[2] as integer)
				
			)
			
		)
		
	)
	
	on btn_refresh pressed do (
	
		refresh_list()
		
	)
	
	on btn_setevent pressed do (
	
		if (btn_setbase.object != undefined) then (
		
			local mdlbase = btn_setbase.object
			local str_event = lst_events.selected
			local tokens = filterstring str_event " "
			local eventname = tokens[1]
			local eventframe = (sliderTime.frame as integer) as string
			
			str_event = eventname + " " + eventframe
			mdlbase.events[lst_events.selection] = str_event
			refresh_list()
			
		)
		
	)
	
	on btn_delete pressed do (
	
		if ((btn_setbase.object != undefined) and (lst_events.selection > 0)) then (
		
			local mdlbase = btn_setbase.object
			
			deleteitem mdlbase.events (lst_events.selection)
			refresh_list()
			
		)
		
	)
	
	--	Quick event code
	
	fn add_event strEvent = (
	
		local str_frame = (sliderTime.frame as integer) as string
		
		if (btn_setbase.object != undefined) then (
		
			local mdlbase = btn_setbase.object
			
			mdlbase.events[mdlbase.events.count + 1] = (strEvent + " " + str_frame)
			refresh_list()
			lst_events.selection = mdlbase.events.count
			
		)
		
	)
	
	on btn_qe_blurstart pressed do (
	
		add_event "blur_start"
		
	)
	
	on btn_qe_blurend pressed do (
	
		add_event "blur_end"
		
	)
	
	on btn_qe_drawarrow pressed do (
	
		add_event "draw_arrow"
		
	)
	
	on btn_qe_drawweapon pressed do (
	
		add_event "draw_weapon"
		
	)
	
	on btn_qe_hit pressed do (
	
		add_event "hit"
		
	)
	
	on btn_qe_parry pressed do (
	
		add_event "parry"
		
	)
	
	on btn_qe_cast pressed do (
	
		add_event "cast"
		
	)
	
	on btn_qe_sndfootstep pressed do (
	
		add_event "snd_footstep"
		
	)
	
	on btn_qe_sndhitground pressed do (
	
		add_event "snd_hitground"
		
	)
	
	on btn_qe_detonate pressed do (
	
		add_event "detonate"
		
	)
	
)

if (nx_eventbrowse_flag != true) then (

	createDialog nx_eventbrowse
	
)
