/*-----------------------------------------------------------------------------\

	Scale Wizard

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
		---------------------------------------------------------------
		v5.04.26 --	Fix to allow for turning off Sanity Checks when performing
					Mass Scaling operations.
     
\-----------------------------------------------------------------------------*/

if (g_linkCopy == undefined) then (

	global g_linkCopy = #()
	
)

--	Import routine for Mass Scaler

fn mdaImport mdaModel = (

	--	Variable Declaration
	
	local mdaPwkFile, mdaDwkFile
	
	--	End Variables
	
	--	Sanity Check (valid model and path)
	
	if ((mdaModel == undefined) or ((getfiles mdaModel).count == 0)) then (
	
		MessageBox "Cannot find file!" title:"File Missing!"
		return false
		
	)
	
	disableSceneRedraw()
	progressStart "Loading Model"
	ImportNWNmdl mdaModel true false location:[0, 0, 0]
	progressEnd()
	enableSceneRedraw()
	clearSelection()
	gc()

)

--	Model Scale Wizard

rollout nx_ScaleWizard "Scale Wizard" (

	group "Basics" (
	
		spinner spn_scale "% to Scale: " width:75 align:#center type:#float range:[0.0, 1000.0, 100.0]
		label lbl_status ""
		
	)
	
	group "Mass Scaling" (
	
		checkbox mdaMassScaler "Enable Mass Scaling" align:#left
		edittext mdaDirName "Directory: " text:"*.mdl" width:215 align:#left enabled:false across:2
		button mdaDirBrowse "Browse" align:#right enabled:false
		edittext mdaRenameMdl "New Pheno #: " width:175 align:#left enabled:false toolTip:"Enter a new pheno number for the models."
		edittext mdaMdlNum "Number of Models in Dir: " width:175 align:#left readOnly:true enabled:false
		checkbox mdaInsane "Turn off Sanity Checks" align:#left enabled:false toolTip:"Turning off Sanity Checks will ensure all models get exported."
	
	)
	
    button btn_scale "Scale" width:44 across:2
    button btn_close "Close" width:44
	
	on mdaMassScaler changed mdaNewState do (
	
		mdaDirName.enabled = mdaNewState
		mdaDirBrowse.enabled = mdaNewState
		mdaRenameMdl.enabled = mdaNewState
		mdaMdlNum.enabled = mdaNewState
		btn_scale.enabled = not(mdaNewState)
		mdaInsane.enabled = mdaNewState
		
		if (mdaNewState == false) then (
		
			mdaDirName.text = "*.mdl"
			mdaRenameMdl.text = "*.mdl"
			mdaMdlNum.text = ""
		
		)
	
	)
	
	on mdaDirBrowse pressed do (
	
		--	Variable Declaration
	
		local mdaNumMdls
		local mdaDirPath
		
		--	End Variables
		
		--	Get Directory Path and display it
		
		mdaDirPath = getSavePath()
		
		if (mdaDirPath != undefined) then (
		
			mdaDirName.text = mdaDirPath
			
			--	Get Number of models in directory
			
			mdaNumMdls = getFiles(mdaDirPath + "\\*.mdl")
			mdaMdlNum.text = mdaNumMdls.count as string
			
			--	End Number of models
			
			--	Enable Scaling
			
			btn_scale.enabled = true
			
			--	End Enable Scaling

			--	Display error if no models found in directory
			
			if (mdaNumMdls.count == 0) then (
			
				MessageBox "No model files in the selected directory.\nPlease select another." title:"MDL Not Found!"
				btn_scale.enabled = false
				
			)
				
			--	End Display error
			
		)
		
		--	End Directory Path
		
	)

    on btn_scale pressed do (
	
		--	Variable Declaration
		
		local doScaling = true
		local mdaMdlNames = #()
		local mdaDirPath
		local mdaMdlCount = 1
		
		if (mdaMassScaler.checked == true) then (
		
			mdaDirPath = mdaDirName.text
			
			if ((mdaDirPath != "*.mdl") and (mdaDirPath != "")) then (
			
				mdaMdlNames = getFiles(mdaDirPath + "\\*.mdl")
				mdaMdlCount = mdaMdlNames.count
				
			) else (
			
				MessageBox "Please select a directory before continuing." title:"No Directory Selected!"
				doScaling = false
			
			)
			
		)
		
		--	End Variables
		
		if (doScaling == true) then (
		
			for i = 1 to mdaMdlCount do (
			
				local skinslist = #()
				local selectedSkins = #()
				local lParents = #()
				local objSelect = #()
				local n
				local s
				local obj
				
				if (mdaMassScaler.checked == true) then (
				
					resetMaxFile #noPrompt
					clearListener()
					
					--	Import model
					
					mdaImport mdaMdlNames[i]
					gc()
					
				)
				
				--	Scaling starts here.

				--	Select Everything
			
				max select all
				lbl_status.text = "Storing selection ..."
			
				--	Build up parent list and a list of the selection.
			
				for s in selection do (
			
					append lParents s
					append lParents s.parent
					append objSelect s
				
				)
			
				--	We have to delink all skins at this stage.
				--	Also record all the skins for furture processing.
			
				lbl_status.text = "Unlinking skinmeshes ..."
			
				for s in objSelect do (
			
					if (s.modifiers["Skin"] != undefined) then (
				
						s.parent = undefined
						append selectedSkins s
					
					)
				
				)
			
				--	Also store the bone/weight information for each skin.
				--	We will "re-import" this later when we recreate the skin.
			
				lbl_status.text = "Storing bone weights ..."
			
				local swl = #()
				local weightBuffer
				local strStream
			
				for s in selectedSkins do (
			
					weightBuffer = #()
				
					--	Code from BioWare.  To export bone weights,
					--	modified for this particular purpose.
				
					local save_selection = selection
				
					max modify mode
					select s
				
					local i, j, zeroWeights
					local m = s.modifiers["Skin"]
					local n = skinops.getnumbervertices m
				
					--	Debug code, uncomment "format" lines to use.
				
					--	format "   weights %%" n g_delim to:g_strBuffer
				
					--	End Debug code
				
					for i = 1 to n do (
				
						local w_num = skinops.getvertexweightcount m i
						local counter = 0
					
						strStream = stringStream ""
					
						--	Debug code, uncomment "format" lines to use.
					
						--	format "    " to:g_strBuffer
					
						--	End Debug code
					
						for j = 1 to w_num do (
					
							local bone_id = (skinops.getvertexweightboneid m i j)
							local bone_name = "root"
							local weight_i_j = 1
						
							if (bone_id > 0) do (
						
								bone_name = (skinops.getbonename m bone_id 0)	--	Could be 1 or could be 0 UNSURE!!
							
							)
						
							weight_i_j = (skinops.getvertexweight m i j)
						
							if (weight_i_j != 0.0) then (
						
								format "  % % " bone_name weight_i_j to:strStream
								counter += 1
							
							)
						
						)
					
						if ((w_num == 0) or (counter == 0)) do (
					
							format "  root 1.00 " to:strStream
						
						)
					
						append weightBuffer (strStream as string)
					
					)
				
					if (save_selection != undefined) do (
				
						select save_selection
					
					)
				
					append swl (nx_SkinWeights s weightBuffer)
				
				)
			
				--	Perform the Scaling
			
				lbl_status.text = "Scaling geometry ..."
			
				local calc_scale = spn_scale.value / 100
				local tm_scale = [calc_scale, calc_scale, calc_scale]
			
				for obj in objSelect do (
			
					-- Scale anything but skins.
				
					if (obj.modifiers["Skin"] == undefined) then (
				
						obj.scale = tm_scale
					
					)
				
				)
			
				--	Unlink the entire selection.
			
				lbl_status.text = "Unlinking geometry ..."
			
				for s in objSelect do (
			
					s.parent = undefined
				
				)
			
				--	Collapse all skin modifiers.
			
				lbl_status.text = "Collapsing skinmeshes ..."
			
				for s in selectedSkins do (
			
					try (
				
						collapseStack s
					
					) catch (
				
						--	Do nothing here.
				
					)
				
				)
			
				--	Do the reset.
			
				lbl_status.text = "Reset XForms ..."
			
				local rotvalue
				local piv
				local orig
			
				for s in objSelect do (
			
					if (iskindof s Editable_Mesh) then (
				
						rotvalue = s.rotation
						s.rotation = (quat 0 0 0 1)
						orig = copy s
						nx_reset_transforms s
						s.rotation = rotvalue
					
						format "XForm added: %\r\n" s.name
					
						--	Collapse the stack
					
						try (
					
							collapsestack s
						
						) catch (
					
							--	Do nothing here.
						
						)
					
						format "XForm Collapsed.\r\n"
					
						--	Restore original modifiers less the Xform.
					
						nx_copy_modifiers s orig ignore:#("XForm")
						delete orig
					
					) else (
				
						--	Not editable mesh therefore is a dummy.
					
						s.scale = [1, 1, 1]
					
					)
				
				)
			
				--	Restore skin modifiers.
			
				lbl_status.text = "Restoring skinmeshes ..."
			
				for s in selectedSkins do (
			
					--	Make a new skin modifier and apply it.
				
					addmodifier s (skin())
				
				)
			
				for i = 1 to swl.count do (
			
					--	Apply weight info to the skin modifer.
				
					addMDLSkin (swl[i]).Object (swl[i]).WeightsData
				
				)
			
				--	Relink selection.
			
				lbl_status.text = "Linking geometry ..."
			
				for n = 1 to lParents.count by 2 do (
			
					lParents[n].parent = lParents[n + 1]
				
				)
			
				lbl_status.text = "Updating model bases ..."
			
				--	Update the aurora bases in the selection
			
				for s in objSelect do (
			
					if ((nx_strclassof s) == "aurorabase") then (
				
						if (s.setanimationscale == 0) then (
					
							s.setanimationscale = 1
						
						)
					
						s.setanimationscale = s.setanimationscale * calc_scale
					
						if s.setsupermodel == "NULL" then (
					
							lbl_status.text = "No supermodel set."
						
						)
					
					)
				
				)
			
				if (lbl_status.text != "No supermodel set.") then (
			
					lbl_status.text = "Done."
				
				)
				
				--	Scaling ends here
				
				--	More Mass Scaler routines here
				
				if (mdaMassScaler.checked == true) then (
				
					local mdaAuraBase = ""
					
					max select all
					
					for s in selection do (
					
						if ((classof s) == aurorabase) then (
						
							mdaAuraBase = s.name
							
							--	Debug code.  Uncomment "format" lines to use.
							
							--	format "Found Aurorabase -- %\n" mdaAuraBase
							
							--	End Debug code.
							
						)
					
					)
				
					--	Rename the Model
					
					if (mdaRenameMdl.text != "") then (
					
						local mdaNewName
						local mdaFront = substring mdaAuraBase 1 3
						local mdaIndex = findString mdaAuraBase "_"
						local mdaBack = substring mdaAuraBase mdaIndex -1
					
						select (getNodeByName mdaAuraBase)
					
						--	Debug code.  Uncomment "format" lines to use.
					
						--	format "mdaAuraBase -->> mdaNewName = % + % + % == %\n" mdaFront mdaRenameMdl.text mdaBack mdaNewName
					
						--	End Debug code.
					
						mdaNewName = mdaFront + mdaRenameMdl.text + mdaBack
						$.name = mdaNewName
					
						--	Debug code.  Uncomment "format" lines to use.
					
						--	format "Rename finished on %" $.name
					
						--	End Debug code.
						
					)
							
					--	End Rename
					
					--	Prep for Export
					
					if mdaInsane.checked == true then (
					
						max select all
					
						for s in selection do (
						
							if ((classof s) == aurorabase) then (
							
								s.sanity_check = 0
							
							)
							
						)
					
					)
					
					local bIsSelection = false
					
					if not(nx_existDir nx_util1.edt_resetdir.text) then (
					
						MessageBox "Directory does not exist!" title:"Invalid Directory!"
						return 0
						
					)
					
					for b in $helpers do (
					
						if (b.isSelected and (iskindof b aurorabase)) then (
						
							bIsSelection = true
							
							-- Node is selected so reset base.
							
							b.export_path = nx_util1.edt_resetdir.text
							
						)
						
					)
					
					if not(bIsSelection) then (
					
						MessageBox "No Aurora Bases selected."
						return 0
						
					)
					
					--	Export
					
					ExportAll()
					
					--	End Export
					
					if mdaInsane.checked == true then (
					
						max select all
					
						for s in selection do (
						
							if ((classof s) == aurorabase) then (
							
								s.sanity_check = 1
							
							)
							
						)
					
					)
					
					gc()
						
				)
				
			)
			
		)
		
	)
	
	on btn_close pressed do (
	
		destroyDialog nx_ScaleWizard
		
	)
	
)

createDialog nx_ScaleWizard modal:true width:300 height:230
