/*-----------------------------------------------------------------------------\

    Speed Buttons UI

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
		---------------------------------------------------------------
		v3.07.22 --	Export Path button enabled in Preferences rollout and tied
					into the Reset button of the Model Base Parameters,
					Set Position button enabled in Preference rollout now saves
					rollout location if not docked.
		---------------------------------------------------------------
		v4.08.25 --	Addition of "Check for Skins" to sanity checks.
		---------------------------------------------------------------
		v5.04.26 --	Fixed 'Set Sanity Checks'.  It will now save Sanity Check
					preferences.
		v5.04.27 --	Turned 'Reset X-form' off for Skinned meshes.
		v5.06.08 --	Removed Texture Tools rollout.  Doesn't work with gMax,
					will never work with gMax.  Removed Reset Export Dir from
					General Utilities rollout, redundant code.
		---------------------------------------------------------------
		v6.11.12 --	Prototyping for Autodock preference.
					Autodock, Export Path, and Anim List Size preferences have
					been fixed.
					Sanity check settings have been moved to the "Preferences"
					rollout.
		v6.11.20 --	Fixed TileSet Creator button.
     
\-----------------------------------------------------------------------------*/

filein (g_startpath + "nw_ie_inc\\aurora_fn_desparkle.ms")

--	Global Variables

global g_linkCopy = #()
global tscDir

fn LoadPlugIns = (

	--	Load plugins into g_plugins
	--	Pass through the plugins dir and process all the .ini files
	
	g_plugins = #()
	
	local filepattern = scriptsPath + "nwmax_plus\\plugins\\*.ini"
	local files = getFiles filepattern
	
	for f in files do (
	
		--	Each file is an ini file so get the keys and add to global array
		
		append g_plugins (getINISetting f "setup" "name")
		append g_plugins (getINISetting f "setup" "script")
		
	)
	
)

rollout nx_util2 "Robes\Armor\Skin" (

	button btn_armourer "Armor Maker" width:150
	button btn_cutter "Armor Cutter" width:150
	button btn_skindumper "Skin Dumper" width:150
	button btn_weightmaster "Weight Master" width:150
	button btn_pivotsnap "Pivot Snap" width:75 align:#left across:2
	dropdownlist drp_snaptypes width:60 align:#right items:#("pmh", "pfh", "pma", "pfa", "pmd", "pfd", "pme", "pfe", "pmg", "pfg", "pmo", "pfo")
	checkbox chk_pivotonly "Pivot Only" width:75 align:#left checked:false across:2
	dropdownlist drp_snappheno width:60 align:#right items:#("0", "2")
	
	on btn_armourer pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\ramaker\\nwmax_ramaker.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_cutter pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\ramaker\\nwmax_armourcutter.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_skindumper pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\ramaker\\nwmax_skindumper.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_weightmaster pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_weightmaster.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_pivotsnap pressed do (
	
		nx_snaptonormalpivots (drp_snaptypes.selected + drp_snappheno.selected) pivotonly:(chk_pivotonly.checked)
		
	)
	
	on nx_util2 open do (
	
		--	Make sure some utils are available
		
		if not (nx_existFile(scriptsPath + "nwmax_plus\\ramaker\\nwmax_armourcutter.ms")) then (
		
			btn_cutter.enabled = false
			
		)
		
		if not (nx_existFile(scriptsPath + "nwmax_plus\\ramaker\\nwmax_skindumper.ms")) then (
		
			btn_skindumper.enabled = false
			
		)
		
	)
	
)

rollout nx_util3 "Mesh Tools" (

	button btn_trimesh "Trimesh" width:150 align:#center toolTip:"Convert mesh into a trimesh, preserving modifiers"
	button btn_cryomesh "Cryomesh" width:150 align:#center toolTip:"Save selected mesh states to file for later reload"
	button btn_shadowchecker "Check Shadows" width:150 align:#center toolTip:"Check shadow validity of all selected objects"
	button btn_tidy_materials "Rationalise Materials" width:150 align:#center toolTip:"Rationalise the materials in the scene"
	button btn_tvertdisplace "TVert Displacer" width:150 align:#center toolTip:"Scale/Move tverts on a material that has been resized"
	button btn_randomisecolor "Random Wirecolor" width:150 align:#center toolTip:"Randomise the wirecolor of all selected objects"
	button btn_resetxform "ResetXForm" across:2 width:70 align:#left toolTip:"Auto unlinks selection, reset xforms the editable meshes and relinks. Pivots can be preserved or not."
	checkbox chk_keeppivots "Save Pivots" align:#right checked:true
	checkbutton btn_linkcopy "Copy/Paste Links" width:95 align:#left toolTip:"Copy linkages of selected nodes, then on 2nd push restore them." across:2
	button btn_copyreset "Reset" width:40 align:#right toolTip:"Clear copy link buffer"
	button btn_orderchildren "Order Children" width:150 align:#center toolTip:"Manually reorder children of selected object"
	
	on btn_trimesh pressed do (
	
		undo on (
		
			for obj in selection do (
			
				local orig = copy obj
				
				--	Converting to an Editable Patch and back again splits the
				--	given object's polygons into faces (triangles).
				
				convertToMesh obj
				convertTo obj Editable_Patch
				convertToMesh obj
				
				--	Restore the original modifiers.
				
				nx_copy_modifiers obj orig
				delete orig
				
			)
			
		)
		
	)
	
	on btn_cryomesh pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_cryomesh.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_tvertdisplace pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_tvertdisplacer.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_copyreset pressed do (
	
		g_linkCopy = #()
		btn_linkcopy.checked = false
		
	)
	
	on btn_orderchildren pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_orderchildren.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_linkcopy changed down do (
	
		if down then (
		
			--	Copy links
			
			g_linkCopy = #()
			
			for s in selection do (
			
				append g_linkCopy s
				append g_linkCopy s.parent
				
			)
			
		) else (
		
			--	Restore links
			
			local i
			
			for i = 1 to g_linkCopy.count by 2 do (
			
				try (
				
					g_linkCopy[i].parent = g_linkCopy[i + 1]
					
				) catch (
				
					--	Do Nothing Here
					
				)
				
			)
			
		)
		
	)
	
	on btn_resetxform pressed do (
	
		local lParents = #()
		
		clearListener()
		
		--	Build up parent list
		
		for s in selection do (
		
			append lParents s
			append lParents s.parent
			
		)
		
		--	Unlink the selection
		
		format "unlink\r\n"
		
		for s in selection do (
		
			s.parent = undefined
			
		)
		
		--	Do the reset
		
		format "Reset XForm\r\n"
		
		--	Do the reset
		
		local rotvalue
		local piv
		local orig
		
		for s in selection do (
		
			local skins = 0
			
			for m in s.modifiers do (
			
				if (iskindof m Skin) then (
				
					skins = skins + 1
					
				)
				
			)
			
			if skins == 0 then (
			
				if (iskindof s Editable_Mesh) then (
				
					--	Preserve pivots orientation/rotation
					
					if chk_keeppivots.checked then (
					
						rotvalue = s.rotation
						s.rotation = (quat 0 0 0 1)
						
					)
					
					orig = copy s
					nx_reset_transforms s
					
					if chk_keeppivots.checked then (
					
						s.rotation = rotvalue
						
					)
					
					format "XForm added: %\r\n" s.name
					
					--	Collapse stack
					
					try (
					
						collapsestack s
						
					) catch (
					
						--	Do Nothing Here
						
					)
					
					format "XForm Collapsed\r\n"
					
					--	Restore original modifiers less the Xform
					
					nx_copy_modifiers s orig ignore:#("XForm")
					delete orig
					
				) else (
				
					s.scale = [1, 1, 1]
					
				)
				
			) else (
			
				format "Reset X-form skipped on Skinned Mesh\r\n"
				
			)
			
		)
		
		--	Relink
		
		format "Relink\r\n"
		
		for n = 1 to lParents.count by 2 do (
		
			format "n=%\r\n" n
			
			lParents[n].parent = lParents[n + 1]
			
		)
		
	)
	
	--	Tidy Materials
	
	fn qcompare obj1 obj2 = (
	
		local s1 = obj1.mat.diffusemap.filename
		local s2 = obj2.mat.diffusemap.filename
		
		if (s1 < s2) then (
		
			return -1
			
		)
		
		if (s1 > s2) then (
		
			return 1
			
		)
		
		0		--	They must be equal
		
	)
	
	fn matcompare mat1 mat2 = (
	
		if  ((mat1.name != mat2.name) and (mat1.diffusemap.filename == mat2.diffusemap.filename) and (mat1.diffuse == mat2.diffuse) and (mat1.ambient == mat2.ambient) and (mat1.specular == mat2.specular) and (mat1.specularlevel == mat2.specularlevel) and (mat1.glossiness == mat2.glossiness) and (mat1.selfillumcolor == mat2.selfillumcolor) and (mat1.opacity == mat2.opacity)) then (
		
			return true
			
		)
		
		false		--	Assume not the same unless proven otherwise
		
	)
	
	on btn_tidy_materials pressed do (
	
		local objlist = #()
		
		format "Material Rationalisation\r\n"
		format "... Getting objects\r\n"
		
		--	Get the list of objectss that have standard materials with a bitmap
		
		for o in $objects do (
		
			if ((o.mat != undefined) and ((classof o.mat) == Standardmaterial)) then (
			
				if (o.mat.diffusemap != undefined) then (
				
					append objlist o
					
				)
				
			)
			
		)
		
		format "... Sorting objects\r\n"
		
		--	Sort the array by the filename on the material
		
		qsort objlist qcompare
		
		--	Start the rationalisation
		
		format "... Rationalise objects\r\n"
		
		local matDeleteList = #()
		local i, j, m
		
		for i = 1 to objlist.count do (
		
			j = i + 1
			
			--	Make sure that j is within array bounds
			
			if (j <= objlist.count) then (
			
				while (matcompare objlist[i].mat objlist[j].mat) do (
				
					format "... Object >>  %  << rationalised\r\n" objlist[j]
					
					--	Put this material on the delete list
					
					append matDeleteList objlist[j].mat
					
					--	Rationalise the assigned material
					
					objlist[j].mat = objlist[i].mat
					
					--	Move pointer forward 1
					
					j += 1
					
					--	Make sure j is within array bounds
					
					if (j > objlist.count) then (
					
						exit
						
					)
					
				)
				
				--	Move i forward to j - 1
				
				i = j - 1
				
			)
			
		)
		
		--	Now clean out the materials to be deleted from the scene
		
		format "... Remove redundant materials\r\n"
		
		for m in matDeleteList do (
		
			format "... Material >>  %  << removed\r\n" m.name
			
			deleteItem sceneMaterials m.name
			
		)
		
		format "Material Rationalisation Ends\r\n"
		
	)
	
	--	End Tidy Materials
	
	on btn_randomisecolor pressed do (
	
		local sel = selection as array
		
		for s in sel do (
		
			try (
			
				s.wirecolor = random black white
				
			) catch (
			
				--	Do Nothing Here
				
			)
			
		)
		
	)
	
	--	Test for valid shadows.  If a ray from the pivot point hits the front
	--	of any face in the object, the shadows will be off.
	--	Code by roboius (http://rockbottom.vtex.net/)
	
	fn make_color_str c = (
	
		local r, g, b
		
		r = c.red as string
		g = c.green as string
		b = c.blue as string
		r + "|" + g + "|" + b
		
	)
	
	fn parse_color_str s = (
	
		local list, r, g, b
		
		list = filterString s "|"
		r = list[1] as integer
		g = list[2] as integer
		b = list[3] as integer
		
		return (color r g b)
		
	)
	
	fn test_shadows obj = (
	
		local invalid_shadow_color = [255, 0, 0]
		local disabled_shadow_color = [0, 0, 0]
		local face, normal, vertex, dp, errors = 0
		local bHasFailed = 0
		
		if not (iskindof obj Editable_Mesh) then (
		
			return 0
			
		)
		
		try (
		
			bHasFailed = (getUserProp obj "shadowfailed") as integer
			
		) catch(
		
			bHasFailed = 0
			
		)
		
		if (bHasFailed == 0) then (
		
			setUserProp obj "wirecolor" (make_color_str obj.wirecolor)
			originalcolor = obj.wirecolor
			
		) else (
		
			originalcolor = parse_color_str (getUserProp obj "wirecolor")
			
			if (originalcolor == undefined) then (
			
				originalcolor = obj.wirecolor
				
			)
			
		)
		
		for ii = 1 to obj.numfaces do (
		
			face = getface obj ii
			normal = getfacenormal obj ii
			vertex = getvert obj face.x
			dp = dot normal vertex
			
			if (((dot normal obj.pivot) - dp) > 5e-3) then (
			
				errors += 1
				
			)
			
		)
		
		if (errors > 0) then (
		
			format "Warning: Object % may have an invalid shadow.\r\n" obj.name
			
			obj.wirecolor = invalid_shadow_color
			setUserProp obj "shadowfailed" 1
			
			return 1
			
		) else (
		
			obj.wirecolor = originalcolor
			setUserProp obj "shadowfailed" 0
			
			return 0
			
		)
		
	)
	
	on btn_shadowchecker pressed do (
	
		for obj in selection do (
		
			test_shadows obj
			
		)
		
	)
	
	--	End shadow test functions
	
)

rollout nx_util4 "Anim Tools" (

	button btn_reset_controllers "Reset Controllers" width:150 align:#center toolTip:"Resets the rotation controller to tcb then back to linear"
	button btn_animcopy "Copy Animation Keys" width:150 align:#center toolTip:"Copy Anim keys in the specified range to a target start frame"
	button btn_animapper "Anim Mapper" width:150 align:#center toolTip:"Import animations off nodes in an MDL and map to scene nodes"
	button btn_keymaster "Key Master" width:150 align:#center toolTip:"A number of mass key manipulation functions"
	button btn_keytweak "Mass Key Tweaker" width:150 align:#center toolTip:"Mass tweaking of rot and pos keys"
	button btn_bakeanim "Bake Anims" width:150 align:#center
	button btn_keyreduct "Reduce Anim Keys" width:150 align:#center toolTip:"Rationalise the animation keys for the selected nodes"
	button btn_animbrowse "Anim Broswer" width:150 align:#center
	button btn_eventbrowse "Event Broswer" width:150 align:#center
	
	on btn_keymaster pressed do (
	
		try (

			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_animkeymaster.ms")

		) catch (

			--	Do Nothing Here

		)

	)
	
	on btn_reset_controllers pressed do (
	
		local objSum = $objects.count * 2.0
		local cnt = 0.0
		
		progressStart "Reset Controllers"
		
		format "Setting to tcb rotation\r\n"
		
		for o in $objects do (
		
			o.rotation.controller = tcb_rotation()
			o.pos.controller = tcb_position()
			o.scale.controller = tcb_scale()
			cnt += 1
			
			if ((progressUpdate (((cnt * 100) / objSum) as integer)) == false) then (
			
				return 0
				
			)
			
		)
		
		format "Setting back to linear rotation\r\n"
		
		for o in $objects do (
		
			o.rotation.controller = linear_rotation()
			o.pos.controller = linear_position()
			o.scale.controller = linear_scale()
			cnt += 1
			
			if ((progressUpdate (((cnt * 100) / objSum) as integer)) == false) then (
			
				return 0
				
			)
			
		)
		
		progressEnd()
		
	)
	
	on btn_animcopy pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_animkeycopy.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_animapper pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_mapper.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_keytweak pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_masskeytweak.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_bakeanim pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_animbaker.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_keyreduct pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_keyreduct.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_animbrowse pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_animbrowse.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_eventbrowse pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_eventbrowse.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
)

rollout nx_util5 "Texture Tools" (

	-- UI
	
	button btn_texlib "TexLib" width:150 align:#center
	button btn_texlibmaker "TexLib Maker" width:150 align:#center
	button btn_minimap "MiniMap Maker" width:150 align:#center
	
	-- Ensure only active if running max version of nwmax.
	
	on nx_util5 open do (
	
		if (not g_ismax) then (
		
			btn_texlib.enabled = false
			btn_texlibmaker.enabled = false
			btn_minimap.enabled = false
			
		)
		
	)
	
	-- Actions.
	
	on btn_texlib pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_texturelib.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_texlibmaker pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_texturelibmaker.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
	on btn_minimap pressed do (
	
		try (
		
			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_minimap_maker.ms")
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
	)
	
)

rollout nx_prefs "Preferences" (

	label lbl_wip "This is WIP"
	checkbox chk_autodock "Autodock" checked:(nx_itob(gNX_dock))
	button btn_exportpath "Export Path" width:150 align:#center
	button btn_setPosition "Set Position" width:150 align:#center
	button btn_skinpath "SkinDumper Path" width:150 align:#center enabled:false
	button btn_xpathcryo "CyroMesh Path" width:150 align:#center enabled:false
	spinner spn_animheight "Anim List Length: " fieldwidth:30 align:#right type:#integer range:[5, 30, g_guiAnimListSize]
	
	group "Reset Sanity Check" (
	
		label lblAuraBase "AuraBase Checks:" align:#left
		checkbox box_RotationCheck "Zero Rotation" checked:runRotationCheck
		checkbox box_AnimCheck "Valid Anim Keys" checked:runAnimCheck
		label lblTileChecks "Tile Checks:" align:#left
		checkbox box_CollisionMeshCheck "WOK Mesh Validity" checked:runCollisionMeshCheck
		checkbox box_TileNodeBoundaryCheck "Tile Node Boundary Check" checked:runTileNodeBoundaryCheck
		label lblSanityChecks "Sanity Checks:" align:#left
		checkbox box_AuraEmitterCheck "Emitter Validity" checked:runAuraEmitterCheck
		checkbox box_AuraRefCheck "AuraRef Existence" checked:runAuraRefCheck
		checkbox box_NameCheck "Node Name Validity" checked:runNameCheck
		checkbox box_ScaleCheck "Zero World Scale" checked:runScaleCheck
		checkbox box_PartTextureCheck "Material Bitmap Name Chk" checked:runPartTextureCheck
		checkbox box_BitmapNameCheck "Bitmap Name Validity" checked:runBitmapNameCheck
		checkbox box_WeldCheck "Weld To Nearest cm" checked:runWeldCheck
		checkbox box_ClassCheck "Node Type 'boolean'" checked:runClassCheck
		checkbox box_ControllerCheck "Valid Controllers on Keys" checked:runControllerCheck
		checkbox box_AnimMeshCheck "Animated Mesh Validity" checked:runAnimMeshCheck
		checkbox box_DoganCheck "Texvert Validity" checked:runDoganCheck
		checkbox box_FaceCheck "Zero Faces On a Node" checked:runFaceCheck
		checkbox box_BSACheck "Bumpy-shiny Alignmt Chk" checked:runBSACheck
		checkbox box_AnimrootCheck "Animroot Node Existence" checked:runAnimrootCheck
		checkbox box_BoneCountCheck "Skinning:4 Bones per Vert" checked:runBoneCountCheck
		checkbox box_WOKchecker "Check WOK" checked:runWOKCheck
		checkbox box_SkinCheck "Check for Skins" checked:runSkinCheck
		button btn_setsanities "Set Sanity Checks" width:145 align:#center tooltip:"Sets these sanity check options on all selected model bases"
		
	)
	
	--	Addition by Michael DarkAngel 07-21-12
	
	--	Sanity reset code.
	
	fn setsanity i modelbase val = (
	
		local t = modelbase.sanity_tests
		
		--	Make sure bit string array is upgraded to new length.
		
		if (t.count < 21) then (
		
			t = t + "1"
			
		)
		
		t[i] = val
		modelbase.sanity_tests = t
		
	)
	
	on btn_setsanities pressed do (
	
		local datalist = #()
		local strSanity = "000000000000000000000"
			
		max select all
	
		for s in selection do (
		
			if (iskindof s aurorabase) then (
			
				setsanity 1 s (nx_btoi(runRotationCheck) as string)
				setsanity 2 s (nx_btoi(runAnimCheck) as string)
				setsanity 3 s (nx_btoi(runCollisionMeshCheck) as string)
				setsanity 4 s (nx_btoi(runTileNodeBoundaryCheck) as string)
				setsanity 5 s (nx_btoi(runAuraEmitterCheck) as string)
				setsanity 6 s (nx_btoi(runAuraRefCheck) as string)
				setsanity 7 s (nx_btoi(runNameCheck) as string)
				setsanity 8 s (nx_btoi(runScaleCheck) as string)
				setsanity 9 s (nx_btoi(runPartTextureCheck) as string)
				setsanity 10 s (nx_btoi(runBitmapNameCheck) as string)
				setsanity 11 s (nx_btoi(runWeldCheck) as string)
				setsanity 12 s (nx_btoi(runClassCheck) as string)
				setsanity 13 s (nx_btoi(runControllerCheck) as string)
				setsanity 14 s (nx_btoi(runAnimMeshCheck) as string)
				setsanity 15 s (nx_btoi(runDoganCheck) as string)
				setsanity 16 s (nx_btoi(runFaceCheck) as string)
				setsanity 17 s (nx_btoi(runBSACheck) as string)
				setsanity 18 s (nx_btoi(runAnimrootCheck) as string)
				setsanity 19 s (nx_btoi(runBoneCountCheck) as string)
				setsanity 20 s (nx_btoi(runWOKCheck) as string)
				setsanity 21 s (nx_btoi(runSkinCheck) as string)
				
			)
			
		)
		
		strSanity[1] = (nx_btoi(runRotationCheck) as string)
		strSanity[2] = (nx_btoi(runAnimCheck) as string)
		strSanity[3] = (nx_btoi(runCollisionMeshCheck) as string)
		strSanity[4] = (nx_btoi(runTileNodeBoundaryCheck) as string)
		strSanity[5] = (nx_btoi(runAuraEmitterCheck) as string)
		strSanity[6] = (nx_btoi(runAuraRefCheck) as string)
		strSanity[7] = (nx_btoi(runNameCheck) as string)
		strSanity[8] = (nx_btoi(runScaleCheck) as string)
		strSanity[9] = (nx_btoi(runPartTextureCheck) as string)
		strSanity[10] = (nx_btoi(runBitmapNameCheck) as string)
		strSanity[11] = (nx_btoi(runWeldCheck) as string)
		strSanity[12] = (nx_btoi(runClassCheck) as string)
		strSanity[13] = (nx_btoi(runControllerCheck) as string)
		strSanity[14] = (nx_btoi(runAnimMeshCheck) as string)
		strSanity[15] = (nx_btoi(runDoganCheck) as string)
		strSanity[16] = (nx_btoi(runFaceCheck) as string)
		strSanity[17] = (nx_btoi(runBSACheck) as string)
		strSanity[18] = (nx_btoi(runAnimrootCheck) as string)
		strSanity[19] = (nx_btoi(runBoneCountCheck) as string)
		strSanity[20] = (nx_btoi(runWOKCheck) as string)
		strSanity[21] = (nx_btoi(runSkinCheck) as string)
		
		append datalist #("init", "sanity", (strSanity as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
		nx_setinivalue datalist
		
		gnx_sanity = strSanity
		
	)
	
	on box_RotationCheck changed newState do (
	
		--	1 (Zero Rotation)
		
		runRotationCheck = newState
		
	)
	
	on box_AnimCheck changed newState do (
	
		--	2 (Valid Anim Keys)
		
		runAnimCheck = newState
		
	)
	
	on box_CollisionMeshCheck changed newState do (
	
		--	3 (WOK Mesh Validity)
		
		runCollisionMeshCheck = newState
		
	)
	
	on box_TileNodeBoundaryCheck changed newState do (
	

		--	4 (Tile Node Boundary Check)
		
		runTileNodeBoundaryCheck = newState
		
	)
	
	on box_AuraEmitterCheck changed newState do (
	
		--	5 (Emitter Validity)
		
		runAuraEmitterCheck = newState
		
	)
	
	on box_AuraRefCheck changed newState do (
	
		--	6 (AuraRef Existence)
		
		runAuraRefCheck = newState
		
	)
	
	on box_NameCheck changed newState do (
	
		--	7 (Node Name Validity)
		
		runNameCheck = newState
		
	)
	
	on box_ScaleCheck changed newState do (
	
		--	8 (Zero World Scale)
		
		runScaleCheck = newState
		
	)
	
	on box_PartTextureCheck changed newState do (
	
		--	9 (Material Bitmap Name Chk)
		
		runPartTextureCheck = newState
		
	)
	
	on box_BitmapNameCheck changed newState do (
	
		--	10 (Bitmap Name Validity)
		
		runBitmapNameCheck = newState
		
	)
	
	on box_WeldCheck changed newState do (
	
		--	11 (Weld to Nearest cm)
		
		runWeldCheck = newState
		
	)
	
	on box_ClassCheck changed newState do (
	
		--	12 (Node Type 'boolean')
		
		runClassCheck = newState
		
	)
	
	on box_ControllerCheck changed newState do (
	
		--	13 (Valid Controller on Keys)
		
		runControllerCheck = newState
		
	)
	
	on box_AnimMeshCheck changed newState do (
	
		--	14 (Animated Mesh Validity)
		
		runAnimMeshCheck = newState
		
	)
	
	on box_DoganCheck changed newState do (
	
		--	15 (Texvert Validity)
		
		runDoganCheck = newState
		
	)
	
	on box_FaceCheck changed newState do (
	
		--	16 (Zero Faces on a Node)
		
		runFaceCheck = newState
		
	)
	
	on box_BSACheck changed newState do (
	
		--	17 (Bumpy-shiny Alignmt Chk)
		
		runBSACheck = newState
		
	)
	
	on box_AnimrootCheck changed newState do (
	
		--	18 (Animroot Node Existence)
		
		runAnimrootCheck = newState
		
	)
	
	on box_BoneCountCheck changed newState do (
	
		--	19 (Skinning: 4 bones per vert)
		
		runBoneCountCheck = newState
		
	)
	
	on box_WOKchecker changed newState do (
	
		--	20 (Check WOK)
		
		runWOKCheck = newState
		
	)
	
	on box_SkinCheck changed newState do (
	
		--	21 (Check for Skins)
		
		runSkinCheck = newState
		
	)
	
	on spn_animheight changed newValue do (
	
		local datalist = #()
		
		append datalist #("init" "animlistsize" (newValue as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
	
		g_guiAnimListSize = newValue
		
	)
	
	on chk_autodock changed newState do (
	
		local datalist = #()
		local dock
		
		if newState then (
		
			dock = 1
			
		) else (
		
			dock = 0
			
		)
		
		append datalist #("init", "dock", dock, (scriptsPath + "nwmax_plus\\nwmax.ini"))
		nx_setinivalue datalist
		
	)
	
	on btn_exportpath pressed do (
	
		local dirpath
		
		dirpath = getSavePath()
		
		if (dirpath != undefined) then (
		
			local datalist = #()
			
			append datalist #("exportpath", "dirpath", dirpath, (scriptsPath + "nwmax_plus\\nwmax.ini"))
			nx_setinivalue datalist
			
			gnx_exportPath = dirpath
			
		)
		
	)
	
	on btn_setPosition pressed do (
	
		local datalist = #()
		
		gnx_pos_nwmax_x = nwmax_handle.pos.x
		gnx_pos_nwmax_y = nwmax_handle.pos.y
		
		append datalist #("position", "nwmax_x", (gnx_pos_nwmax_x as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
		append datalist #("position", "nwmax_y", (gnx_pos_nwmax_y as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
		nx_setinivalue datalist
		
	)
	
)

rollout nx_util1 "General Utils" (

	local vp_layout
	local plugin_scripts

	--	Speedbar #1

	checkbutton btn_rollup "Dock NWgmax+" width:150 align:#center checked:false
	button btn_sel_parent "Parent" width:68 align:#left toolTip:"Select Parent" across:2
	button btn_sel_child "Child" width:68 align:#right toolTip:"Select Children"
	checkbutton btn_make_dummy_helper "Dummy" width:68 align:#left toolTip:"Create a stock Dummy helper" across:2
	button btn_environ "Set Environ" width:68 align:#right toolTip:"Set default unit scales etc."
	button btn_makeintodummy "IntoDummy" width:68 align:#left tooltop:"Make selected objects into Dummy helpers" across:2
	button btn_linker "Fast Linker" width:68 align:#right toolTip:"Link all selected objects to the AuroraBase in the selection"

	--	Speedbar #2

	button btn_tileslicer "Tile Slicer" width:150 align:#center
	button btn_modelscaler "Scale Wizard" width:150 align:#center
	button btn_massops "Mass Operations" width:150 align:#center
	button btn_export_all "Export All or Selected" width:150 align:#center toolTip:"Export selected models or all models if none selected."

	group "Plug-Ins" (

		dropdownlist dpl_prefabs width:105 height:10 align:#left across:2
		button btn_prefab_do "Go" width:25 align:#right

	)

	group "TileSet Creator" (
	
		button btn_setManager "Set Manager" align:#center toolTip:"Start TSC's Set Manager" width:144
	
	)
	
	--	Old speed bar code

	on nx_util1 open do (

		local temp = #()
		local i

		LoadPlugIns()
		
		plugin_scripts = #()

		--	Load scripts list into drop down

		for i = 1 to g_plugins.count by 2 do (

			append temp g_plugins[i]
			append plugin_scripts g_plugins[i + 1]

		)

		dpl_prefabs.items = temp

		--	Correctly set the floating button state

		try (

			tst_state = cui.getDockState nwmax_handle

			--	Docked

			btn_rollup.checked = false
			btn_rollup.text = "Float NWgmax+"
			
		) catch (
		
			--	Floating
			
			btn_rollup.checked = true
			btn_rollup.text = "Dock NWgmax+"
			
		)
		
		if (nx_existDir (scriptsPath + "tsc_plus")) then (
		
			btn_setManager.enabled = true
			btn_setManager.text = "Set Manager \"Plus\""
			tscDir = "tsc_plus"
			
		) else if (nx_existDir (scriptsPath + "nwn-set")) then (
		
			btn_setManager.enabled = true
			btn_setManager.text = "Set Manager"
			tscDir = "nwn-set"
			
		) else (
		
			btn_setManager.text = "Not Installed"
			btn_setManager.enabled = false
			tscDir = ""
			
		)
		
	)

	on btn_setManager pressed do (
	
		filein (scriptsPath + tscDir + "\\set-manager.ms")
		
	)

/*	
	on nx_util1 moved val do (
	
		if (btn_rollup.text == "Dock NWgmax+") then (
		
			--	Do nothing here
			
		) else (
		
			--	Store the current location of the NWgmax+ Speedbar
			
			local datalist = #()
			
			gnx_pos_nwmax_x = nwmax_handle.pos.x	--val.x
			gnx_pos_nwmax_y = nwmax_handle.pos.y	--val.y
			
			append datalist #("position", "nwmax_x", (gnx_pos_nwmax_x as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
			append datalist #("position", "nwmax_y", (gnx_pos_nwmax_y as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
			nx_setinivalue datalist
			
		)
		
	)
*/

	--	Deal with rollup/extend feature

	on btn_rollup changed val do (

		if val then (

			try (
			
				cui.FloatDialogBar nwmax_handle
				cui.UnRegisterDialogBar nwmax_handle

				nwmax_handle.pos.x = (gnx_pos_nwmax_x as float) - 8
				nwmax_handle.pos.y = (gnx_pos_nwmax_y as float) - 19
				nwmax_handle.size = [200, 600]
				btn_rollup.text = "Dock NWgmax+"
				nx_prefs.btn_setPosition.enabled = true
				
				max views redraw

			) catch (

				format "Error\r\n"

			)

		) else (

			try (

				nwmax_handle.size = [200, 600]
				
				cui.RegisterDialogBar nwmax_handle maxSize:[-1, -1] style:#(#cui_dock_vert, #cui_floatable, #cui_handles)
				cui.DockDialogBar nwmax_handle #cui_dock_left
				
				btn_rollup.text = "Float NWgmax+"
				nx_prefs.btn_setPosition.enabled = false

			) catch (

				format "Error\r\n"

			)

		)

	)

	--	Select parent

	on btn_sel_parent pressed do (

		if (selection[1] != undefined) then (

			if (selection[1].parent != undefined) then (

				select selection[1].parent

			)

		)

	)

	--	Select children

	on btn_sel_child pressed do (

		if (selection[1] != undefined) then (

			if (selection[1].children != undefined) then (

				select selection[1].children

			)

		)

	)

	--	Make a generic helper tool

	tool nwn_dummy_creator (

		local objDummy

		fn createDummy = (

			in coordsys grid (

				objDummy = dummy pos:gridPoint

			)

			objDummy.boxsize = [10, 10, 10]

		)

		on mousePoint clickNo do (

			if (clickNo == 1) then (

				createDummy()

			)

			select objDummy
			#stop

		)

	)

	on btn_make_dummy_helper changed state do (

		if state then (

			startTool nwn_dummy_creator

		)

		btn_make_dummy_helper.state = false

	)

	on btn_environ pressed do (

		units.displaytype = #Metric
		units.metrictype = #Centimeters
		units.SystemType = #Centimeters
		units.SystemScale = 1.0

	)

	on btn_linker pressed do (

		--	First make sure only one aurabase is in the selection

		local basecnt = 0
		local base = undefined

		for obj in selection do (

			if (iskindof obj aurorabase) then (

				basecnt += 1
				base = obj

				format "basecnt=%\r\n" basecnt
				format "base=%\r\n" base

			)

		)

		if (basecnt > 1) then (
		
			messagebox "More than one AuroraBase found in the selection." title:"Fast Linker"
			
			return 0
			
		)
		
		if (base == undefined) then (
		
			messagebox "No AuroraBase found in the selection." title:"Fast Linker"
			
			return 0
			
		)
		
		--	We only have one aurorabase so start the linking process

		for obj in selection do (

			if (obj != base) then (

				obj.parent = base

			)

		)

	)

	on btn_makeintodummy pressed do (

		--	Take the selected objects and make each into a dummy object

		clearlistener()

		local sel = selection as array
		local p			--	Position of s
		local r			--	Rotation of s
		local par		--	Parent of s
		local n			--	Name of s
		local newdummy

		for s in sel do (

			format "Processing: %\r\n" s

			p = s.pos
			r = s.rotation
			n = s.name
			par = s.parent
			newdummy = dummy pos:p
			newdummy.rotation = r
			newdummy.parent = par
			newdummy.boxsize = [10, 10, 10]

			for c in s.children do (

				c.parent = newdummy

			)

			select s

			max delete

			newdummy.name = n

		)

	)

	on btn_prefab_do pressed do (

		local index = dpl_prefabs.selection

		try (

			fileIn (scriptsPath + "nwmax_plus\\plugins\\" + plugin_scripts[index])

		) catch (

			--	Do Nothing Here

		)

	)

	--	Speed bar code stops

	on btn_tileslicer pressed do (

		try (

			filein (scriptsPath + "nwmax_plus\\TileSlicer\\tile-slicer.ms")

		) catch (

			--	Do Nothing Here

		)

	)

	on btn_modelscaler pressed do (

		try (

			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nwmax_scalewiz.ms")

		) catch (

			--	Do Nothing Here

		)

	)

	on btn_massops pressed do (

		try (

			filein (scriptsPath + "nwmax_plus\\nw_ie_inc\\nx_massutils.ms")

		) catch (

			--	Do Nothing Here

		)

	)

	on btn_export_all pressed do (

		ExportAll()

	)

)
