/*-----------------------------------------------------------------------------\

    NWmax Robe Scaler Helper Object

	NWMax Plus for gMax

    NWmax for NWN
    by Joco (jameswalker@clear.net.nz)

	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v6.11.13 --	Original compilation version
     
\-----------------------------------------------------------------------------*/

format "load: nx_mod_robescale.ms\r\n"

nx_progressPlus()

plugin SimpleMod nxrobescale name:"nxRobeScale" classID:#(0xbfe9687f, 0xcaae8c39) version:1 (

	on map i p do (
	
		--	We do nothing here
		--	Per BioWare:
		--	This is a simplemod so it expects us to be adjusting the point
		--	positions, but we dont want to do that.
		
	)
	
)
