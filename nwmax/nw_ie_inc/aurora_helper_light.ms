/*-----------------------------------------------------------------------------\

	Aurora Light Helper Object
	
	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
		---------------------------------------------------------------
		v5.04.22 --	Fix to Lens Flare Parameter box.
		---------------------------------------------------------------
		v6.11.12 --	Helpers are now being categorized under "Neverwinter
					Nights"
     
\-----------------------------------------------------------------------------*/

format "Load: aurora_helper_light.ms\r\n"

nx_progressPlus()

plugin helper auroralight name:"AuroraLight" classID:#(0x678e80c2, 0xb6c98009) version:2 extends:dummy category:"Neverwinter Nights" replaceUI:true invisible:true (

	parameters main rollout:params (
	
		rgb					type:#color		ui:colorpicker_color		default:(color 255 255 255)
		radius				type:#float		ui:spn_radius
		multiplier			type:#float		ui:spn_multiplier			default:1.0						animatable:true
		lightpriority		type:#integer	ui:spn_lightpriority		default:5						animatable:false
		nDynamicType		type:#integer	ui:box_isdynamic											animatable:false
		ambientOnly 		type:#integer	ui:rdoSurfaceType			default:1						animatable:false
		affectDynamic		type:#integer	ui:box_affectDynamic										animatable:false
		Shadow				type:#integer	ui:box_Shadow												animatable:false
		lensflares			type:#integer								default:0						animatable:false
		fadingLight			type:#integer	ui:box_fadingLight			default:1						animatable:false
		negativeLight		type:#integer	ui:box_negative												animatable:false
		
	)
	
	--	From BioWare.  Modified by Joco
	
	parameters flares rollout:lensflareUI (
	
		flareRadius			type:#float		ui:spn_flareRadius			default:5000
		
		/*
			lens_flares is a list of flares with in which the string encodes
			the flare information.  This makes it realatively easy to see the
			total number of flares and to delete those you don't want.  The
			flare data is held in the order:
			
				texture, flare size, position, color shift
		*/
		
		lens_flares			type:#stringtab												tabsizevariable:true
		
	)
	
	rollout params "MDL Light Parameters" (
	
		spinner spn_radius "Radius (cm): " width:106 fieldwidth:40 align:#right range:[0, 10000000000, 500]
		spinner spn_multiplier "Multiplier: " width:106 fieldwidth:40 align:#right
		spinner spn_lightpriority "Priority: " width:106 fieldwidth:40 align:#right type:#integer range:[1, 5, 5] scale:1
		checkbox box_fadingLight "Fading Light" align:#right
		
		group "Intensity/Color" (
		
			colorPicker colorpicker_color "" width:44 height:55 align:#left across:2
			spinner spn_red "Red: " width:85 fieldwidth:40 align:#right range:[0, 255, 255]
			spinner spn_green "Green: " width:85 fieldwidth:40 align:#right offset:[0, -39] range:[0, 255, 255]
			spinner spn_blue "Blue: " width:85 fieldwidth:40 align:#right range:[0, 255, 255]
			checkbox box_negative "Negative Light" align:#left
			
		)
		
		label lblSurfaceType "Surface Type:" align:#left
		radiobuttons rdoSurfaceType align:#left labels:#("Diffuse", "Ambient") default:1 columns:2
		checkbox box_isdynamic "Is Dynamic Light" align:#left
		checkbox box_affectDynamic "Affects Dynamic Objects" align:#left
		checkbox box_shadow "Shadows" align:#left
		
		group "Tile Light Presets" (
		
			radiobuttons rdo_mainlights labels:#("No Preset", "Mainlight 1", "Mainlight 2")
			
		)
		
		fn Mainlight_Preset suffix radius = (
		
			local parentname
			
			if ($.parent != undefined) then (
			
				parentname = $.parent.name
				
			) else (
			
				parentname = "INVALID_"
				
			)
			
			$.name = (parentname + suffix)
			$.radius = radius
			$.multiplier = 1.0
			$.fadingLight = 1
			$.rgb = color 0 0 0
			$.negativeLight = 0
			$.ambientOnly = 1
			$.nDynamicType = 0
			$.affectDynamic = 1
			$.shadow = 0
			$.flareRadius = 0
			
		)
		
		on rdo_mainlights changed arg do (
		
			if (arg == 2) then (
			
				Mainlight_Preset "ml1" 1400
				
			) else if (arg == 3) then (
			
				Mainlight_Preset "ml2" 500
				
			)
			
		)
		
		on colorpicker_color changed col do (
		
			spn_red.value = col.r
			spn_green.value = col.g
			spn_blue.value = col.b
			
		)
		
		on spn_red changed val do (
		
			colorpicker_color.color.r = val
			
		)
		
		on spn_green changed val do (
		
			colorpicker_color.color.g = val
			
		)
		
		on spn_blue changed val do (
		
			colorpicker_color.color.b = val
			
		)
		
		on params open do (
		
			spn_red.value = rgb.r
			spn_green.value = rgb.g
			spn_blue.value = rgb.b
--			colorpicker_color.color.r = rgb.r
--			colorpicker_color.color.g = rgb.g
--			colorpicker_color.color.b = rgb.b
			
		)
		
	)
	
	--	Originally from BioWare.  Modified by Joco
	
	rollout lensflareUI "Lens Flares" rolledUp:true (
	
		/*
			This rollout is kinda gross.  I needed some way of storing
			individual parameters for each separate flare, the total number of
			which could be increased/decreased on the fly.  As such, tieing
			variables to UI objects wasn't feasible, and required all sorts of
			update checks.
		*/
		
		/*	Commented out for the time being
		
		spinner spn_lensflares "Number of flares:" type:#integer range:[0,25,0] fieldWidth:30
		
		*/
		
		spinner spn_flareRadius "Flare Radius: " fieldwidth:50 type:#integer range:[0, 10000, 5000] enabled:true align:#right
		
		group "Flare Parameters" (
		
			listbox lst_flares "Flares" height:5
			button btn_add "Add" width:45 align:#left across:3
			button btn_delete "Delete" width:45 align:#center
			button btn_update "Update" width:45 align:#right
			edittext et_texname "Texture: " text:"" enabled:true
			button bt_gettexfile "Get Texture" width:140 align:#center enabled:true
			spinner spn_size "Size: " fieldwidth:40 align:#right type:#float range:[0.0, 10.0, 0.0] enabled:true
			spinner spn_pos "Position: " fieldwidth:40 align:#right type:#float range:[-10.0, 10.0, 0.0] enabled:true
			label posDescription "Camera Position" enabled:true
			
			groupbox gbxColorShift "Color Shift" width:145 height:85 align:#center
			
			colorPicker clrColorShift "" width:44 height:55 align:#left offset:[0, -70] across:2
			spinner spn_r "Red: " width:85 fieldwidth:40 align:#right offset:[0, -70] type:#float range:[-1.0, 1.0, 0.0] scale:0.001
			spinner spn_g "Green: " width:85 fieldwidth:40 align:#right offset:[0, -39] type:#float range:[-1.0, 1.0, 0.0] scale:0.001
			spinner spn_b "Blue: " width:85 fieldwidth:40 align:#right type:#float range:[-1.0, 1.0, 0.0] scale:0.001
			
			label lblSpacer_2 "" height:5
			
		)
		
		fn updateflarelist = (
		
			local tmpList = #()
			
			for f in lens_flares do (
			
				join tmpList #(f)
				
			)
			
			if (nx_strclassof(selection[1]) == "auroralight") then (
			
				selection[1].lensflareUI.lst_flares.items = tmpList
				
			)
			
		)
		
		on lensflareUI open do (
		
			updateflarelist()
			lst_flares.selection = 0
			clrColorShift.color.r = spn_r.value
			clrColorShift.color.g = spn_g.value
			clrColorShift.color.b = spn_b.value
			
		)
		
		on clrColorShift changed col do (
		
			spn_r.value = (col.r / 255)
			spn_g.value = (col.g / 255)
			spn_b.value = (col.b / 255)
			
		)
		
		on spn_r changed val do (
		
			clrColorShift.color.r = (val * 255)
			
		)
		
		on spn_g changed val do (
		
			clrColorShift.color.g = (val * 255)
			
		)
		
		on spn_b changed val do (
		
			clrColorShift.color.b = (val * 255)
			
		)
		
		on lst_flares selected selindex do (
		
			local tokens = filterString (trimleft(lens_flares[selindex])) " "
			
			if (tokens.count == 6) then (
			
				et_texname.text = tokens[1] as string
				spn_size.value = tokens[2] as float
				spn_pos.value = tokens[3] as float
				spn_r.value = tokens[4] as float
				spn_g.value = tokens[5] as float
				spn_b.value = tokens[6] as float
				
				--	Update the posDesc text
				
				local posVal = spn_pos.value
				
				if ((posVal > -0.05) and (posVal < 0.05)) then (
				
					posDescription.text = "Camera Position"
					
				) else if ((posVal > 0.95) and (posVal < 1.05)) then (
				
					posDescription.text = "Light Position"
					
				) else (
				
					posDescription.text = " "
					
				)
				
			) else (
			
				messageBox "Invalid Flare Listing!"
				
			)
			
		)
		
		on btn_add pressed do (
		
			local addStr = ""
		
			if ((trimleft(et_texname.text)).count > 0) then (
			
				local addStr = et_texname.text + " " + (spn_size.value as string) + " " + (spn_pos.value as string) + " " + (spn_r.value as string) + " " + (spn_g.value as string) + " " + (spn_b.value as string)
				
				append lens_flares addStr
				updateflarelist()
				
			) else (
			
				messageBox "Please Select a Texture!"
				
			)
			
		)
		
		on btn_delete pressed do (
		
			local selIndex = lst_flares.selection
			
			if (selIndex != 0) then (
			
				deleteItem lens_flares selIndex
				
			)
			
			updateflarelist()
			
		)
		
		on btn_update pressed do (
		
			local selIndex = lst_flares.selection
			
			if ((trimleft(et_texname.text)).count > 0) then (
			
				if (selIndex != 0) then (
				
					local addStr = et_texname.text + " " + (spn_size.value as string) + " " + (spn_pos.value as string) + " " + (spn_r.value as string) + " " + (spn_g.value as string) + " " + (spn_b.value as string)
					
					lens_flares[selIndex] = addStr
					
				)
				
				updateflarelist()
				
			)
			
		)
		
		on bt_gettexfile pressed do (
		
			try (
			
				f = getFilenameFile (getOpenFileName "Lens Flare Texture")
				et_texname.text = f
				
			) catch (
			
				--	Do Nothing Here
				
			)
			
		)
		
		on spn_pos changed arg do (
		
			--	By pushing the little arrows on the spinner enough times, 0.0
			--	no longer equals 0.0 even though 0.0 is displayed.  0.0
			--	eventually turns into 0.00000001 or something equally
			--	intelligent.  This necessitates the following tests for 0.0
			--	and 1.0
			
			if ((arg > -0.05) and (arg < 0.05)) then (
			
				posDescription.text = "Camera Position"
				
			) else if ((arg > 0.95) and (arg < 1.05)) then (
			
				posDescription.text = "Light Position"
				
			) else (
			
				posDescription.text = " "
				
			)
			
		)
		
	)		--	End "Lens Flares" rollout
	
)

plugin helper auroralightbox name:"AuroraLight" category:"Neverwinter Nights" (

	local mdaAuroraLight
	
	tool create (
	
		on mousePoint click do (
		
			if (click == 1) then (
			
				in coordsys grid (
				
					--	We only ever do this once.
					
					mdaAuroraLight = auroralight pos:[0, 0, 0]
					mdaAuroraLight.pos = gridPoint
					
				)
				
				mdaAuroraLight.delegate.boxSize = [10, 10, 10]
				select mdaAuroraLight
				
				--	Enter modify mode to exit mouse tool and get ready for name
				--	updates.
				
				max modify mode
				
			)
			
			return #stop
			
		)
		
	)
	
)
