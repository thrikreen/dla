/*-----------------------------------------------------------------------------\

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)

	Plus
	by Michael DarkAngel (www.tbotr.net)

	Credits:

		Wayland Reid:

			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script

		Zaddix:

			Based on code from Zaddix's original WOK file importer

		BioWare:

			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.

	Legal Stuff:

		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.

		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.

		3.	Credit has been given where code or methods have been integrated
			from other sources.

		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.

		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.

	Version Info:

		v1.09.26 --	Original compilation version
		v1.12.08 --	Fixed an issue with the "Plus" Pivot tool
		---------------------------------------------------------------
		v3.07.22 --	Added Tile Lifter
		v3.10.14 --	Added "Zoom Sel" and "Zoom All" buttons
		---------------------------------------------------------------
		v4.08.23 --	Added "MDL Mirror" from roboius' Tile Slicer
		---------------------------------------------------------------
		v5.05.27 --	Added hide fading objects functionality
		v5.05.29 --	Added functionality to "Hide Fading Objects"
		---------------------------------------------------------------
		v6.11.13 --	"Clean" Model Re-Pivot has been disabled until the process
					can run clean of errors.

\-----------------------------------------------------------------------------*/

global nx_mesh_desparkle

rollout nwmaxPlus "\"Plus\"" (

	button btn_mdaClone "Clone Selected" width:150 align:#center toolTip:"ChilliSkinner's Clone Routine"
	button btn_mdaZoomSel "Zoom Sel" across:2 align:#left toolTip:"Active View Only" width:67
	button btn_mdaZoomAll "Zoom All" align:#right toolTip:"Active View Only" width:67
	button btn_mdaMassTess "Mass Tessellate" width:150 align:#center toolTip:"Tessellate all MDL files in a selected directory"
	button btn_mdaWokCheck "Walkmesh Checker" width:150 align:#center toolTip:"Check Walkmesh for Errors"
	button btn_mdaRemoveShadows "Remove Shadows" width:150 align:#center toolTip:"Turn off shadows for selected meshes"
	checkbutton btnHideFade "Hide Fading Objects" align:#center width:150 tooltip:"Hide objects with tilefade>1"

	group "\"Clean\" Model" (

		checkbox chk_mdaRotCheck "Zero Rotation" checked:runMDARotCheck
		checkbox chk_mdaDesparkle "Desparkle" checked:runMDADesparkleCheck
		checkbox chk_mdaWeldVerts "Weld Verts" checked:runMDAWeldVertsCheck
		checkbox chk_mdaAutoSmooth "AutoSmooth" checked:runMDAAutoSmoothCheck offset:[15, 0]
		checkbox chk_mdaWeldTVerts "Weld TVerts" checked:runMDAWeldTVertsCheck
		checkbox chk_mdaNormalTVerts "Normalize TVerts" checked:runMDANormalTVerts offset:[15, 0]
		checkbox chk_mdaRepivot "Re-Pivot" checked:false enabled:false
		button btn_desparkle "\"Clean\" Model" width:144 align:#center toolTip:"Desparkle, Weld Verts and Auto Smooth, Weld TVerts and Re-Pivot to the best of my ability."
		--listbox lst_mdaBadPivots "Bad Pivots" width:140 height:10 align:#center

		/*	Commented out for the time being

		button btn_mdaTestPivot "Test Pivoting" width:144 height:18 align:#center

		*/

	)

	group "Tile Lifter" (
	
		button btn_mdaLift "Adjust Models" width:150 align:#center toolTip:"Adjust the height of the child objects of all tiles in the scene"
		spinner spn_mdaMulti "Multiplier:" range:[-5, 5, 1] type:#integer scale:1 align:#right fieldwidth:48
		spinner spn_mdaHeight "Height Change:" range:[0, 10000, 500] type:#worldunits scale:10 align:#right fieldwidth:48
		
	)

	group "MDL Mirror" (
	
		button btnMirror "Mirror Models" tooltip:"Mirrors the child nodes of the selected models around the model center" width:150
		radiobuttons rdoMirror "" default:1 labels:#("X", "Y")
		
	)
	
	group "Pivot" (

		radiobuttons rdo_pivotx "" align:#center labels:#("-X ", "Center ", "+X") default:2
		radiobuttons rdo_pivoty "" align:#center labels:#("-Y ", "Center ", "+Y") default:2
		radiobuttons rdo_pivotz "" align:#center labels:#("-Z ", "Center ", "+Z") default:2
		--button btn_centerpivots "Center Pivots" tooltip:"Centers Pivot of selected meshes" width:120 height:18
		checkbox chk_pivotint "Force Integer" align:#left checked:false
		checkbox chk_snappivot "Snap Only" align:#left checked:false
		button btn_setpivotxyz "Set Pivots" width:144 align:#center toolTip:"Set Pivot of selected meshes, use \"Force Integer\" to force integer position on the pivot."

	)

	group "Select" (

		spinner spn_selectIndex "Sel #: " fieldwidth:40 align:#left type:#integer range:[1, 100000, 1] across:2
		radiobuttons rdo_selectIndex align:#right labels:#("Vert", "Edge", "Face")

	)

	group "Set Verts" (
	
		label lblAbs "Abs" align:#left
		checkbox chkAbsX checked:false across:3
		spinner spn_vposx "X: " fieldwidth:55 type:#float range:[-100000.0, 100000.0, 0.0] scale:0.01
		button btn_getvertpos "Get" width:40 align:#right toolTip:"Get Center position of selected Verts, can be used across multiple meshes"
		checkbox chkAbsY checked:false across:3
		spinner spn_vposy "Y: " fieldwidth:55 type:#float range:[-100000.0, 100000.0, 0.0] scale:0.01
		button btn_weldvertpos "Set" width:40 align:#right toolTip:"Set Selected Verts to position, can be used across multiple meshes"
		checkbox chkAbsZ checked:false across:3
		spinner spn_vposz "Z: " fieldwidth:55 type:#float range:[-100000.0, 100000.0, 0.0] scale:0.01
		Checkbox chk_snapvalues "Snap" align:#right checked:true
		
	)

	fn hideFading = (
	
		hideFade = #()
		
		max select all
		
		for obj in selection do (
		
			local node_mods_count = (obj.modifiers).count
			
			if (node_mods_count > 0) then (
			
				if (obj.modifiers[1].name == "AuroraTrimesh") then (
				
					if (obj.modifiers[1].tilefadeprop > 1) then (
					
						append hideFade obj
						
						hide obj
						
					)
					
				)
				
			)
			
		)
		
		if (hideFade.count == 0) then (
		
			messageBox "Nothing to hide." beep:true title:"Nothing to Hide"
			
			btnHideFade.checked = false
			
		)
		
	)
	
	fn unhideFading = (
	
		if (hideFade.count > 0) then (
		
			for obj in hideFade do (
			
				unhide obj
				
			)
			
		)
		
	)
	
	fn checkHide = (
	
		if (btnHideFade.checked == true) then (
		
			btnHideFade.checked = false
			
			unhideFading()
		
		)
	
	)

	fn mirror_point mirror x1 x2 = (
	
		local ret
		
		if (mirror) then (
		
			ret = x2 - (x1 - x2)
			
		) else (
		
			ret = x1
			
		)
		
		return ret
		
	)
	
	on btnHideFade changed checkState do (
	
		case ((nx_btoi checkstate)) of (
		
			0: (
			
				unhideFading()
			
			)
			
			1: (
			
				hideFading()
				
			)
		
		)
		
		max select none
	
	)
	
	on btnMirror pressed do undo on (
	
		local axis = rdoMirror.state - 1
		
		for mdl in selection do (
		
			if (iskindof mdl Model or iskindof mdl aurabase) then (
			
				--	Hmmm, what about objects linked to the anim node?
				
				for obj in mdl.children do (
				
					local orig = copy obj
					local x, y, z
					
					--	Collapse the entire modifier stack.
					
					collapsestack obj
					
					--	Mirror the position manually.
					
					x = mirror_point (axis == 0) obj.pos.x mdl.pos.x
					y = mirror_point (axis == 1) obj.pos.y mdl.pos.y
					z = obj.pos.z
					obj.pos = [x, y, z]
					
					if (iskindof obj Editable_Mesh) then (
					
						--	Snap vertices on Editable Meshes.
						
						snap_vertices obj
						
						--	Add the mirror modifier.
						
						local m = mirror()
						
						addmodifier obj m
						
						m.mirror_axis = axis
						
						--	Reset Transforms on the given object.
						
						collapsestack obj
						reset_transforms obj
						collapsestack obj
						
					)
					
					--	Restore the original modifiers.
					
					copy_modifiers obj orig
					delete orig
					
				)
				
			)
			
		)
		
	)
	
	on btn_mdaZoomAll pressed do (
	
		--	Code borrowed from ChilliSkinner by
		--	Colin "Chilli" Semple

		set undo off
		max tool zoomextents all
		set undo on
		
	)
	
	on btn_mdaZoomSel pressed do (
	
		--	Code borrowed from ChilliSkinner by
		--	Colin "Chilli" Semple

		set undo off
		max zoomext sel
		set undo on
		
	)

	on btn_mdaClone pressed do (

		--	Code borrowed from ChilliSkinner by
		--	Colin "Chilli" Semple

		disablesceneredraw()

		undo on (

			Objs = $
			TmpVal = PrelimCheck Objs false true false

			if (TmpVal == false) do (

				return false

			)

			TmpVal = CloneHidePolys Objs

			if (TmpVal == false) do (

				CSError "Clone + Hide Polys Error"

			)

			if (ccAutoSelect == true) do (

				max select all

			)

			if (ccAutoZoom == true) do (

				max tool zoomextents all

			)

		)

		enablesceneredraw()

	)

	on btn_mdaLift pressed do undo on (
	
		if (selection.count > 0) then (
		
			for obj in selection do (
			
				if ((iskindof obj aurorabase) and (obj.classification_sel == 2)) then (
				
					for child in obj.children do (
					
						if (child.name != "ignore_NGon01") then (
						
							child.pos.z = child.pos.z + spn_mdaMulti.value * spn_mdaHeight.value
						
						)
					
					)
				
				)
			
			)
		
		) else (
		
			for obj in objects do (
			
				if ((iskindof obj aurorabase) and (obj.classification_sel == 2)) then (
				
					for child in obj.children do (
					
						if (child.name != "ignore_NGon01") then (
						
							child.pos.z = child.pos.z + spn_mdaMulti.value * spn_mdaHeight.value
							
						)
						
					)
					
				)
				
			)
			
		)
		
	)
	
	on btn_mdaMassTess pressed do (

		try (

			filein(scriptsPath + "nwmax_plus\\nw_ie_inc\\mda_massTessellate.ms")

		) catch (
		)

	)

	on btn_mdaRemoveShadows pressed do undo on (

		for obj in selection do (

			local node_mods_count = (obj.modifiers).count

			if (node_mods_count > 0) then (

				if (obj.modifiers[1].name == "AuroraTrimesh") then (

					obj.modifiers[1].shadow = 0

				)

			)

		)

		format "Shadows Removed\r\n"

	)

	on lst_mdaBadPivots doubleClicked itemSel do (

		select(getNodeByName lst_mdaBadPivots.items[itemSel])

	)

	on btn_mdaTestPivot pressed do undo on (

		for obj in selection do (

			numFaces = getNumFaces obj

			for i = 1 to numFaces do (

				format "Normal Face #% : %\r\n" i (getFaceNormal obj i)

			)

		)

	)

	on chk_mdaWeldVerts changed newState do (

		if (newState == false) then (

			runMDAAutoSmoothCheck = newState
			chk_mdaAutoSmooth.checked = newState
			chk_mdaAutoSmooth.enabled = newState

		) else (

			chk_mdaAutoSmooth.enabled = newState

		)

		runMDAWeldVertsCheck = newState

	)

	--	Perform MDA's "Clean" Model routine.

	on btn_desparkle pressed do (

		if (nx_strclassof selection[1]) == "aurorabase" then (

			clearListener()
			repivotMessage = 0
			mdaBadPivots = #()
			lst_mdaBadPivots.items = mdaBadPivots
			nx_mesh_desparkle selection[1]

			format "\"Clean\" Model complete\r\n"

			if (repivotMessage == 1) then (

				MessageBox "Meshes were re-pivoted, please run ResetXForm prior to exporting!"

			)

		) else (

			MessageBox "No AuroraBase selected."

		)

	)

	--	End of "Clean" Model routine

	on btn_mdaWokCheck pressed do (

		try (

			filein (scriptsPath + "nwmax_plus/nw_ie_inc/nx_woktool.ms")

		) catch ()

	)

	fn getmax o = (
	
		local mx = o.center.x
		local my = o.center.y
		local mz = o.center.z
		local verts = o.verts
		
		for v in verts do (
		
			if (v.pos.x >= mx) then (
			
				mx = v.pos.x
				
			)
			
			if (v.pos.y >= my) then (
			
				my = v.pos.y
				
			)
			
			if (v.pos.z >= mz) then (
			
				mz = v.pos.z
				
			)
			
		)
		
		return #(mx, my, mz)
		
	)
	
	fn getmin o = (
	
		local mx = o.center.x
		local my = o.center.y
		local mz = o.center.z
		local verts = o.verts
		
		for v in verts do (
		
			if (v.pos.x <= mx) then (
			
				mx = v.pos.x
				
			)
			
			if (v.pos.y <= my) then (
			
				my = v.pos.y
				
			)
			
			if (v.pos.z <= mz) then (
			
				mz = v.pos.z
				
			)
			
		)
		
		return #(mx, my, mz)
		
	)
	
	fn round_value v = (
	
		local vmod
		
		if (v >= 0.0) then (
		
			vmod = floor(v + 0.5)
			
		) else (
		
			vmod = ceil(v - 0.5)
			
		)
		
		return vmod
		
	)
	
	on chk_snappivot changed val do (

		local s

		if (val == true) then (

			s = false

		) else (

			s = true

		)

		rdo_pivotx.enabled = s
		rdo_pivoty.enabled = s
		rdo_pivotz.enabled = s
		chk_pivotint.enabled = s

	)

	on btn_setpivotxyz pressed do (

		local ox, oy, oz
		local i = 0
		local omax, omin

		if (chk_snappivot.checked == false) then (

			for obj in selection do (

				i += 1

				if (superclassof obj == GeometryClass) then (

					omax = getmax obj
					omin = getmin obj
					ocenter = obj.center

					if (rdo_pivotx.state == 1) then (

						ox = omin[1]

					) else if (rdo_pivotx.state == 2) then (

						ox = obj.center.x

					) else if (rdo_pivotx.state == 3) then (

						ox = omax[1]

					)

					if (rdo_pivoty.state == 1) then (

						oy = omin[2]

					) else if (rdo_pivoty.state == 2) then (

						oy = obj.center.y

					) else if (rdo_pivoty.state == 3) then (

						oy = omax[2]

					)

					if (rdo_pivotz.state == 1) then (

						oz = omin[3]

					) else if (rdo_pivotz.state == 2) then (

						oz = obj.center.z

					) else if (rdo_pivotz.state == 3) then (

						oz = omax[3]

					)

					--	Force integer pivot position

					if chk_pivotint.checked then (

						ox = round_value ox
						oy = round_value oy
						oz = round_value oz

					)

					obj.pivot = [ox, oy, oz]

				)		--	Superclass geom

			)		--	For obj

		) else (		--	Snap pivot.checked

			for obj in selection do (

				i += 1

				if (superclassof obj == GeometryClass) then (

					ox = round_value obj.pivot.x
					oy = round_value obj.pivot.y
					oz = round_value obj.pivot.z
					obj.pivot = [ox, oy, oz]

				)

			)

		)

	)

	fn mesh_subobject_select = (

		if (selection.count != 1) then (

			messageBox "Can only be performed on a single mesh!"

			return false

		)

		local o = selection[1]
		local val = spn_selectIndex.value

		if (classof selection[1] == Editable_mesh) then (

			--	Max modify mode

			try (

				subobjectLevel = rdo_selectIndex.state

			) catch (

				--	Do Nothing Here

			)

			case rdo_selectIndex.state of (

				1: (

					if (val <= selection[1].verts.count) then (

						selection[1].selectedVerts = #(val)

					) else (

						spn_selectIndex.value = selection[1].verts.count

					)

				)

				2: (

					if (val <= selection[1].edges.count) then (

						selection[1].selectedEdges = #(val)

					) else (

						spn_selectIndex.value = selection[1].edges.count

					)

				)

				3: (

					if (val <= selection[1].faces.count) then (

						setFaceSelection selection[1] #(val)

					) else (

						spn_selectIndex.value = selection[1].faces.count

					)

				)

			)

		)

	)

	on spn_selectIndex changed val do (

		mesh_subobject_select()

	)

	on rdo_selectIndex changed arg do (

		mesh_subobject_select()

	)
	
	on btn_getvertpos pressed do (
	
		local sv = #()
		local vpos = #()
		local tx = 0, ty = 0, tz = 0
		
		for i = 1 to selection.count do (
		
			sv[i] = getVertSelection selection[i]
			
			for v in sv[i] do (
			
				append vpos (getVert selection[i] v)
				
			)
			
		)
		
		for v in vpos do (
		
			tx = tx + v.x
			ty = ty + v.y
			tz = tz + v.z
			
		)
		
		tx = tx / vpos.count
		ty = ty / vpos.count
		tz = tz / vpos.count
		
		--	Round values
		
		if chk_snapvalues.checked then (
		
			local x, y, z
			
			if (tx >= 0.0) then (
			
				tx = floor(tx + 0.5 )
				
			) else (
			
				tx = ceil(tx-0.5)
				
			)
			
			if (ty >= 0.0) then (
			
				ty = floor(ty + 0.5 )
				
			) else (
			
				ty = ceil(ty-0.5)
				
			)
			
			if (tz >= 0.0) then (
			
				tz = floor(tz + 0.5)
				
			) else (
			
				tz = ceil(tz-0.5)
				
			)
			
		)
		
		spn_vposx.value = tx
		spn_vposy.value = ty
		spn_vposz.value = tz
		
	)
	
	on btn_weldvertpos pressed do (
	
		with undo on (
		
			local tx, ty, tz
			
			tx = spn_vposx.value
			ty = spn_vposy.value
			tz = spn_vposz.value
			
			for obj in selection do (
			
				for i = 1 to obj.selectedVerts.count do (
				
					v = obj.selectedVerts[i]
					
					if chkAbsX.checked then (
					
						ty = v.pos.y
						tz = v.pos.z
						
					) else if chkAbsY.checked then (
					
						tx = v.pos.x
						tz = v.pos.z
						
					) else if chkAbsZ.checked then (
					
						tx = v.pos.x
						ty = v.pos.y
						
					)
					
					meshop.setVert obj v.index [tx, ty, tz]
					
				)
				
				update obj
				
			)
			
		)
		
	)
	
	fn oppValue val = (
	
		if (val == true) then (
		
			newVal = false
			
		) else (
		
			newVal = true
			
		)
		
		return newVal
	
	)
	
	on chkAbsX changed val do (
	
		newVal = oppValue val
	
		chkAbsY.enabled = newVal
		chkAbsZ.enabled = newVal
		spn_vposy.enabled = newVal
		spn_vposz.enabled = newVal
	
	)
	
	on chkAbsY changed val do (
	
		newVal = oppValue val
	
		chkAbsX.enabled = newVal
		chkAbsZ.enabled = newVal
		spn_vposx.enabled = newVal
		spn_vposz.enabled = newVal
	
	)
	
	on chkAbsZ changed val do (
	
		newVal = oppValue val
	
		chkAbsX.enabled = newVal
		chkAbsY.enabled = newVal
		spn_vposx.enabled = newVal
		spn_vposy.enabled = newVal
	
	)
	
)
