/*-----------------------------------------------------------------------------\

	Cryomesh
	
	Store the links, relative parent pos and object offset data in a flat cryo
	file.

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
     
\-----------------------------------------------------------------------------*/

global gNX_exestr
global gNX_fileh

rollout nx_cryomesh "Cryo Mesh" width:162 (

	edittext edt_file "File: " width:150 align:#center
	button btn_browse "Browse" width:150 align:#center
	button btn_save "Save" width:66 align:#left across:2
	button btn_load "Restore" width:66 align:#right
	checkbox chk_excludeposdata "Load ONLY Hierarchy" align:#left checked:false
	listbox lst_names width:150 height:10 align:#center
	button btn_assign "Assign Name" width:150 align:#center
	
	fn NXcryodiver node = (
	
		local objoffsetpos, objoffsetrot, objoffsetscale, position
		local name, parent
		local child
		
		name = node.name
		
		if (node.parent == undefined) then (
		
			parent = "NULL"
			
		) else (
		
			parent = node.parent.name
			
		)
		
		objoffsetpos = node.objectoffsetpos
		objoffsetrot = node.objectoffsetrot
		objoffsetscale = node.objectoffsetscale
		
		in coordsys parent (
		
			position = node.pos
			
		)
		
		if (name == undefined) then (
		
			name = "NULL"
			
		)
		
		--	Save data
		
		if g_ismax then (
		
			format "%|%|%|%|%|%\n" name parent objoffsetpos objoffsetrot objoffsetscale position to:gNX_fileh
			
		) else (
		
			format "%|%|%|%|%|%~\n" name parent objoffsetpos objoffsetrot objoffsetscale position
			
		)
		
		for child in node.children do (
		
			NXcryodiver child
			
		)
		
	)
	
	fn NXcryomeshload file = (
	
		local fileh = openFile file mode:"r"
		local fileline = ""
		local linetoks
		local data = #()
		local node, parent
		
		if (fileh == undefined) then (
		
			format "ERROR: Can't open file:%\r\n" file
			
			return false
			
		)
		
		--	Load in the data from file
		
		while not eof(fileh) do (
		
			fileline = readLine fileh
			linetoks = filterString fileline "|"
			
			--	Remember data order is:
			--	name parent objoffsetpos objoffsetrot objoffsetscale position
			
			append data linetoks
			
		)
		
		--	Close the file
		
		if (fileh != undefined) then (
		
			close fileh
			
		)
		
		--	Relink, Position, and Pivot correct
		
		for d in data do (
		
			node = getNodeByName d[1]
			parent = getNodeByName d[2]
			
			if (node != undefined) then (
			
				node.parent = parent
				
				--	Position the mesh pieces correctly in relation to their
				--	parents
				
				gNX_exestr = "in coordsys parent $" + node.name + ".pos=" + d[6]
				execute(gNX_exestr)
				
				--	Pivot adjust
				
				gNX_exestr = "$" + node.name + ".objectoffsetpos=" + d[3]
				execute(gNX_exestr)
				
				--	Rotation adjust
				
				gNX_exestr = "$" + node.name + ".objectoffsetrot=" + d[4]
				execute(gNX_exestr)
				
				--	Scale adjust
				
				gNX_exestr = "$" + node.name + ".objectoffsetscale=" + d[5]
				execute(gNX_exestr)
				
			)
			
		)
		
	)
	
	fn NXcryomeshsave modelbase = (
	
		if ((not(iskindof modelbase aurorabase)) and (not(iskindof modelbase aurabase))) then (
		
			format "ERROR: No Modelbase Selected!\r\n"
			
			return false
			
		)
		
		local extn = ""
		
		if not(matchPattern edt_file.text pattern:"*.cyo") then (
		
			extn = ".cyo"
			
		)
		
		if g_ismax then (
		
			gNX_fileh = createFile (edt_file.text + extn)
			
			if (gNX_fileh != undefined) then (
			
				NXcryodiver modelbase
				flush gNX_fileh
				close gNX_fileh
				
			) else (
			
				format "Error: File Not Created!\r\n"
				
			)
			
		) else (
		
			clearListener()
			
			format "<snoopstart file=%>~\n" (edt_file.text + extn)
			
			NXcryodiver modelbase
			
			format "</snoopstart>~\n"
			format "</snoopend>~\r\n"
			
		)
		
	)
	
	on btn_browse pressed do (
	
		local filename = getOpenFileName caption:"Open Crymesh file" types:"Cryomesh(*.cyo)|*.cyo|All|*.*|"
		
		if (filename != undefined) then (
		
			local fileh = openFile filename mode:"r"
			local filedata
			local listdata = #()
			
			while not eof(fileh) do (
			
				filedata = filterString (readline fileh) "|"
				append listdata filedata[1]
				
			)
			
			lst_names.items = listdata
			edt_file.text = filename
			
		)
		
	)
	
	on btn_assign pressed do (
	
		if (lst_names.selection != 0) then (
		
			selection[1].name = lst_names.selected
			
		)
		
	)
	
	on btn_save pressed do (
	
		NXcryomeshsave selection[1]
		
	)
	
	on btn_load pressed do (
	
		NXcryomeshload edt_file.text
		
	)
	
)

createDialog nx_cryomesh
