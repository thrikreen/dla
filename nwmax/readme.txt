(Preferred method) Quick install instructions for auto start users:

1. Extract the zip file to the gmax\scripts directory. You should now have a new folder called NWmax (case is not important)
2. move the file autonwmax.ms to the folder <some path>\gmax\scripts\startup\
3. start gmax

NWmax should now start automatically



Quick install instructions for users wanting to start NWmax by hand
1. Extract the zip file to the gmax\scripts directory. You should now have a new folder called NWmax (case is not important)
2. start gmax
3. run the script nwmax.ms