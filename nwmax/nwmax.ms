/*-----------------------------------------------------------------------------\

	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.09.27 --	Original compilation version
		v1.12.08 --	Fixed an issue with the "Plus" Pivot tool
		---------------------------------------------------------------
		v2.01.25 --	Fixed the character.nam animation slot file.
		---------------------------------------------------------------
		v3.07.22 --	Export Path preferences enabled, Added wokmat.ini that
					includes 1.69 walkmesh materials (thanks to pstemarie),
					Added Tile Lifter
		v3.09.22 --	Fixed a bug with the Clean Model Routine
					Helper creation tweaks (thanks to OldTimeRadio)
					Fixed a "Division by Zero" bug in aurora_mod_animesh.ms
		v3.10.14 --	Added "Zoom Sel" and "Zoom All" buttons to NWgMax Plus
		v3.10.15 --	Fixed an issue that was causing the script to not run
					under 3DSMax
		---------------------------------------------------------------
		v4.08.23 --	Tile Slicer is now a "Plus" product.
					"MDL Mirror" has been moved from Tile Slicer to Plus.
		v4.08.25 --	Addition of "Check for Skins" to sanity checks.
					Robe and Armor Maker is now a "Plus" product.
		---------------------------------------------------------------
		v5.01.12 --	Fixes to aurora_fn_export to solve the 65536 issue.
		v5.02.23 --	Fix to allow for "Obscuring" and "No Walk" as valid
					selections for pwk and dwk walkmeshes.
		v5.04.22 --	Fix to Lens Flare Parameter box.
		v5.04.26 --	Fix to 'Set Sanity Check'.  Now saves Sanity Check
					preferences.
					Fix to the enitre Sanity Check preference routine.  Now
					acknowledges Sanity Check preferences and loads them
					accordingly.
		v5.04.27 --	Turned 'Reset X-form' off for Skinned meshes.
		v5.05.17 --	Addition of cappear2 and cdisappear2 to creature
					animation list.  Fix to the number of frames for ccastout.
		v5.05.18 --	cappear2 and cdisappear2 added to creature animation list.
					Really
		v5.05.27 --	Added hide fading objects and reload last imported model
					functionality.
		v5.05.29 --	Added functionality to "Reload" and "Hide Fading Objects"
					Added the ability to turn off rounding during export.
		v5.06.01 --	Fixed a rounding issue when dealing with skins.
					Fixed the "Model Base Wizard to work the same as it does in
					3DSMax.
		v5.06.08 --	Removed Texture Tools rollout, Reset Export Dir from
					General Utilities rollout.
		---------------------------------------------------------------
		v6.11.19 --	Prototyping for Autodock preference.
					"Clean" Model Re-Pivot has been disabled.
					Override "nxlocalprefs.ini" settings file has been
					eliminated.
		v6.11.20 --	Fixed rollout size.
		v6.11.28 --	Fixed missing gnx_exportPath global variable
     
\-----------------------------------------------------------------------------*/

--	Ensure stack and heap size are large enough

if (heapSize < 400000000) then (

	heapSize = 400000000
)

if (stackLimit < 400000000) then (

	stackLimit = 400000000
	
)

global gNX_maxver = (maxversion())[1]

ClearListener()

global gblVersion = "v6.11.28"

format "------------------------------------------------------------\r\n"
format "|                                                          |\r\n"
format "|                   NWgmax Plus %                   |\r\n" gblVersion
format "|                                                          |\r\n"
format "------------------------------------------------------------\r\n"

--	Global Variables and init setups

global DEBUG = false		--	Debug flag
global SPEEDT = false		--	Speed timer flag
global strIndent1 = "  "
global strIndent2 = "    "
global strIndent3 = "      "
global strIndent4 = "        "
global strIndent5 = "          "
global gnx_pos_nwmax = ""
global gnx_pos_nwmax_x = ""
global gnx_pos_nwmax_y = ""

--	Load nwmax ini global vars

format "NWmax ini load\r\n"

global g_3dsmax = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "init" "usemax"
global g_guiAnimListSize = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "init" "animlistsize"
global g_verbose = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "init" "verbose"
global gNX_dock = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "init" "dock"
global gNX_visible = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "init" "visible"
global gnx_sanity = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "init" "sanity"
global g_basepointer = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "init" "basepointer"
global gnx_pos_nwmax = undefined
global gnx_pos_nwmax_x = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "position" "nwmax_x"
global gnx_pos_nwmax_y = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "position" "nwmax_y"
global gnx_exportPath = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "exportpath" "dirpath"

if ((gnx_pos_nwmax_x == undefined) or (gnx_pos_nwmax_x == "")) then (

	gnx_pos_nwmax_x = 10
	gnx_pos_nwmax_y = 100
	
) else (

	gnx_pos_nwmax = "set"
	
)

--	Set globals for 3dsmax v's gmax

format "Set globals for 3dsmax v's gmax\r\n"

global g_fextn = ""
global g_gmaxini = ""
global g_ismax = false

if (g_3dsmax == "1") then (

	g_fextn = ".max"
	g_gmaxini = "3dsmax.ini"
	g_ismax = true
	
) else (

	g_fextn = ".gmax"
	g_gmaxini = "gmax.ini"
	g_ismax = false
	
)

--	Set variables that did not get set by preferences

format "\nBefore - Export Path = %\n" gnx_exportPath

if ((gnx_exportPath == undefined) or (gnx_exportPath == "")) then (

	gnx_exportPath = getDir #export

)

format "\n After - Export Path = %\n" gnx_exportPath

--	Correct ini vars for undefined and set required defaults and type conversion

format "Correct ini vars\r\n"

if (g_guiAnimListSize == undefined) then (

	g_guiAnimListSize = 10
	
) else (

	g_guiAnimListSize = (g_guiAnimListSize as integer)
	
)

if (g_verbose == undefined) then (

	g_verbose = 0
	
) else (

	g_verbose = (g_verbose as integer)
	
)

if (gNX_dock == undefined) then (

	gNX_dock = 0
	
) else (

	gNX_dock = (gNX_dock as integer)
	
)

if (gNX_visible == undefined) then (

	gNX_visible = 0
	
) else (

	gNX_visible = (gNX_visible as integer)
	
)

if ((gnx_sanity == undefined) or (gnx_sanity == "")) then (

	gnx_sanity = "110011111101111101111"
	
)

if (g_basepointer == undefined) then (

	g_basepointer = 1
	
) else (

	g_basepointer = (g_basepointer as integer)
	
)

--	Load in all paths from gmax.ini file

format "Load in all paths\r\n"

global g_maps_path = #()
iniMaps = ""
i = 1

while (iniMaps = (getINISetting (scriptsPath + "..\\" + g_gmaxini) "BitmapDirs" ("Dir" + (i as string)))) != "" do (

	append g_maps_path iniMaps
	
	i = i + 1

)

if g_maps_path.count == 0 then (

	format "No Bitmap Paths loaded!\r\n"
	
) else (

	format "Maps search dirs:\r\n"
	
	print g_maps_path
	
)

--	Exporting stream buffer

format "Setup exporting stream\r\n"

if g_ismax then (

	global g_strBuffer = undefined			--	File stream buffer for exporting
	global g_delim = "\n"					--	Buffered export system delimter
	
) else (

	global g_strBuffer = stringStream ""	--	StringStream buffer for compressed export system
	
	if (g_verbose == 1) then (
	
		g_delim = "~\n"
		
	) else (
	
		g_delim = "~"
		
	)
	
)

--	End 3dsmax v's gmax

global g_startpath = ""

if (autostart == undefined) then (

	autostart = false
	
)

if (autostart == true) then (

	g_startpath = "nwmax_plus/"
	
)

--	Material surfaces used in WOK walkmeshes

global matsurfs = #("Dirt", "Obscuring", "Grass", "Stone", "Wood", "Water", "Nonwalk", "Transparent", "Carpet", "Metal", "Puddles", "Swamp", "Mud", "Leaves", "Lava", "BottomlessPit", "DeepWater", "Door", "Snow", "Sand", "BareBones", "StoneBridge", "Temp1", "Temp2", "Temp3", "Temp4", "Temp5", "Temp6", "Temp7", "Trigger")
global g_plugins = #()	--	List of plugins
global g_compiler = scriptsPath + "nwmax_plus\\mdlcomp\\nwnmdlcomp.exe"
global g_exportPath = ""
global nwmax_handle		--	Handle to the master floater window
global raFloater		--	Handle for the Robe and Armor Maker window

--	Global stack to be used for files and generic stack routines

global g_FileHandles = #()

--	Specific import globals

global lastDanglyName = undefined
global AnimRootNodeName = undefined
global LastAnimNumber = undefined
global LastAnimEventNumber = undefined

--	Specific export globals

global animIndex = 0		--	Used by animmesh routine for modelbase.animation index

--	Used in plug in load % bar

global percent = 0
global com_load_total = 9
global com_load = 0

--	UI flags for active dialogs

global nx_animkeymaster_flag = false
global nx_armourcutter_flag = false
global nx_ramaker_flag = false
global nx_skindumper_flag = false

--	Global execute str

global nx_exec_str

--	These are for the Active Sanity Check rollout in aurorabase per Bioware's
--	export scripts code.

fn nx_ib i = (

	if (i == 0) then (
	
		return false
		
	) else (
	
		return true
		
	)
	
)

format "Set Sanity check default settings\r\n"

global runRotationCheck = (nx_ib (gnx_sanity[1] as integer))
global runAnimCheck = (nx_ib (gnx_sanity[2] as integer))
global runCollisionMeshCheck = (nx_ib (gnx_sanity[3] as integer))
global runTileNodeBoundaryCheck = (nx_ib (gnx_sanity[4] as integer))
global runAuraEmitterCheck = (nx_ib (gnx_sanity[5] as integer))
global runAuraRefCheck = (nx_ib (gnx_sanity[6] as integer))
global runNameCheck = (nx_ib (gnx_sanity[7] as integer))
global runScaleCheck = (nx_ib (gnx_sanity[8] as integer))
global runPartTextureCheck = (nx_ib (gnx_sanity[9] as integer))
global runBitmapNameCheck = (nx_ib (gnx_sanity[10] as integer))
global runWeldCheck = (nx_ib (gnx_sanity[11] as integer))
global runClassCheck = (nx_ib (gnx_sanity[12] as integer))
global runControllerCheck = (nx_ib (gnx_sanity[13] as integer))
global runAnimMeshCheck = (nx_ib (gnx_sanity[14] as integer))
global runDoganCheck = (nx_ib (gnx_sanity[15] as integer))
global runFaceCheck = (nx_ib (gnx_sanity[16] as integer))
global runBSACheck = (nx_ib (gnx_sanity[17] as integer))
global runAnimrootCheck = (nx_ib (gnx_sanity[18] as integer))
global runBoneCountCheck = (nx_ib (gnx_sanity[19] as integer))
global runWOKCheck = (nx_ib (gnx_sanity[20] as integer))
global runSkinCheck = (nx_ib (gnx_sanity[21] as integer))

--	These are for MDA's "Clean" Model routine

format "Set \"Clean\" Model routine default settings\r\n"

global runMDARotCheck = true
global runMDADesparkleCheck = true
global runMDAWeldVertsCheck = true
global runMDAAutoSmoothCheck = true
global runMDAWeldTVertsCheck = true
global runMDANormalTVerts = true
global runMDARePivotCheck = false
global repivotMessage = 0
global mdaBadPivots = #()

--	Include in all the global function prototypes

format "Load Function Prototypes\r\n"

filein (g_startpath + "nw_ie_inc\\aurora_fn_proto.ms")

--	Structures

format "Define structs and utility fns\r\n"

struct nx_Tokenizer (

	--	Used to parse out the imported mdl file
	
	tokens = #(),
	
	fn SetString str = (
	
		str = trimright(trimleft(str))
		tokens = filterString str "\t ,"
		
	),
	
	fn ReadToken = (
	
		if (tokens.count > 0) then (
		
			local tok = tokens[1]
			
			deleteItem tokens 1
			
			if DEBUG then (
			
				format "ReadToken: %\r\n" tok
				
			)
			
			tok
			
		) else (
		
			undefined
			
		)
		
	),
	
	fn PeekToken = (
	
		if (tokens.count > 0) then (
		
			tokens[1]
			
		) else (
		
			undefined
			
		)
		
	),
	
	fn ReadPoint3 = (
	
		local x = ReadToken() as float
		local y = ReadToken() as float
		local z = ReadToken() as float
		
		if DEBUG then (
		
			format "ReadPoint3: %\r\n" (point3 x y z)
			
		)
		
		(point3 x y z)
		
	),
	
	fn ReadFloat = (
	
		(ReadToken() as float)
		
	),
	
	fn ReadInteger = (
	
		(ReadToken() as integer)
		
	),
	
	fn ReadString = (
	
		(ReadToken())
		
	),
	
	fn ReadColor = (
	
		local r = 255 * (ReadToken() as float)
		local g = 255 * (ReadToken() as float)
		local b = 255 * (ReadToken() as float)
		
		(color r g b)
		
	),
	
	fn ReadBoolean = (
	
		local b = ReadToken()
		
		if ((nx_lowercase b) == "true") then (
		
			true
			
		) else (
		
			false
			
		)
		
	),
	
	fn ReadRemainingTokens = (
	
		local str = ""
		
		while (PeekToken() != undefined) do (
		
			str += ReadToken() + " "
			
		)
		
		substring str 1 (str.count - 1)
		
	)
	
)

struct nx_MultiArray (

	--	Base array store
	
	ArrayData = #(),
	width = 1,
	
	fn append row = (
	
		if (row.count != width) then (
		
			return false
			
		)
		
		join ArrayData row
		
		true
		
	),
	
	fn delete row = (
	
		local realIndex, i
		
		realIndex = row * width
		
		if (ArrayData.count < (realIndex + width - 1)) then (
		
			return false
			
		)
		
		for i = realIndex to (realIndex + width - 1) do (
		
			deleteItem ArrayData i
			
		)
		
		true
		
	),
	
	fn get row col = (
	
		local realIndex, i
		
		realIndex = ((row - 1) * width) + col
		
		if (ArrayData.count < realIndex) then (
		
			return false
			
		)
		
		ArrayData[realIndex]
		
	),
	
	fn size = (
	
		ArrayData.count / width
		
	)
	
)

struct nx_SkinWeights (

	--	Used to store the skin object and weights data
	
	Object,
	WeightsData = #()
	
)

--	End Structure Definitions

fn nx_coderefresh = (

	filein (g_startpath + "nw_ie_inc\\aurora_fn_general.ms")
	filein (g_startpath + "nw_ie_inc\\aurora_fn_sanity.ms")
	filein (g_startpath + "nw_ie_inc\\aurora_fn_export.ms")
	filein (g_startpath + "nw_ie_inc\\aurora_gui_speedbtns.ms")
	filein (g_startpath + "nw_ie_inc\\nwn_gui_import.ms")
	filein (g_startpath + "nw_ie_inc\\nwn_gui_about.ms")
	filein (g_startpath + "nw_ie_inc\\nwn_gui_updaters.ms")
	
	--	NWMax Plus
	
	filein (g_startpath + "nw_ie_inc\\nwmax_plus.ms")
	
	max views redraw
	
)

--	General functions

filein (g_startpath + "nw_ie_inc\\aurora_fn_general.ms")

--	Start progress for loading

progressStart "PlugIns Loading ..."

--	Export functions

filein (g_startpath + "nw_ie_inc\\aurora_fn_sanity.ms")
filein (g_startpath + "nw_ie_inc\\aurora_fn_export.ms")

--	Scripted Plug-Ins

filein (g_startpath + "nw_ie_inc\\aurora_mod_trimesh.ms")
filein (g_startpath + "nw_ie_inc\\aurora_mod_flex.ms")
filein (g_startpath + "nw_ie_inc\\aurora_mod_walkmesh.ms")
filein (g_startpath + "nw_ie_inc\\aurora_mod_placeable.ms")
filein (g_startpath + "nw_ie_inc\\aurora_mod_animesh.ms")
filein (g_startpath + "nw_ie_inc\\aurora_helper_reference.ms")
filein (g_startpath + "nw_ie_inc\\aurora_helper_base.ms")
filein (g_startpath + "nw_ie_inc\\aurora_helper_light.ms")
filein (g_startpath + "nw_ie_inc\\aurora_helper_emitter.ms")
filein (g_startpath + "nw_ie_inc\\aurora_helper_tools.ms")

--	Rollout definitions

filein (g_startpath + "nw_ie_inc/aurora_gui_speedbtns.ms")
filein (g_startpath + "nw_ie_inc/nwn_gui_import.ms")
filein (g_startpath + "nw_ie_inc/nwn_gui_about.ms")

--	NWMax Plus

filein (g_startpath + "nw_ie_inc/nwmax_plus.ms")

--	Set callbacks

callbacks.removeScripts id:#mdaCheckHideCB

callbacks.addScript #systemPreNew "nwmaxplus.checkHide()" id:#mdaCheckHideCB
callbacks.addScript #systemPreReset "nwmaxplus.checkHide()" id:#mdaCheckHideCB

--	Rollout Window creation

fn nx_make_floater = (

	nwmax_handle = newRolloutFloater "NWgmax Plus" 208 612
	addRollout nx_util1			nwmax_handle rolledUp:true	--	"General Utilities"
	addRollout nwmaxPlus		nwmax_handle rolledUp:true	--	"Plus"
	addRollout nx_util2			nwmax_handle rolledUp:true	--	"Robes/Armor/Skin"
	addRollout nx_util3			nwmax_handle rolledUp:true	--	"Mesh Tools"
	addRollout nx_util4			nwmax_handle rolledUp:true	--	"Anim Tools"
	addRollout nx_util5			nwmax_handle rolledUp:true	--	""
	addRollout ImportRollout    nwmax_handle rolledUp:true	--	"MDL Loading"
	addRollout nx_prefs			nwmax_handle rolledUp:true	--	"Preferences"
	addRollout AboutRollout     nwmax_handle rolledUp:false	--	"Ego Stroker"
	
	--	Load in old position from local ini file
	
	nwmax_handle.pos.x = (gnx_pos_nwmax_x as float) - 12
	nwmax_handle.pos.y = (gnx_pos_nwmax_y as float) - 16
	nwmax_handle.size = [208, 612]
	
)

fn nx_destroy_floater = (

	if (nwmax_handle != undefined) do (
	
		--	Make sure floater is not docked and unregister it
		
		try (
		
			cui.FloatDialogBar nwmax_handle
			cui.UnRegisterDialogBar nwmax_handle
			
		) catch (
		
			--	Do Nothing Here
			
		)
		
		--	Close it.
		
		closerolloutfloater nwmax_handle
		
	)
	
)

--	Try to close out a floater if it already exists

nx_destroy_floater()

if (gNX_visible == 1) then (

	nx_make_floater()
	
	if (gNX_dock == 1) then (
	
		nwmax_handle.size = [208, 612]
	
		--	Register floater as a dockable bar
		
		cui.RegisterDialogBar nwmax_handle maxSize:[-1, -1] style:#(#cui_dock_vert, #cui_floatable, #cui_handles)
		
		--	By default dock the bar
		
		cui.DockDialogBar nwmax_handle #cui_dock_left
		nx_util1.btn_rollup.checked = false
		nx_util1.btn_rollup.text = "Float NWgmax+"
		
	) else (
	
		nx_util1.btn_rollup.checked = true
		nx_util1.btn_rollup.text = "Dock NWgmax+"
		
	)
	
)

--	Set app title

cui.setAppTitle ("NWgmax Plus " + gblVersion)

if (IsSceneRedrawDisabled() == true) do (

	enablesceneredraw()
	
)

--	GUI updaters

filein (g_startpath + "nw_ie_inc\\nwn_gui_updaters.ms")

-- selChanged

fn nx_selChanged = ()

--	Call this function once to set all the UI elements properly

nx_selChanged()

format "----------------------------Done----------------------------\r\n"

progressEnd()

max views redraw
