/*-----------------------------------------------------------------------------\

	Armor Cutter
	
	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v6.11.13 --	Original compilation version

\-----------------------------------------------------------------------------*/

/*
	Script to cut an NPC model into armour parts
	
	By Joco (James Walker).
	Date: 28-Dec-2003
	
	Comments: Path to look for the base models is hard-coded. Change as required.
*/

fn nx_cutarmour basename arnumber makesuper:false = (

	local robeparts = #("head_g", "neck_g", "Lshoulder_g", "lbicep_g", "lforearm_g", "lhand_g", "Rshoulder_g", "rbicep_g", "rforearm_g", "rhand_g", "torso_g", "belt_g", "pelvis_g", "lthigh_g", "lshin_g", "lfoot_g", "rthigh_g", "rshin_g", "rfoot_g")
	local armourparts = #("head", "neck", "shol", "bicepl", "forel", "handl", "shor", "bicepr", "forer", "handr", "chest", "belt", "pelvis", "legl", "shinl", "footl", "legr", "shinr", "footr")
	
	clearListener()
	
	format "Armor cutting started:\r\n"
	
	--	Get the template name to use to drive the armor part loading.
	
	local templateBase = undefined
	
	for h in $helpers do (
	
		if (iskindof h aurorabase) then (
		
			templateBase = h
			
			--	Set base name correctly.
			
			templateBase.name = basename
			
			exit
			
		)
		
	)
	
	if templateBase == undefined then (
	
		messageBox "No model base found!"
		
		return 0
		
	)
	
	local templateName = templateBase.name
	local toLoad, i, snode, tnode, gnode, modelbase, partname, partdummy
	
	--	Load it all in first.
	
	for i = 1 to robeparts.count do (
	
		--	Get the target source node and target nodes then reposition.
		
		tnode = getNodeByName robeparts[i]
		
		if tnode != undefined then (
		
			modelbase = aurorabase()
			modelbase.pos = tnode.pos
			modelbase.delegate.boxSize = [100, 100, 5]
			
			--	Make the pointer to the front.
			
			local tri = ngon()
			tri.radius = 25
			tri.nsides = 3
			tri.rotation *= (quat 90 [0, 0, 1])
			tri.pos = tnode.pos + [0, 50, 0]
			tri.wireColor = (color 92 179 240)
			tri.name = "ignore_" + tri.name
			tri.parent = modelbase
			
			--	Set the name of the new model base correctly.
			
			modelBase.name = basename + "_" + armourparts[i] + arnumber
			
			--	Set the target nodes name to the correct standard.
			
			tnode.name = basename + "_" + armourparts[i] + arnumber + "g"
			
			--	Make super dummies if needed.
			
			if makesuper then (
			
				partdummy = dummy boxsize:[5, 5, 5]
				partdummy.pos = tnode.pos
				partdummy.name = robeparts[i]
				
			)
			
			--	Link to new modelbase.
			
			tnode.parent = modelbase
			
		) else (
		
			format "% not found\r\n" robeparts[i]
			
		)
		
	)
	
	--	If we are not making a super then go no further.
	
	if not makesuper then (
	
		--	Exit early
		
		return 1
		
	)
	
	--	Tidy up some "special nodes" and make sure they are all linked.
	--	Re-link our normal nodes using array of node/parent pairs.
	
	local skeletonparts = #("head_g", "neck_g", "head", "head_g", "neck_g", "torso_g", "Lshoulder_g", "lbicep_g", "lbicep_g", "torso_g", "lforearm_g", "lbicep_g", "lforearm", "lforearm_g", "lhand_g", "lforearm_g", "lhand", "lhand_g", "Rshoulder_g", "rbicep_g", "rbicep_g", "torso_g", "rforearm_g", "rbicep_g", "rhand_g", "rforearm_g", "rhand", "rhand_g", "torso_g", "rootdummy", "impact", "torso_g", "belt_g", "pelvis_g", "pelvis_g", "rootdummy", "lthigh_g", "pelvis_g", "lshin_g", "lthigh_g", "lfoot_g", "lshin_g", "rthigh_g", "pelvis_g", "rshin_g", "rthigh_g", "rfoot_g", "rshin_g", "tail", "pelvis_g", "wings", "torso_g", "handconjure", "base", "headconjure", "base", "rootdummy", "base")
	
	format "Linking supermodel\r\n"
	
	local tparent = undefined
	
	for i = 1 to skeletonparts.count by 2 do (
	
		tnode = getNodeByName skeletonparts[i]
		
		if skeletonparts[i+1] == "base" then (
		
			tparent = templateBase
			
		) else (
		
			tparent = getNodeByName skeletonparts[i + 1]
			
		)
		
		if (tnode != undefined) and (tparent != undefined) then (
		
			tnode.parent = tparent
			
		) else (
		
			format "Failed link: %  --> %\r\n" (skeletonparts[i]) (skeletonparts[i + 1])
			
		)
		
	)
	
)

rollout nx_armourcutter "Armor Cutter" (

	group "" (
	
		edittext edt_basename "Base Name:" text:"pmh0" width:150
		edittext edt_armournum "Number:" text:"001" width:150
		checkbox chk_createsuper "Create Super"
		label lbl_msg "Ready" align:#center width:150 height:40
		
	)
	
	button btn_cutarmour "Cut Armor" width:165
	
	on nx_armourcutter open do (
	
		nx_armourcutter_flag = true
		
	)
	
	on nx_armourcutter close do (
	
		nx_armourcutter_flag = false
		
	)
	
	on btn_cutarmour pressed do (
	
		nx_cutarmour (edt_basename.text) (edt_armournum.text) makesuper:(chk_createsuper.checked)
		
		lbl_msg.text = "Done cutting. Review listener for missed body parts."
		
		format "Armor cutting ended.\r\n"
		
	)
	
)

if not nx_armourcutter_flag then (

	createdialog nx_armourcutter style:#(#style_titlebar, #style_border, #style_sysmenu, #style_minimizebox) width:180
	
)
