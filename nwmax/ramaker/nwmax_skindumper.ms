/*-----------------------------------------------------------------------------\

	Skin Dumper
	
	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v6.11.13 --	Original compilation version

\-----------------------------------------------------------------------------*/

rollout nx_skindumper "Skin Dumper" (

	edittext edt_dirPath "Dir:" align:#left width:155
	button btn_browse "Set Path" width:160
	
	group "Save Details" (
	
		edittext edt_filename "File Name:" align:#left width:155
		label lbl1 "Comment:" align:#left
		edittext edt_comment align:#left width:155
		button btn_save "Save" width:160
		
	)
	
	group "Available Skin Files" (
	
		listbox lst_files height:10
		label lbl_comment "" height:60
		button btn_load "Load" width:160
		
	)
	
	label lbl_status ""
	
	fn NX_getskindata s = (
	
		--	Build up skin weight data.
		
		local swl, weightBuffer, strStream
		
		weightBuffer = #()
		
		--	Code from Bioware.  To export bone weights, modified for this
		--	particular purpose.
		
		local save_selection = selection
		
		max modify mode
		select s
		
		local i, j, zeroWeights
		local m = s.modifiers["Skin"]
		
		if m == undefined then (
		
			return #()
			
		)
		
		local n = skinops.getnumbervertices m
		
		for i = 1 to n do (
		
			local w_num = skinops.getvertexweightcount m i
			local counter = 0
			
			strStream = stringStream ""
			
			for j = 1 to w_num do (
			
				local bone_id = (skinops.getvertexweightboneid m i j)
				local bone_name = "root"
				local weight_i_j = 1
				
				if bone_id > 0 then (
				
					bone_name = (skinops.getbonename m bone_id 1)
					
				)
				
				weight_i_j = (skinops.getvertexweight m i j)
				
				if weight_i_j != 0.0 then (
				
					format "  % % "  bone_name weight_i_j to:strStream
					
					counter += 1
					
				)
				
			)
			
			if (w_num == 0) or (counter == 0) then (
			
				format "  root 1.00 " to:strStream
				
			)
			
			append weightBuffer (strStream as string)
			
		)
		
		if save_selection != undefined then (
		
			select save_selection
			
		)
		
		swl = (nx_SkinWeights (s.name) weightBuffer)
		
		swl
		
	)
	
	fn NX_savedata sdata filename = (
	
		ClearListener()
		
		--	Start the file.
		
		format "<snoopstart file=%>%" filename g_delim to:g_strBuffer
		format "\n%" g_delim to:g_strBuffer
		format "\n%" g_delim to:g_strBuffer
		format "\n%" g_delim to:g_strBuffer
		
		--	Put in comment.
		
		format "%%" ("#" + edt_comment.text) g_delim to:g_strBuffer
		
		--	Write out the skin obj name as the first line.
		
		format "%%" (sdata.Object) g_delim to:g_strBuffer
		
		--	Cycle through all the data lines and export them all a row at a
		--	time.
		
		for wstr in sdata.WeightsData do (
		
			format "%%" wstr g_delim to:g_strBuffer
			
			nx_FlushBuffer()
			
		)
		
		--	Close the file.
		
		format "\n%" g_delim to:g_strBuffer
		
		--	Close exportcontrol.txt
		
		format "</snoopstart>%" g_delim to:g_strBuffer
		format "</snoopend>%" g_delim to:g_strBuffer
		
		nx_FlushBuffer force:true
		
	)
	
	fn NX_loaddata filename = (
	
		local sdata = #()
		local skinobj
		local fileh = openFile filename mode:"r"
		local fileline = ""
		local comment = ""
		
		if fileh == undefined then (
		
			format "ERROR: Can't open file:%\r\n" filename
			
			return false
			
		)
		
		--	Get firstline.  It might be either comment or skin obj, depending
		--	on version.
		
		fileline = readLine fileh
		
		if fileline[1] == "#" then (
		
			comment = fileline
			fileline = readLine fileh
			
		)
		
		skinobj = getNodeByName fileline
		
		if skinobj == undefined then (
		
			return false
			
		)
		
		--	Delete any skin modifier if exists.
		
		md = skinobj.modifiers["skin"]
		
		if md != undefined then (
		
			deleteModifier skinobj md
			
		)
		
		addModifier skinobj (skin())
		
		--	Load in the data from file.
		
		while not eof(fileh) do (
		
			fileline = readLine fileh
			
			append sdata fileline
			
		)
		
		close fileh
		
		--	Data is loaded.
		
		addMDLSkin skinobj sdata
		
	)
	
	fn nx_refresh_files path = (
	
		local files = getFiles path
		local temp = #()
		
		for f in files do (
		
			append temp (getFilenameFile f)
			
		)
		
		lst_files.items = temp
		
	)
	
	on nx_skindumper open do (
	
		nx_skindumper_flag = true
		edt_dirPath.text = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "skindumper" "filepath"
		
		if (edt_dirPath.text == undefined) or (edt_dirPath.text == "") then (
		
			edt_dirPath.text = scriptsPath + "nwmax_plus\\scratch\\"
			
		)
		
		--	Refresh the available .skn list.
		
		nx_refresh_files (edt_dirPath.text + "*.skn")
		
	)
	
	on nx_skindumper close do (
	
		nx_skindumper_flag = false
		
	)
	
	on lst_files selected val do (
	
		--	Set the new comment field.
		
		local comment = ""
		
		if lst_files.selected != undefined then (
		
			local filename = edt_dirPath.text + lst_files.selected + ".skn"
			local fileh = openFile filename mode:"r"
			local fileline = ""
			
			if fileh == undefined then (
			
				format "ERROR: Can't open file:%\r\n" filename
				
				return false
				
			)
			
			fileline = readLine fileh
			
			if fileline[1] == "#" then (
			
				comment = fileline
				comment[1] = " "
				
			)
			
			close fileh
			
		)
		
		lbl_comment.text = comment
		
	)
	
	on btn_browse pressed do (
	
		local pth = getSavePath()
		
		if pth != undefined then (
		
			edt_dirPath.text = pth + "\\"
			
		)
		
		nx_refresh_files (edt_dirPath.text + "*.skn")
		
	)
	
	on btn_save pressed do (
	
		local sel = selection[1]
		
		if selection.count > 1 then (
		
			messagebox "More than one object selected!"
			
			return false
			
		)
		
		if sel == undefined then (
		
			messagebox "No object selected!"
			
			return false
			
		)
		
		if sel.modifiers["skin"] == undefined then (
		
			messagebox "No skin selected!"
			
			return false
			
		)
		
		local sdata = NX_getskindata sel
		
		select sel
		
		NX_savedata sdata (edt_dirPath.text + edt_filename.text + ".skn")
		
		lbl_status.text = "Save Done."
		
	)
	
	on btn_load pressed do (
	
		if lst_files.selected == undefined then (
		
			lbl_status.text = "No file chosen."
			
			return false
			
		)
		
		--	Make sure file exists.
		
		if not (nx_existFile (edt_dirPath.text + lst_files.selected + ".skn")) then (
		
			lbl_status.text = "File not found."
			
			return false
			
		)
		
		NX_loaddata (edt_dirPath.text + lst_files.selected + ".skn")
		
		lbl_status.text = "Load Done."
		
	)
	
)

if not nx_skindumper_flag then (

	createDialog nx_skindumper style:#(#style_titlebar, #style_border, #style_sysmenu, #style_minimizebox) width:180
	
)