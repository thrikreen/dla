/*-----------------------------------------------------------------------------\

	Robe and Armor Maker
	
	NWMax Plus for gMax

	NWmax
	by Joco (jameswalker@clear.net.nz)
	
	Plus
	by Michael DarkAngel (www.tbotr.net)
	
	Credits:
	
		Wayland Reid:
		
			Extends Wayland's (wreid@spectrumanalytic.com) Import/Export script
			
		Zaddix:
		
			Based on code from Zaddix's original WOK file importer
			
		BioWare:
		
			This code is based on the information gleaned from BioWares 3DS Max
			scripts.  In many cases the Bioware code is used instead of
			"re-inventing the wheel".  I have attempted to note all those
			places where I have used the Bioware code.
			
	Legal Stuff:
	
		1.	This is free software and is provided with no explicit or implied
			warranty.  If you use this software then you do so at your own risk.
			
		2.	If there is any law in your country that requires the author to
			provide any form of support or indemnity on the use of the this
			software you are not therefore authorized to use said software as no
			such indemnity or support will be provided (per 1) other than at the
			authors discretion.
			
		3.	Credit has been given where code or methods have been integrated
			from other sources.
			
		4.	In the case of code used from the Bioware scripts their intellectual
			property rights still hold.
			
		5.	No code has been knowingly included from other sources where that
			code is sold for commercial purposes.
			
	Version Info:
	
		v1.08.25 --	Original compilation version
		---------------------------------------------------------------
		v6.11.13 --	Cleaned up code.
					Made rollout prettier.
					Switched to nwmax.ini from nxlocalprefs.ini file.

\-----------------------------------------------------------------------------*/

global rloRAUtilities

if (g_linkCopy == undefined) then (

	global g_linkCopy = #()
	
)

--	Robe and Armor Part Builder

rollout nx_ramaker "Robe and Armor Maker" (

	local basemodels = #()
	
	--	List box for ref models
	
	group "Reference Models" (
	
		listbox lst_basemodels "" height:10 width:160
		
		--	Browse btn/edit with add btn to id models to add
		
		button btn_browsemodel "Browse" width:165
		button btn_delete "Delete" width:165
		editText edt_loadpattern "Pattern:" align:#left width:160
		button btn_loadpattern "Load Pattern" width:165
		
	)
	
	--	Browse btn/edit for export path
	
	group "Export Path" (
	
		edittext edt_path "Dir: " align:#left text:(getdir #export) width:160
		button btnResetDir "Reset" across:2 align:#left toolTip:"Reset the export directory of all selected model bases to the directory set in Preferences." width:75
		button btnBrowseDir "Browse" align:#right toolTip:"Select a temporary export path" width:75
		
	)
	
	--	Scale INI file
	
	group "Scaling INI File" (
	
		dropdownList drp_scaleini "" width:160
		label lbl_inidescr "" height:85 width:165
		
	)
	
	group "Race Processing and Over-rides" (
	
		checkbox chk_human "Human" across:2 checked:false
		checkbox chk_elf "Elf" checked:false
		checkbox chk_dwarf "Dwarf" across:2 checked:false
		checkbox chk_halfling "Halfling" checked:false
		checkbox chk_gnome "Gnome" across:2 checked:false
		checkbox chk_orc "Half Orc" checked:false
		radiobuttons rdo_process_sex "Sex Processing Over-rides:" align:#left default:1 labels:#("Default", "Male", "Female")
		radiobuttons rdo_process_pheno "Pheno Processing Over-rides:" align:#left default:1 labels:#("Default", "0", "2")
		label lbl_scaleadj "Global Scale Adjust\n(added to scale factors):" align:#left height:30
		spinner spn_x "x:" across:3 align:#left range:[0, 1, 0] width:50
		spinner spn_y "y:" align:#center range:[0, 1, 0] width:50
		spinner spn_z "z:" align:#right range:[0, 1, 0] width:50
		checkbox chk_snappos "Snap Pivots for Robe Bones" checked:true
		
	)
	
	--	Scale start buttons
	
	group "Build the Robes or Armor" (
	
		button btn_build_armour "Build Armor" toolTip:"Build Armor using the base models and supplied Scale File" width:165
		button btn_build_robes "Build Robes" toolTip:"Build Robes using the base models and supplied Scale File" width:165
		
	)
	
	group "Status" (
	
		listbox lst_history "" height:10 width:160
		label lbl_status "Ready" align:#center width:165
		
	)
	
	--	Rollout Functions
	
	fn nxra_updateList = (
	
		local tmp = #()
		local m
		
		for m in basemodels do (
		
			--	Only add in the file name
			
			append tmp (getFilenameFile m)
			
		)
		
		lst_basemodels.items = tmp
		
	)
	
	--	Scale the armor parts using a recursive pattern.  The scale factor is
	--	only searched for once.  When found it's not looked for again.
	
	fn scaleArmourPart node objSelect scaleData race sex pheno tm_scale:undefined = (
	
		format "Node: %\n" node.name
		
		--	Perform the scale
		
		lbl_status.text = "Scale geom ..."
		
		--	Based on the node part, race and sex get the scale matrix to use
		
		local i, r, s, p, part
		
		if ((tm_scale == undefined) or (tm_scale == [1, 1, 1])) then (
		
			--	If tm_scale is "undefined"
			
			tm_scale = [1, 1, 1]
			
			--	Determine scale to use
			
			for i = 1 to (scaleData.size()) do (
			
				r = ((scaleData.get i 2)[3] == race)
				s = ((scaleData.get i 2)[2] == sex)
				p = ((scaleData.get i 2)[4] == pheno)
				
				--	Build the part name
				
				part = scaleData.get i 1
				
				if (((findstring (nx_lowercase(node.name)) part) != undefined) and r and s and p) then (
				
					tm_scale = scaleData.get i 3
					
					format "Scale Found\n"
					
					exit
					
				)
				
			)
			
		)
		
		--	Adjust scale factor to anything that is an editable mesh only
		
		if (iskindof node Editable_Mesh) then (
		
			tm_scale.x += spn_x.value
			tm_scale.y += spn_y.value
			tm_scale.z += spn_z.value
			
		)
		
		format "Scale Factor %\n" tm_scale
		
		--	Scale anything but skins
		
		if (node.modifiers["Skin"] == undefined) then (
		
			node.scale = tm_scale
			
		)
		
		for part in node.children do (
		
			scaleArmourPart part objSelect scaleData race sex pheno tm_scale:tm_scale
			
		)
		
	)
	
	--	Scale the robe parts using a recursive pattern.  The scale factor is
	--	only searched for once.  When found it's not looked for again.
	
	fn scaleRobePart node scaleData race sex pheno tm_scale:undefined = (
	
		--	All the robe body parts
		
		local bodyparts = #("neck_g", "head_g", "head", "torso_g", "Lshoulder_g", "lbicep_g", "lforearm_g", "lforearm", "lhand_g", "lhand", "Rshoulder_g", "rbicep_g", "rforearm_g", "rhand_g", "rhand", "pelvis_g", "belt_g", "rthigh_g", "rshin_g", "rfoot_g", "lthigh_g", "lshin_g", "lfoot_g", "handconjure", "headconjure", "rootdummy", "Impact", "wings", "tail", "belt1_g", "FB1_g01", "FB2_g01", "FB4_g01", "TF1_g", "TF1L_g", "TF1R_g" ,"TF2_g", "TF3_g01", "Cloak_g", "CL1_fg", "CL1_g", "CL2_g", "CL3_g", "CL4_g", "cloak_shL", "cloak_shR", "CM1_g", "CM2_g", "CM3_g", "CM4_g", "CR1_fg", "CR1_g", "CR2_g", "CR3_g", "CR4_g", "Coat_top", "Arm_L", "Arm_R", "coat_root", "coat_BL1", "coat_BL1a", "coat_BL2", "coat_BR1", "coat_BR1a", "coat_BR2", "coat_FL1", "coat_FL1a", "coat_FL2", "coat_FR1", "coat_FR1a", "coat_FR2")
		
		format "Node: %\n" node.name
		
		--	Perform the scale
		
		lbl_status.text = "Scale geom..."
		
		--	Based on the node part, race and sex get the scale matrix to use
		
		if ((tm_scale == undefined) or (tm_scale == [1, 1, 1]) or ((findItem bodyparts node.name) != 0)) then (
		
			tm_scale = [1, 1, 1]
			
			local i, p, part, r, s
			
			--	Determine scale
			
			for i = 1 to (scaleData.size()) do (
			
				r = ((scaleData.get i 2)[3] == race)
				s = ((scaleData.get i 2)[2] == sex)
				p = ((scaleData.get i 2)[4] == pheno)
				part = scaleData.get i 1
				
				if ((part == nx_lowercase(node.name)) and r and s and p) then (
				
					tm_scale = scaleData.get i 3
					
					format "Scale Found\n"
					
					exit
					
				)
				
			)
			
		)
		
		--	Adjust scale factor to anything that is an Editable_Mesh only
		
		if (iskindof node Editable_Mesh) then (
		
			tm_scale.x += spn_x.value
			tm_scale.y += spn_y.value
			tm_scale.z += spn_z.value
			
		)
		
		format "Scale Factor %\n" tm_scale
		
		--	Don't scale if is a skin mesh or if is the rootdummy
		
		if ((node.modifiers["Skin"] == undefined) and (node.name != "rootdummy")) then (
		
			format "Scaling Part: %\n" node.name
			
			node.scale = tm_scale
			
		)
		
		for c in node.children do (
		
			scaleRobePart c scaleData race sex pheno tm_scale:tm_scale
			
		)
		
	)
	
	--	End Functions
	--	Rollout Events
	
	on btn_browsemodel pressed do (
	
		local filename = getOpenFileName caption:"Base Model" types:("(*" + g_fextn + ")|*" + g_fextn + "|")
		
		if (filename != undefined) then (
		
			append basemodels filename
			
		)
		
		nxra_updateList()
		
	)
	
	on btn_build_armour pressed do (
	
		local errors = 0, modelBase, newFileName, scaleINI, strMsg = "", tmpFileName, tmpLst
		
		--	Sanity checks
		
		scaleINI = scriptsPath + "nwmax_plus\\" + drp_scaleini.selected
		
		if not (nx_existFile scaleINI) then (
		
			strMsg += "Scale INI file does not exist.\n"
			errors += 1
			
		)
		
		if not(nx_existDir edt_path.text) then (
		
			strMsg += "Export directory does not exist.\n"
			errors += 1
			
		)
		
		if (errors > 0) then (
		
			messagebox strMsg title:"Sanity Errors!"
			
			return 0
			
		)
		
		--	End Sanity checks
		--	Make sure a trailing "\" is on the export path
		
		if ((edt_path.text[edt_path.text.count] != "\\") and (edt_path.text[edt_path.text.count] != "/")) then (
		
			edt_path.text += "\\"
			
		)
		
		--	Empty the scene
		
		strMsg = "This process will empty the scene before commencing."
		
		if not (queryBox strMsg) then (
		
			return 0
			
		)
		
		clearListener()
		resetMaxFile #noPrompt
		
		--	Load up the scale data and parts mapping data
		
		local scaleData = nx_MultiArray()
		local maxData = ((getINISetting scaleINI "init" "nTransforms") as integer)
		local index, match, part, s, scaleMatrix, temp
		
		scaleData.width = 3
		
		for index = 0 to (maxData - 1) do (
		
			s = "s" + (index as string)
			match = nx_lowercase(getINISetting scaleINI s "match")
			part = nx_lowercase(getINISetting scaleINI s "part")
			temp = filterString (getINISetting scaleINI s "Scale") "(),"
			scaleMatrix = point3 (temp[1] as float) (temp[2] as float) (temp[3] as float)
			scaleData.append (#(part, match, scaleMatrix))
			temp = undefined
			
		)
		
		local raceStr = ""
		
		if chk_human.checked then (
		
			raceStr += "h"
			
		)
		
		if chk_elf.checked then (
		
			raceStr += "e"
			
		)
		
		if chk_dwarf.checked then (
		
			raceStr += "d"
			
		)
		
		if chk_halfling.checked then (
		
			raceStr += "a"
			
		)
		
		if chk_gnome.checked then (
		
			raceStr += "g"
			
		)
		
		if chk_orc.checked then (
		
			raceStr += "o"
			
		)
		
		--	Reset history
		
		lst_history.items = #()
		
		--	Start processing the models in the base model list
		
		for mf in basemodels do (
		
			format "Processing basemodel: %\n" mf
			
			tmpLst = lst_history.items
			append tmpLst ("Model: " + (getFilenameFile mf))
			lst_history.items = tmpLst
			
			--	We need to process for each roberace
			
			local raceIndex
			
			for raceIndex = 1 to raceStr.count do (
			
				--	For speed we don't want to waste time in screen updates
				
				DisableSceneRedraw()
				
				format "Processing Race: %\n" raceStr[raceIndex]
				
				if (loadMaxFile mf) then (
				
					format "File loaded\n"
					
					--	Update model status
					
					tmpLst = lst_history.items
					lst_history.items = tmpLst
					
					--	Prep for scaling
					
					local skinslist = #()
					local selectedSkins = #()
					local lParents = #()
					local objSelect = #()
					
					--	Determine some key info
					
					local pheno, sex
					
					case (rdo_process_sex.state) of (
					
						1: sex = (getFilenameFile mf)[2]
						2: sex = "m"
						3: sex = "f"
						
					)
					
					case (rdo_process_pheno.state) of (
					
						1: pheno = (getFilenameFile mf)[4]
						2: pheno = "0"
						3: pheno = "2"
						
					)
					
					format "Processing for sex = %\nProcessing for pheno = %\n" sex pheno
					
					--	Select everything
					
					max select all
					
					lbl_status.text = "Store selection..."
					
					--	Build parent list and a list of the selection
					
					for s in selection do (
					
						append lParents s
						append lParents s.parent
						append objSelect s
						
					)
					
					--	We have to delink all skins at this stage.  Also record
					--	all the skins for furture processing.
					
					lbl_status.text = "Delink skins..."
					
					for s in objSelect do (
					
						if (s.modifiers["Skin"] != undefined) then (
						
							s.parent = undefined
							
							append selectedSkins s
							
						)
						
					)
					
					--	Also store the bone/weight information for each skin.
					--	We will "re-import" this later when we recreate the
					--	skin.
					
					lbl_status.text = "Store bone weights..."
					
					local swl = #()
					local strStream, weightBuffer
					
					for s in selectedSkins do (
					
						weightBuffer = #()
						
						--	Code from Bioware.  To export bone weights,
						--	modified for this particular purpose.
						
						local save_selection = selection
						
						max modify mode
						select s
						
						local i, j, zeroWeights
						local m = s.modifiers["Skin"]
						local n = skinops.getnumbervertices m
						
						for i = 1 to n do (
						
							local w_num = skinops.getvertexweightcount m i
							local counter = 0
							
							strStream = stringStream ""
							
							for j = 1 to w_num do (
							
								local bone_id = (skinops.getvertexweightboneid m i j)
								local bone_name = "root"
								local weight_i_j = 1
								
								if(bone_id > 0) do (
								
									bone_name = (skinops.getbonename m bone_id 1)
									
								)
								
								weight_i_j = (skinops.getvertexweight m i j)
								
								if (weight_i_j != 0.0) then (
								
									format "  % % " bone_name weight_i_j to:strStream
									
									counter += 1
									
								)
								
							)
							
							if ((w_num == 0) or (counter == 0)) do (
							
								format "  root 1.00 " to:strStream
								
							)
							
							append weightBuffer (strStream as string)
							
						)
						
						if (save_selection != undefined) do (
						
							select save_selection
							
						)
						
						append swl (nx_SkinWeights s weightBuffer)
						
					)
					
					--	Parse through the model file in the scene in a
					--	tree-like way to find the model base.
					
					for tmp in $helpers do (
					
						if ((nx_strclassof tmp) == "aurorabase") then (
						
							modelBase = tmp
							
							scaleArmourPart modelBase objSelect scaleData raceStr[raceIndex] sex pheno
							
							exit
							
						)
						
					)
					
					--	Un-link the entire selection
					
					lbl_status.text = "Un-link geom..."
					
					for s in objSelect do (
					
						s.parent = undefined
						
					)
					
					--	Collapse all skin modifiers
					
					lbl_status.text = "Collapse Skins..."
					
					for s in selectedSkins do (
					
						try (
						
							collapseStack s
							
						) catch (
						
							--	Do nothing here!
							
						)
						
					)
					
					--	Perform a "Reset XForms"
					
					lbl_status.text = "Reset XForms..."
					
					local orig, piv, rotvalue
					
					for s in objSelect do (
					
						if (iskindof s Editable_Mesh) then (
						
							rotvalue = s.rotation
							s.rotation = (quat 0 0 0 1)
							orig = copy s
							
							nx_reset_transforms s
							
							s.rotation = rotvalue
							
							format "XForm added: %\n" s.name
							
							--	Collapse stack
							
							try (
							
								collapsestack s
								
							) catch(
							
								--	Do nothing here!
								
							)
							
							format "XForm collapsed\n"
							
							--	Restore original modifiers less the Xform
							
							nx_copy_modifiers s orig ignore:#("XForm")
							
							delete orig
							
						) else (
						
							--	Not Editable_Mesh therefore is a dummy
							
							s.scale = [1, 1, 1]
							
						)
						
					)
					
					--	Restore skin modifiers
					
					lbl_status.text = "Restore Skins..."
					
					for s in selectedSkins do (
					
						--	Make a new skin modifier and apply it
						
						addmodifier s (skin())
						
					)
					
					for i = 1 to swl.count do (
					
						--	Apply weight info to the skin modifer
						
						addMDLSkin (swl[i]).Object (swl[i]).WeightsData
						
					)
					
					--	Re-link selection
					
					lbl_status.text = "Re-link Geom..."
					
					for n = 1 to lParents.count by 2 do (
					
						lParents[n].parent = lParents[n + 1]
						
					)
					
					--	Work out new file name and save as new file to export
					--	path.
					
					tmpFileName = getFilenameFile mf
					tmpFileName[2] = sex
					tmpFileName[3] = raceStr[raceIndex]
					tmpFileName[4] = pheno
					
					--	Correct model name
					
					lbl_status.text = "Update model bases..."
					
					modelBase.name = tmpFileName
					
					--	Also need to do a special update on the only child of
					--	the model base.
					
					modelBase.children[1].name = tmpFileName + "g"
					
					--	Finalize file name
					
					newFileName = edt_path.text + tmpFileName
					
					--	Save the file out to disk
					
					saveMaxFile newFileName
					
				) else (
				
					--	File load failed
					
					strMsg = "Failed to load: " + mf
					
					messageBox strMsg title:"Error!"
					
				)
				
				--	Turn screen drawing back on again
				
				EnableSceneRedraw()
				
				--	Reset the scene before loading the next file
				
				resetMaxFile #noPrompt
				
			)
			
		)
		
		lbl_status.text = "Done."
		
	)
	
	on btn_build_robes pressed do (
	
		local animSuper, errors = 0, modelBase, newFileName, scaleINI, strMsg = "", tmpFileName, tmpLst
		
		--	Sanity checks
		
		scaleINI = scriptsPath + "nwmax_plus\\" + drp_scaleini.selected
		
		if not (nx_existFile scaleINI) then (
		
			strMsg += "Scale INI file does not exist.\n"
			errors += 1
			
		)
		
		if not(nx_existDir edt_path.text) then (
		
			strMsg += "Export directory does not exist.\n"
			errors += 1
			
		)
		
		if (errors > 0) then (
		
			messagebox strMsg title:"Errors!"
			
			return 0
			
		)
		
		--	End Sanity checks
		--	Make sure a trailing "\" is on the export path
		
		if ((edt_path.text[edt_path.text.count] != "\\") and (edt_path.text[edt_path.text.count] != "/")) then (
		
			edt_path.text += "\\"
			
		)
		
		--	Empty the scene
		
		strMsg = "This process will empty the scene before commencing."
		
		if not (queryBox strMsg) then (
		
			return 0
			
		)
		
		clearListener()
		resetMaxFile #noPrompt
		
		--	Load up the scale data and parts mapping data
		
		local scaleData = nx_MultiArray()
		local maxData = ((getINISetting scaleINI "init" "nTransforms") as integer)
		
		scaleData.width = 3
		
		--	If maxData is undefined then fundamental error
		
		if (maxData == undefined) then (
		
			strMsg = "Error in .INI scale file.  Make sure nTransforms key is set."
			
			messageBox strMsg title:"Error!"
			
			return 0
			
		)
		
		local index, match, part, s, scaleMatrix, sub, temp
		
		for index = 0 to (maxData - 1) do (
		
			s = "s" + (index as string)
			part = nx_lowercase(getINISetting scaleINI s "part")
			match = nx_lowercase(getINISetting scaleINI s "match")
			temp = filterString (getINISetting scaleINI s "Scale") "(),"
			scaleMatrix = point3 (temp[1] as float) (temp[2] as float) (temp[3] as float)
			
			scaleData.append (#(part, match, scaleMatrix))
			
			temp = undefined
			
		)
		
		local raceStr = ""
		
		if chk_human.checked then (
		
			raceStr += "h"
			
		)
		
		if chk_elf.checked then (
		
			raceStr += "e"
			
		)
		
		if chk_dwarf.checked then (
		
			raceStr += "d"
			
		)
		
		if chk_halfling.checked then (
		
			raceStr += "a"
			
		)
		
		if chk_gnome.checked then (
		
			raceStr += "g"
			
		)
		
		if chk_orc.checked then (
		
			raceStr += "o"
			
		)
		
		--	Start processing the models in the base model list
		
		lst_history.items = #()
		
		for mf in basemodels do (
		
			format "Processing basemodel: %\n" mf
			
			tmpLst = lst_history.items
			
			append tmpLst ("Model: " + (getFilenameFile mf))
			
			lst_history.items = tmpLst
			
			--	We need to process for each roberace
			
			local raceIndex
			
			for raceIndex = 1 to raceStr.count do (
			
				--	For speed we don't want to waste time in screen updates
				
				DisableSceneRedraw()
				
				format "Processing Race: %\n" raceStr[raceIndex]
				
				--	Load the model file
				
				if (loadMaxFile mf) then (
				
					format "File Loaded\n"
					
					--	Update model status
					
					tmpLst = lst_history.items
					
					case (raceStr[raceIndex]) of (
					
						"a": append tmpLst "...Halfling"
						"d": append tmpLst "...Dwarf"
						"e": append tmpLst "...Elf"
						"g": append tmpLst "...Gnome"
						"o": append tmpLst "...Half-Orc"
						"h": append tmpLst "...Human"
						"p": append tmpLst "...Phenotype"
						
					)
					
					lst_history.items = tmpLst
					
					--	Prep for scaling
					
					local skinslist = #()
					local selectedSkins = #()
					local lParents = #()
					local objSelect = #()
					
					--	Determine some key info
					
					local pheno, sex
					
					case (rdo_process_sex.state) of (
					
						1: sex = (getFilenameFile mf)[2]
						2: sex = "m"
						3: sex = "f"
						
					)
					
					case (rdo_process_pheno.state) of (
					
						1: pheno = (getFilenameFile mf)[4]
						2: pheno = "0"
						3: pheno = "2"
						
					)
					
					format "Processing for sex = %\nProcessing for pheno = %\n" sex pheno
					
					--	Select everything
					
					max select all
					
					lbl_status.text = "Store selection..."
					
					--	Build parent list and a list of the selection
					
					for s in selection do (
					
						append lParents s
						append lParents s.parent
						append objSelect s
						
					)
					
					--	We have to de-link all skins at this stage.  Also
					--	record all the skins for furture processing.
					
					lbl_status.text = "De-link skins..."
					
					for s in objSelect do (
					
						if (s.modifiers["Skin"] != undefined) then (
						
							s.parent = undefined
							
							append selectedSkins s
							
						)
						
					)
					
					--	Also store the bone/weight information for each skin.
					--	We will "re-import" this later when we recreate the
					--	skin.
					
					lbl_status.text = "Store bone weights..."
					
					local swl = #()
					local strStream, weightBuffer
					
					for s in selectedSkins do (
					
						weightBuffer = #()
						
						--	Code from Bioware.  To export bone weights,
						--	modified for this particular purpose.
						
						local save_selection = selection
						
						max modify mode
						select s
						
						local i, j, zeroWeights
						local m = s.modifiers["Skin"]
						local n = skinops.getnumbervertices m
						
						for i = 1 to n do (
						
							local w_num = skinops.getvertexweightcount m i
							local counter = 0
							
							strStream = stringStream ""
							
							for j = 1 to w_num do (
							
								local bone_id = (skinops.getvertexweightboneid m i j)
								local bone_name = "root"
								local weight_i_j = 1
								
								if (bone_id > 0) do (
								
									bone_name = (skinops.getbonename m bone_id 1)
									
								)
								
								weight_i_j = (skinops.getvertexweight m i j)
								
								if (weight_i_j != 0.0) then (
								
									format "  % % " bone_name weight_i_j to:strStream
									
									counter += 1
									
								)
								
							)
							
							if ((w_num == 0) or (counter == 0)) do (
							
								format "  root 1.00 " to:strStream
								
							)
							
							append weightBuffer (strStream as string)
							
						)
						
						if (save_selection != undefined) do (
						
							select save_selection
							
						)
						
						append swl (nx_SkinWeights s weightBuffer)
						
					)
					
					--	Parse through the model file in the scene in a
					--	tree-like way to find the model base.
					
					for tmp in $helpers do (
					
						if ((nx_strclassof tmp) == "aurorabase") then (
						
							modelBase = tmp
							
							scaleRobePart modelBase scaleData raceStr[raceIndex] sex pheno
							
							exit
							
						)
						
					)
					
					--	Work out new file name and save as new file to export
					--	path.
					
					tmpFileName = getFilenameFile mf
					tmpFileName[2] = sex
					tmpFileName[3] = raceStr[raceIndex]
					tmpFileName[4] = pheno
					
					--	Un-link the entire selection
					
					lbl_status.text = "Un=link geom..."
					
					for s in objSelect do (
					
						s.parent = undefined
						
					)
					
					--	Collapse all skin modifiers
					
					lbl_status.text = "Collapse Skins..."
					
					for s in selectedSkins do (
					
						try (
						
							collapseStack s
							
						) catch (
						
							--	Do nothing here!
							
						)
						
					)
					
					--	Perform a "Reset XFrom"
					
					lbl_status.text = "Reset XForms..."
					
					local orig, piv, rotvalue
					
					for s in objSelect do (
					
						if (iskindof s Editable_Mesh) then (
						
							rotvalue = s.rotation
							s.rotation = (quat 0 0 0 1)
							orig = copy s
							
							nx_reset_transforms s
							
							s.rotation = rotvalue
							
							format "XForm added: %\n" s.name
							
							--	Collapse stack
							
							try (
							
								collapsestack s
								
							) catch(
							
								--	Do nothing here!
								
							)
							
							format "XForm collapsed\n"
							
							--	Restore original modifiers less the Xform
							
							nx_copy_modifiers s orig ignore:#("XForm")
							
							delete orig
							
						) else (
						
							--	Not Editable_Mesh therefore is a dummy
							
							s.scale = [1, 1, 1]
							
						)
						
					)
					
					--	Restore skin modifiers
					
					lbl_status.text = "Restore Skins..."
					
					for s in selectedSkins do (
					
						--	Make a new skin modifier and apply it
						
						addmodifier s (skin())
						
					)
					
					for i = 1 to swl.count do (
					
						--	Apply weight info to the skin modifer
						
						addMDLSkin (swl[i]).Object (swl[i]).WeightsData
						
					)
					
					--	Perform pivot snap if needed
					
					format "Snap Pivot\n"
					
					if chk_snappos.checked then (
					
						nx_snaptonormalpivots (substring tmpFileName 1 4)
						
					)
					
					--	Re-link selection
					
					lbl_status.text = "Re-link Geom..."
					
					for n = 1 to lParents.count by 2 do (
					
						lParents[n].parent = lParents[n + 1]
						
					)
					
					--	Correct model name
					
					lbl_status.text = "Update model bases..."
					
					modelBase.name = tmpFileName
					
					--	Set the model animsuper
					
					animSuper = getINISetting scaleINI "skeleton" (substring tmpFileName 1 4)
					
					format "Supermodel: %\n" animSuper
					
					modelBase.setsupermodel = animSuper
					
					--	Finalize file name
					
					newFileName = edt_path.text + tmpFileName
					
					--	Save the file out to disk
					
					saveMaxFile newFileName
					
				) else (
				
					--	File load failed
					
					strMsg = "Failed to load: " + mf
					
					messageBox strMsg title:"Error!"
					
				)
				
				--	Turn screen drawing back on again
				
				EnableSceneRedraw()
				
				--	Reset the scene before loading the next file
				
				resetMaxFile #noPrompt
				
			)
			
		)
		
		lbl_status.text = "Done."
		
	)
	
	on btn_delete pressed do (
	
		local sel = lst_basemodels.selection
		
		if (sel == 0) then (
		
			messagebox "Please select a Reference Model to delete first." title:"No Model Selected!"
			
			return 0
			
		) else (
		
			deleteItem basemodels sel
			
			nxra_updateList()
			
		)
		
	)
	
	on btn_loadpattern pressed do (
	
		local files, f
		
		if ((edt_loadpattern == undefined) or (edt_loadpattern.text == "")) then (
		
			messagebox "Please enter a search pattern first." title:"No Search Pattern Entered!"
			
			return 0
			
		) else (
		
			files = getFiles (edt_loadpattern.text)
			
			for f in files do (
			
				append basemodels f
				
			)
			
			nxra_updateList()
			
		)
		
	)
	
	on btnBrowseDir pressed do (
	
		local dirpath
		
		-- Suggested by Danmar.
		
		dirpath = getSavePath()
		
		if (dirpath != undefined) then (
		
			edt_path.text = dirpath
			
		)
		
	)
	
	on btnResetDir pressed do (
	
		local bIsSelection = false
		
		if not(nx_existDir edt_path.text) then (
		
			messagebox "Directory does not exist!"
			
			return 0
			
		)
		
		for b in $helpers do (
		
			if b.isSelected and (iskindof b aurorabase) then (
			
				bIsSelection = true
				
				-- Node is selected so reset base.
				
				b.export_path = edt_path.text
				
			)
			
		)
		
		if not (bIsSelection) then (
		
			messagebox "No Aurora Bases selected."
			return 0
			
		)
		
	)
	
	on drp_scaleini selected val do (
	
		lbl_inidescr.text = ""
		lbl_inidescr.text = getINISetting (scriptsPath + "nwmax_plus\\" + (drp_scaleini.items[val])) "init" "descr"
		
		--	Get the default races this file will generate
		
		chk_human.checked = false
		chk_elf.checked = false
		chk_dwarf.checked = false
		chk_halfling.checked = false
		chk_gnome.checked = false
		chk_orc.checked = false
		
		local defaultRaces = getINISetting (scriptsPath + "nwmax_plus\\" + (drp_scaleini.items[val])) "roberaces" "races"
		local i
		
		for i = 1 to (defaultRaces.count) do (
		
			case defaultRaces[i] of (
			
				"h": chk_human.checked = true
				"e": chk_elf.checked = true
				"d": chk_dwarf.checked = true
				"a": chk_halfling.checked = true
				"g": chk_gnome.checked = true
				"o": chk_orc.checked = true
				
			)
			
		)
		
		local validProcess = getINISetting (scriptsPath + "nwmax_plus\\" + (drp_scaleini.items[val])) "init" "type"
		
		btn_build_armour.enabled = false
		btn_build_robes.enabled = false
		
		case validProcess of (
		
			"a": btn_build_armour.enabled = true
			"r": btn_build_robes.enabled = true
			
		)
		
	)
	
	on nx_ramaker close do (
	
		--	Do nothing here
		
	)
		
	on nx_ramaker moved val do (
	
		--	Do nothing here
		
	)
	
	on nx_ramaker open do (
	
		--	Position floater
	
		ramaker_x = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "ramaker" "ramaker_x"
		ramaker_y = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "ramaker" "ramaker_y"
		
		if (ramaker_x == undefined) or (ramaker_x == "") then (
		
			raFloater.pos = [20, 20]
			
		) else (
		
			format "Positioning...\r\n"
		
			raFloater.pos.x = (ramaker_x as float) - 12
			raFloater.pos.y = (ramaker_y as float) - 16
			
		)
		
		raFloater.size = [220, 600]
		
		--	End Position
		
		local inis = #()
		local tmp = getFiles (scriptsPath + "nwmax_plus\\sc_*.ini")
		
		--	Sort the file list
		
		sort tmp
		
		for s in tmp do (
		
			append inis (filenameFromPath s)
			
		)
		
		drp_scaleini.items = inis
		drp_scaleini.selection = 1
		
		--	Get scale file description
		
		lbl_inidescr.text = getINISetting tmp[1] "init" "descr"
		
		--	Get the default races this file will generate
		
		chk_human.checked = false
		chk_elf.checked = false
		chk_dwarf.checked = false
		chk_halfling.checked = false
		chk_gnome.checked = false
		chk_orc.checked = false
		
		local defaultRaces = getINISetting tmp[1] "roberaces" "races"
		local validProcess = getINISetting tmp[1] "init" "type"
		local i
		
		for i = 1 to (defaultRaces.count) do (
		
			case defaultRaces[i] of (
			
				"h": chk_human.checked = true
				"e": chk_elf.checked = true
				"d": chk_dwarf.checked = true
				"a": chk_halfling.checked = true
				"g": chk_gnome.checked = true
				"o": chk_orc.checked = true
				
			)
			
		)
		
		btn_build_armour.enabled = false
		btn_build_robes.enabled = false
		
		case validProcess of (
		
			"a": btn_build_armour.enabled = true
			"r": btn_build_robes.enabled = true
			
		)
		
		--	Load in last used paths
		
		local exportpath = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "ramaker" "exportPath"
		
		if (loadptrn != undefined) then (
		
			edt_loadpattern.text = loadptrn
			
		)
		
		if (exportpath != undefined) then (
		
			edt_path.text = exportpath
			
		)
		
	)
	
	--	End Events
	
)

rollout rloRAUtilities "Utilities" (

	group "Load Armor Pieces" (
	
		edittext edt_arloadptrn "Dir: " align:#left text:(getdir #export) width:160
		editText edt_arsrp "Sex/Race/Pheno: " align:#right text:"pmh0" width:160
		editText edt_arnum "Armor #: " align:#right text:"001" width:160
		button btn_loadarmour "Load Armor" align:#center toolTip:"Load all armor pieces matching a pattern" width:165
		
	)
	
	group "Mass Export" (
	
		edittext edt_masstarget "Dir: " align:#left text:(getdir #export) width:160
		button btn_massexport "Mass Export" align:#center toolTip:"Mass Export all models in \"Export Path\" to Target" width:165
		
	)
	
	--	Rollout Events
	
	on btn_loadarmour pressed do (
	
		local robeparts = #("head_g", "neck_g", "Lshoulder_g", "lbicep_g", "lforearm_g", "lhand_g", "Rshoulder_g", "rbicep_g", "rforearm_g", "rhand_g", "torso_g", "belt_g", "pelvis_g", "lthigh_g", "lshin_g", "lfoot_g", "rthigh_g", "rshin_g", "rfoot_g")
		local armourparts = #("head", "neck", "shol", "bicepl", "forel", "handl", "shor", "bicepr", "forer", "handr", "chest", "belt", "pelvis", "legl", "shinl", "footl", "legr", "shinr", "footr")
		local fileList
		
		--	Some sanity checks before getting too far
		
		if (((edt_arloadptrn.text[edt_arloadptrn.text.count]) != "\\") and ((edt_arloadptrn.text[edt_arloadptrn.text.count]) != "/")) then (
		
			edt_arloadptrn.text += "\\"
			
		)
		
		--	Get the template name to use to drive the armor part loading
		
		local super = edt_arsrp.text
		
		--	Reset the scene and load model base dummy in from supermodels
		
		resetMaxFile #noPrompt
		loadMaxFile (scriptsPath + "nwmax_plus\\supermodels\\" + super + g_fextn)
		
		local templateBase = undefined
		
		for h in $helpers do (
		
			if ((nx_strclassof h) == "aurorabase") then (
			
				templateBase = h
				
				exit
				
			)
			
		)
		
		if (templateBase == undefined) then (
		
			messagebox "No model base found." title:"No Model Base Found!"
			
			return 0
			
		)
		
		--	Clean out all materials from the scene
		
		for o in $objects do (
		
			if ((o.mat != undefined) and ((classof o.mat) == Standardmaterial)) then (
			
				deleteItem sceneMaterials o.mat.name
				
				o.mat = undefined
				
			)
			
		)
		
		--	Models to load
		
		local path = edt_arloadptrn.text
		local toLoad, i, snode, tnode, gnode, toLoadName
		
		--	Load it all in first
		
		for i = 1 to armourparts.count do (
		
			--	Start loading
			
			if (edt_arnum.text == "*") then (
			
				toLoad = path + super + "_" + armourparts[i] + "*" + g_fextn
				fileList = getFiles toLoad
				toLoad = fileList[1]
				
				if (toLoad != undefined) then (
				
					toLoadName = getFilenameFile toLoad
					
				)
				
			) else (
			
				toLoad = path + super + "_" + armourparts[i] + edt_arnum.text + g_fextn
				toLoadName = getFilenameFile toLoad
				
			)
			
			try (
			
				if (mergeMaxFile toLoad #deleteOldDups) then (
				
					--	Get the targetsource node and target nodes then
					--	re-position
					
					snode = getNodeByName toLoadName	--	Loaded node
					tnode = getNodeByName robeparts[i]	--	Target dummy node
					
					if (snode != undefined) then (
					
						--	Change Position
						
						snode.pos = tnode.pos
						
						format "Loaded: %.\nMoved to: %.\n" snode.name tnode.name
						
					) else (
					
						format "Error - node: % not found\n" toLoadName
						
					)
					
				)
				
			) catch (
			
				format "MergeMaxFile error trapped\n"
				
			)
			
		)
		
	)
	
	on btn_massexport pressed do (
	
		--	Check that the directory exists
		
		local dir = edt_masstarget.text
		
		if (dir[dir.count] != "\\") then (
		
			dir += "\\"
			
		)
		
		if not (nx_existDir dir) then (
		
			--	Warning message and exit
			
			return 0
			
		)
		
		local sourcedir = edt_path.text
		
		if (sourcedir[sourcedir.count] != "\\") then (
		
			sourcedir += "\\"
			
		)
		
		if not (nx_existDir sourcedir) then (
		
			--	Warning message and exit
			
			return 0
			
		)
		
		sourcedir += ("*" + g_fextn)
		
		--	Start the export process
		
		massexport pattern:sourcedir target:dir anims:true
		
	)
	
	on rloRAUtilities open do (
	
		--	Load in last used paths
		
		local ardummyloadptrn = getINISetting (scriptsPath + "nwmax_plus\\nwmax.ini") "ramaker" "ra_dummyload_pattern"
		local trgt = getINISetting (scriptsPath + "nwmac_plus\\nwmax.ini") "ramaker" "ra_mass_target_path"
		
		if (ardummyloadptrn != undefined) then (
		
			edt_arloadptrn.text = ardummyloadptrn
			
		)
		
		if (trgt != undefined) then (
		
			edt_masstarget.text = trgt
			
		)
		
	)
	
)

rollout rlo_raPrefs "Preferences" (

	button btn_exportPath "Export Path" width:165
	button btn_setPosition "Set Position" width:165
	
	on btn_setPosition pressed do (
	
		local datalist = #()
		
		ramaker_x = raFloater.pos.x
		ramaker_y = raFloater.pos.y
		
		append datalist #("ramaker", "ramaker_x", (ramaker_x as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
		append datalist #("ramaker", "ramaker_y", (ramaker_y as string), (scriptsPath + "nwmax_plus\\nwmax.ini"))
		nx_setinivalue datalist
		
	)
	
	on btn_exportPath pressed do (
	
		local dirpath
		
		dirpath = getSavePath()
		
		if (dirpath != undefined) then (
		
			local datalist = #()
			
			append datalist #("ramaker", "exportPath", dirpath, (scriptsPath + "nwmax_plus\\nwmax.ini"))
			nx_setinivalue datalist
			
			nx_ramaker.edt_path.text = dirpath
			
		)
		
	)
	
)

--	Close the old floater

if (raFloater != undefined) do (
	
	closeRolloutFloater raFloater
		
)
	
--	Create a new floater

raFloater = newRolloutFloater "Robe and Armor Maker" 220 600 0 100
addRollout nx_ramaker raFloater
addRollout rloRAUtilities raFloater
addRollout rlo_raPrefs raFloater rolledUp:true
